package com.aeolus.dots

import android.app.Application
import com.aeolus.dots.di.components.ApplicationComponent
import com.aeolus.dots.di.components.DaggerApplicationComponent
import com.aeolus.dots.di.modules.AndroidModule

class App : Application() {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        component = DaggerApplicationComponent.builder().androidModule(AndroidModule(this)).build()
        component.inject(this)
    }

}