package com.aeolus.dots.custom_views

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.aeolus.dots.R
import com.madrapps.pikolo.listeners.SimpleColorSelectionListener
import kotlinx.android.synthetic.main.view_color_picker.view.*
import java.util.*


class ColorPickerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    var onClosedListener: (() -> Unit)? = null
    private var currentColor: Int = -1
    private var selectedColor: Int = -1

    init {
        View.inflate(context, R.layout.view_color_picker, this)
        setRandomColor()
        updateCurrentColorSelection()
        class ColorSelectedListener : SimpleColorSelectionListener() {

            override fun onColorSelected(color: Int) {
                super.onColorSelected(color)
                currentColor = color
                fab_ok.backgroundTintList = ColorStateList.valueOf(color)
            }
        }
        color_picker.setColorSelectionListener(ColorSelectedListener())
        fab_ok.setOnClickListener {
            selectedColor = currentColor
            hide()
        }
        color_picker_background.setOnClickListener { hide() }
    }

    override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState() ?: return null
        val ss = SavedState(superState)
        ss.currentColor = currentColor
        ss.selectedColor = selectedColor
        ss.isVisible = isVisible()
        return ss
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        val ss = state as SavedState
        super.onRestoreInstanceState(ss.superState)
        currentColor = ss.currentColor
        selectedColor = ss.selectedColor
        updateCurrentColorSelection()
        if (ss.isVisible) show() else hide()
    }

    private fun updateCurrentColorSelection() {
        color_picker.setColor(currentColor)
        fab_ok.backgroundTintList = ColorStateList.valueOf(currentColor)
    }

    private fun setCurrentColor(color: Int) {
        currentColor = color
        updateCurrentColorSelection()
    }

    fun setRandomColor() {
        val rnd = Random()
        setCurrentColor(
            Color.argb(
                255,
                rnd.nextInt(256),
                rnd.nextInt(256),
                rnd.nextInt(256)
            )
        )
        selectedColor = currentColor
    }

    fun setSelectedColor(color: Int) {
        selectedColor = color
    }

    fun getSelectedColor(): Int {
        return selectedColor
    }

    fun getCurrentColor(): Int {
        return currentColor
    }

    fun show() {
        color_picker_background.visibility = View.VISIBLE
    }

    fun hide() {
        color_picker_background.visibility = View.GONE
        setCurrentColor(selectedColor)
        onClosedListener?.invoke()
    }

    fun isVisible(): Boolean {
        return color_picker_background.visibility == View.VISIBLE
    }

    private class SavedState : BaseSavedState {

        companion object {

            @JvmField
            val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.Creator<SavedState> {
                override fun createFromParcel(inParcel: Parcel): SavedState {
                    return SavedState(inParcel)
                }

                override fun newArray(size: Int): Array<SavedState?> {
                    return arrayOfNulls(size)
                }
            }
        }

        var currentColor: Int = -1
        var selectedColor: Int = -1
        var isVisible: Boolean = false

        internal constructor(superState: Parcelable) : super(superState)

        private constructor(inParcel: Parcel) : super(inParcel) {
            currentColor = inParcel.readInt()
            selectedColor = inParcel.readInt()
            isVisible = inParcel.readInt().toBoolean()
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeInt(currentColor)
            out.writeInt(selectedColor)
            out.writeInt(isVisible.toInt())
        }

        fun Boolean.toInt() = if (this) 1 else 0

        fun Int.toBoolean() = this > 0

    }

}