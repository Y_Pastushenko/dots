package com.aeolus.dots.custom_views

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aeolus.dots.R
import com.aeolus.dots.entities.GameState
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.list_item_turn.view.*
import kotlinx.android.synthetic.main.view_turn_selector.view.*


class TurnSelector @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    var onStateClick: ((GameState) -> Unit)? = null

    init {
        View.inflate(context, R.layout.view_turn_selector, this)
        rv_turns.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    }

    fun setUp(statesList: List<GameState>) {
        rv_turns.adapter = TurnsAdapter(context)
        (rv_turns.adapter as TurnsAdapter).setGameStates(statesList)
        if (onStateClick != null) (rv_turns.adapter as TurnsAdapter).onStateClick = onStateClick
    }

    fun setClickListener(onStateClick: (GameState) -> Unit) {
        this.onStateClick = onStateClick
        if (rv_turns.adapter != null) (rv_turns.adapter as TurnsAdapter).onStateClick = onStateClick
    }

    override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState() ?: return null
        val ss = SavedState(superState)
        if (rv_turns.adapter != null)
            ss.turnSelectorState = TurnSelectorState((rv_turns.adapter as TurnsAdapter).states)
        return ss
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        val ss = state as SavedState
        super.onRestoreInstanceState(ss.superState)
        if (ss.turnSelectorState != null) setUp(ss.turnSelectorState!!.statesList)
    }

    class TurnsAdapter(private val context: Context?) : RecyclerView.Adapter<TurnViewHolder>() {

        val states: MutableList<GameState> = mutableListOf()
        var onStateClick: ((GameState) -> Unit)? = null

        fun addGameState(gameState: GameState) {
            states.add(gameState)
            notifyDataSetChanged()
        }

        fun setGameStates(gameStates: List<GameState>) {
            this.states.clear()
            this.states.addAll(gameStates)
            notifyDataSetChanged()
        }

        fun deleteGameState(gameState: GameState) {
            states.remove(gameState)
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): TurnViewHolder {
            return TurnViewHolder(
                LayoutInflater.from(context)
                    .inflate(R.layout.list_item_turn, parent, false)
            )
        }

        override fun onBindViewHolder(holder: TurnViewHolder, position: Int) {
            if (context != null) holder.background.setOnClickListener { onStateClick?.invoke(states[position]) }
        }

        override fun getItemCount(): Int {
            return states.size
        }

    }

    class TurnViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val background: RelativeLayout = view.rl_background
    }

    @Parcelize
    private data class TurnSelectorState(val statesList: MutableList<GameState>) : Parcelable

    private class SavedState : BaseSavedState {

        companion object {

            @JvmField
            val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.Creator<SavedState> {
                override fun createFromParcel(inParcel: Parcel): SavedState {
                    return SavedState(inParcel)
                }

                override fun newArray(size: Int): Array<SavedState?> {
                    return arrayOfNulls(size)
                }
            }
        }

        var turnSelectorState: TurnSelectorState? = null

        internal constructor(superState: Parcelable) : super(superState)

        private constructor(inParcel: Parcel) : super(inParcel) {
            turnSelectorState = inParcel.readParcelable(TurnSelectorState::class.java.classLoader)
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeParcelable(turnSelectorState, 0)
        }

    }

}