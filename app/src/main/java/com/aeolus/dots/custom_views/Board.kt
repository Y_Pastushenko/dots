package com.aeolus.dots.custom_views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.RelativeLayout
import com.aeolus.dots.CELL_LENGTH
import com.aeolus.dots.DOT_RADIUS
import com.aeolus.dots.R
import com.aeolus.dots.STROKE_WIDTH
import com.aeolus.dots.Utils.Companion.dpToPx
import com.aeolus.dots.Utils.Companion.pxToDp
import com.aeolus.dots.entities.Cell
import com.aeolus.dots.entities.GameState
import com.aeolus.dots.entities.Line
import kotlinx.android.synthetic.main.view_board.view.*
import kotlin.math.floor

class Board @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    companion object {
        private const val CLICK_ACTION_THRESHHOLD = 200
    }

    private var gameState: GameState? = null

    init {
        View.inflate(context, R.layout.view_board, this)
    }

    fun setup(columns: Int, rows: Int) {
        board_grid.setup(columns, rows)
        dots_layer.setup(columns, rows)
    }

    fun setOnCellClickListener(onCellClick: (Int, Int) -> Unit) {
        var lastTouchDown: Long = 0
        board_grid.setOnTouchListener { _: View, motionEvent: MotionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> lastTouchDown = System.currentTimeMillis()
                MotionEvent.ACTION_UP -> {
                    if (System.currentTimeMillis() - lastTouchDown < CLICK_ACTION_THRESHHOLD) {
                        val x = motionEvent.x
                        val y = motionEvent.y
                        val dpx = pxToDp(context, x)
                        val dpy = pxToDp(context, y)
                        val cellX = floor(dpx / CELL_LENGTH).toInt()
                        val cellY = floor(dpy / CELL_LENGTH).toInt()
                        onCellClick(cellX, cellY)
                    }
                }
            }
            true
        }
    }

    fun showState(gameState: GameState) {
        this.gameState = gameState
        dots_layer.showState(gameState)
    }

    override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState() ?: return null
        val ss = SavedState(superState)
        ss.gameState = gameState
        return ss
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        val ss = state as SavedState
        super.onRestoreInstanceState(ss.superState)
        gameState = ss.gameState
        if (gameState != null) {
            setup(gameState!!.board.size, gameState!!.board[0].size)
            showState(gameState!!)
        }
    }

    private class SavedState : BaseSavedState {

        companion object {

            @JvmField
            val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.Creator<SavedState> {
                override fun createFromParcel(inParcel: Parcel): SavedState {
                    return SavedState(inParcel)
                }

                override fun newArray(size: Int): Array<SavedState?> {
                    return arrayOfNulls(size)
                }
            }
        }

        var gameState: GameState? = null

        internal constructor(superState: Parcelable) : super(superState)

        private constructor(inParcel: Parcel) : super(inParcel) {
            gameState = inParcel.readParcelable(GameState::class.java.classLoader)
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeParcelable(gameState, 0)
        }

    }
}

class BoardGrid @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val linePaint = Paint()
    private val cellLength = dpToPx(context, CELL_LENGTH.toFloat())
    var columns = -1
    var rows = -1

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        for (i in 0 until columns) {
            canvas.drawLine(
                cellLength / 2 + cellLength * i,
                0f,
                cellLength / 2 + cellLength * i,
                cellLength * columns,
                linePaint
            )
        }
        for (i in 0 until rows) {
            canvas.drawLine(
                0f,
                cellLength / 2 + cellLength * i,
                cellLength * columns,
                cellLength / 2 + cellLength * i,
                linePaint
            )
        }
    }

    fun setup(columns: Int, rows: Int): BoardGrid {
        val layoutParams: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
            dpToPx(context, CELL_LENGTH.toFloat() * columns).toInt(),
            dpToPx(context, CELL_LENGTH.toFloat() * rows).toInt()
        )
        this.layoutParams = layoutParams
        linePaint.color = Color.BLACK
        linePaint.style = Paint.Style.STROKE
        linePaint.strokeWidth = 3f
        this.rows = rows
        this.columns = columns
        invalidate()
        return this
    }

}

class DotsLayer @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val dotPaint = Paint()
    private val previewDotPaint = Paint()
    private val linePaint = Paint()
    private val cellsWithDots: MutableSet<Cell> = mutableSetOf()
    private val linesToDisplay: MutableList<Line> = mutableListOf()
    private var cellForPreview: Cell? = null
    var columns = -1
    var rows = -1

    init {
        dotPaint.color = Color.BLACK
        dotPaint.style = Paint.Style.FILL
        previewDotPaint.strokeWidth = 10f
        previewDotPaint.color = Color.BLACK
        previewDotPaint.style = Paint.Style.STROKE
        linePaint.strokeWidth = dpToPx(context, STROKE_WIDTH.toFloat())
        linePaint.color = Color.BLACK
        linePaint.style = Paint.Style.STROKE
    }

    fun setup(columns: Int, rows: Int): DotsLayer {
        this.columns = columns
        this.rows = rows
        val layoutParams: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
            dpToPx(context, CELL_LENGTH.toFloat() * columns).toInt(),
            dpToPx(context, CELL_LENGTH.toFloat() * rows).toInt()
        )
        this.layoutParams = layoutParams
        return this
    }

    fun showState(gameState: GameState) {
        cellsWithDots.clear()
        linesToDisplay.clear()
        for (row in gameState.board) for (cell in row) if (cell.hasDot()) cellsWithDots.add(cell)
        cellForPreview = gameState.addedCell
        linesToDisplay.addAll(gameState.lines)
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        for (cell in cellsWithDots) drawCell(cell, canvas, dotPaint)
        if (cellForPreview != null) drawCell(cellForPreview!!, canvas, previewDotPaint)
        for (line in linesToDisplay) drawLine(line, canvas)
    }

    private fun drawCell(cell: Cell, canvas: Canvas, paint: Paint) {
        val circleCenter = getCellCenter(cell)
        val color = cell.dot?.player?.color
        if (color != null) dotPaint.color = color
        canvas.drawCircle(
            circleCenter.first,
            circleCenter.second,
            dpToPx(context, DOT_RADIUS.toFloat()),
            paint
        )
    }

    private fun drawLine(line: Line, canvas: Canvas) {
        val cell1Center = getCellCenter(line.x1, line.y1)
        val cell2Center = getCellCenter(line.x2, line.y2)
        linePaint.color = line.color
        canvas.drawLine(
            cell1Center.first,
            cell1Center.second,
            cell2Center.first,
            cell2Center.second,
            linePaint
        )
    }

    private fun getCellCenter(cell: Cell): Pair<Float, Float> {
        return Pair(
            ((cell.column + 0.5) * dpToPx(context, CELL_LENGTH.toFloat())).toFloat(),
            ((cell.row + 0.5) * dpToPx(context, CELL_LENGTH.toFloat())).toFloat()
        )
    }

    private fun getCellCenter(column: Int, row: Int): Pair<Float, Float> {
        return Pair(
            ((column + 0.5) * dpToPx(context, CELL_LENGTH.toFloat())).toFloat(),
            ((row + 0.5) * dpToPx(context, CELL_LENGTH.toFloat())).toFloat()
        )
    }

}