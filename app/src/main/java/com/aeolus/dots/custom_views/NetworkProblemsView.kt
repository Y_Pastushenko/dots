package com.aeolus.dots.custom_views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.view_network_problems.view.*
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import com.aeolus.dots.R


class NetworkProblemsView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.view_network_problems, this)
    }

    fun show(message: String, onRetry: () -> Unit) {
        tv_no_network_message.text = message
        iv_retry.setOnClickListener { onRetry() }
        rl_np_background.animate()
            .alpha(1f)
            .setDuration(300)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    rl_np_background.visibility = VISIBLE
                }
            })
    }

    fun hide() {
        rl_np_background.animate()
            .alpha(0.0f)
            .setDuration(300)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    rl_np_background.visibility = GONE
                }
            })
    }

}