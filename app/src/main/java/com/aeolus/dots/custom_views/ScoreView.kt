package com.aeolus.dots.custom_views

import android.content.Context
import android.graphics.Typeface
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import com.aeolus.dots.R
import com.aeolus.dots.Utils.Companion.dpToPx
import com.aeolus.dots.entities.GameState
import com.aeolus.dots.entities.Player
import kotlinx.android.synthetic.main.view_score.view.*

class ScoreView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var tvs: HashMap<Player, TextView> = hashMapOf()
    private var gameState: GameState? = null

    init {
        View.inflate(context, R.layout.view_score, this)
    }

    fun setState(gameState: GameState) {
        if (tvs.isEmpty())
            for (ps in gameState.players) {
                val tv = TextView(context)
                tv.layoutParams = LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
                tv.gravity = View.TEXT_ALIGNMENT_CENTER
                tv.setPadding(
                    dpToPx(context, 10f).toInt(),
                    dpToPx(context, 0f).toInt(),
                    dpToPx(context, 10f).toInt(),
                    dpToPx(context, 0f).toInt()
                )
                tv.setTextColor(ps.player.color)
                tvs[ps.player] = tv
                ll_score_container.addView(tv)
            }
        for (ps in gameState.players)
            tvs[ps.player]?.text = (if (gameState.rules.emptyFieldCost == 0f) ps.score.toInt() else ps.score).toString()
        for (tv in tvs.values) {
            tv.textSize = 16f//spToPx(context, 16f)
            tv.typeface = Typeface.DEFAULT
        }
        tvs[gameState.getCurrentPlayer()]?.textSize = 20f//spToPx(context, 20f)
        tvs[gameState.getCurrentPlayer()]?.typeface = Typeface.DEFAULT_BOLD
    }

    override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState() ?: return null
        val ss = SavedState(superState)
        ss.gameState = gameState
        return ss
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        val ss = state as SavedState
        super.onRestoreInstanceState(ss.superState)
        gameState = ss.gameState
        if (gameState != null) setState(gameState!!)
    }

    private class SavedState : BaseSavedState {

        companion object {

            @JvmField
            val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.Creator<SavedState> {
                override fun createFromParcel(inParcel: Parcel): SavedState {
                    return SavedState(inParcel)
                }

                override fun newArray(size: Int): Array<SavedState?> {
                    return arrayOfNulls(size)
                }
            }
        }

        var gameState: GameState? = null

        internal constructor(superState: Parcelable) : super(superState)

        private constructor(inParcel: Parcel) : super(inParcel) {
            gameState = inParcel.readParcelable(GameState::class.java.classLoader)
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeParcelable(gameState, 0)
        }

    }

}