package com.aeolus.dots.root.view

interface RootView {
    fun startLoading()
    fun stopLoading()
    fun showNoNetworkProblem(message: String, onRetry: () -> Unit)
    fun hideNoNetworkProblem()
    fun showMessage(message: String)
    fun exitApp()
}