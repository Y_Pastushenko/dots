package com.aeolus.dots.root.presenter

import com.aeolus.dots.root.view.RootView

interface RootPresenter {
    fun onInit()
    fun onViewCreated(rootView: RootView)
    fun onBackPressed()
}