package com.aeolus.dots.root.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.aeolus.dots.custom_views.LoadingView
import com.aeolus.dots.custom_views.NetworkProblemsView
import com.aeolus.dots.root.presenter.RootPresenter
import com.aeolus.dots.screens.ScreenManager
import javax.inject.Inject

abstract class RootActivity : AppCompatActivity(), RootView {

    protected var rootPresenter: RootPresenter? = null

    private var loadingView: LoadingView? = null

    private var networkProblemsView: NetworkProblemsView? = null

    protected var configurationChanged = false

    @Inject
    lateinit var screenManager: ScreenManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (lastCustomNonConfigurationInstance is RootPresenter) {
            rootPresenter = lastCustomNonConfigurationInstance as RootPresenter
            configurationChanged = true
        }
        screenManager.setCurrentContext(this)
    }

    override fun onRetainCustomNonConfigurationInstance(): Any {
        return if (rootPresenter != null) rootPresenter!! else Object()
    }

    override fun onResume() {
        super.onResume()
        deleteView(loadingView)
        loadingView = LoadingView(this)
        addOverlayingView(loadingView)
        deleteView(networkProblemsView)
        networkProblemsView = NetworkProblemsView(this)
        addOverlayingView(networkProblemsView)
        if (!configurationChanged)
            rootPresenter?.onInit()
    }

    override fun onBackPressed() {
        rootPresenter?.onBackPressed()
    }

    fun inflatePresenter(presenter: RootPresenter) {
        this.rootPresenter = presenter
        presenter.onViewCreated(this)
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun startLoading() {
        loadingView?.startLoading()
    }

    override fun stopLoading() {
        loadingView?.stopLoading()
    }

    override fun showNoNetworkProblem(message: String, onRetry: () -> Unit) {
        networkProblemsView?.show(message, onRetry)
    }

    override fun hideNoNetworkProblem() {
        networkProblemsView?.hide()
    }

    override fun exitApp() {
        val homeIntent = Intent(Intent.ACTION_MAIN)
        homeIntent.addCategory(Intent.CATEGORY_HOME)
        homeIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(homeIntent)
    }

    private fun deleteView(view: View?) {
        if (view == null)
            return
        val vg = window.decorView.rootView as ViewGroup
        vg.removeView(view)
    }

    private fun addOverlayingView(view: View?) {
        if (view == null)
            return
        val vg = window.decorView.rootView as ViewGroup
        vg.addView(
            view, ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
    }

}
