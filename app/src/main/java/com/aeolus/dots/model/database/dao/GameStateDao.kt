package com.aeolus.dots.model.database.dao

import androidx.room.*
import com.aeolus.dots.entities.GameState
import com.aeolus.dots.model.database.DBScheme
import io.reactivex.Single

@Dao
interface GameStateDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveGameState(gameState: GameState): Single<Long>

    @Delete
    fun deleteGameState(gameState: GameState): Single<Void>

    @Query("SELECT * FROM ${DBScheme.Tables.GameState.NAME} WHERE ${DBScheme.Tables.GameState.Columns.GAME_ID} == :gameId")
    fun getGameStatesByGameId(gameId: Long): Single<List<GameState>>

    @Query(
        "SELECT * FROM ${DBScheme.Tables.GameState.NAME} " +
                "WHERE ${DBScheme.Tables.GameState.Columns.GAME_ID} == :gameId " +
                "AND ${DBScheme.Tables.GameState.Columns.TURN_NUMBER} = " +
                "(SELECT MAX(${DBScheme.Tables.GameState.Columns.TURN_NUMBER}) " +
                "FROM ${DBScheme.Tables.GameState.NAME} " +
                "WHERE ${DBScheme.Tables.GameState.Columns.GAME_ID} == :gameId)"
    )
    fun getLatestStateByGameId(gameId: Long): Single<GameState>

}