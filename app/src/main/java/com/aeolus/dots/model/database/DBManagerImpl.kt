package com.aeolus.dots.model.database

import android.content.Context
import com.aeolus.dots.entities.Game
import com.aeolus.dots.entities.GameState
import com.aeolus.dots.entities.templates.Template
import com.aeolus.dots.entities.tree.Node
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class DBManagerImpl(appContext: Context) : DBManager {

    private val db: AppDatabase = AppDatabase.getInstance(appContext)

    override fun saveGameState(gameState: GameState): Single<Long> {
        return db.gameStateDao().saveGameState(gameState).subscribeOn(Schedulers.io())
    }

    override fun deleteGameState(gameState: GameState): Single<Void> {
        return db.gameStateDao().deleteGameState(gameState).subscribeOn(Schedulers.io())
    }

    override fun getGameStatesByGameId(gameId: Long): Single<List<GameState>> {
        return db.gameStateDao().getGameStatesByGameId(gameId).subscribeOn(Schedulers.io())
    }

    override fun getLatestStateByGameId(id: Long): Single<GameState> {
        return db.gameStateDao().getLatestStateByGameId(id).subscribeOn(Schedulers.io())
    }

    override fun updateLastTurnTime(gameID: Long, currentTimeMillis: Long): Single<Void> {
        return db.gameDao().updateLastTurnTime(gameID, currentTimeMillis).subscribeOn(Schedulers.io())
    }

    override fun saveGame(game: Game): Single<Long> {
        return db.gameDao().saveGame(game).subscribeOn(Schedulers.io())
    }

    override fun deleteGame(game: Game): Single<Void> {
        return db.gameDao().deleteGame(game).subscribeOn(Schedulers.io())
    }

    override fun getGameById(id: Long): Single<Game> {
        return db.gameDao().getGameById(id).subscribeOn(Schedulers.io())
    }

    override fun getLatestGame(): Single<Game> {
        return db.gameDao().getLatestGame().subscribeOn(Schedulers.io())
    }

    override fun getAllGames(): Single<List<Game>> {
        return db.gameDao().getAllGames().subscribeOn(Schedulers.io())
    }

}