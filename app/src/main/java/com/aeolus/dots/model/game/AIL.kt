package com.aeolus.dots.model.game

import com.aeolus.dots.entities.Chain
import com.aeolus.dots.entities.GameState
import com.aeolus.dots.entities.Player
import com.aeolus.dots.entities.templates.Template
import com.aeolus.dots.entities.templates.TemplateFieldSideType
import com.aeolus.dots.entities.templates.TemplateRotationType
import com.aeolus.dots.entities.templates.TemplateUtils
import com.aeolus.dots.entities.tree.GameTree
import com.aeolus.dots.entities.tree.Node
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.roundToInt
import kotlin.math.sqrt

class AIL {

    companion object {

        private const val HUMAN: Int = 1
        private const val AI: Int = 2

        fun move(
            gameState: GameState,
            nodesManager: HashMap<Long, Node>,
            templates: MutableList<Template>
        ): Single<GameState> {
            return getAIMove(gameState, nodesManager, templates).flatMap {
                var move = it
                if (move.first < 0 || move.first >= gameState.board.size || move.second < 0 || move.second >= gameState.board[0].size)
                    move = getRandomMove(gameState)
                GameLogic.putDot(move.first, move.second, gameState.aiPlayer!!, gameState).flatMap {
                    if (GameLogic.canGround(gameState.board, gameState.aiPlayer!!)) {
                        val tGameState = gameState.clone()
                        GameLogic.ground(tGameState, tGameState.aiPlayer!!)
                        if (tGameState.getBestPlayer() == tGameState.aiPlayer!!) GameLogic.ground(
                            gameState,
                            gameState.aiPlayer!!
                        )
                    }
                    Single.just(gameState)
                }
            }.subscribeOn(Schedulers.computation())
        }

        private fun getAIMove(
            gameState: GameState,
            nodesManager: HashMap<Long, Node>,
            templates: MutableList<Template>
        ): Single<Pair<Int, Int>> {
            val level = 10
            return GameLogic.findBestMove(gameState.aiPlayer!!, gameState).flatMap { bestMove ->
                Single.fromCallable {
                    if (bestMove.second > 0)
                        return@fromCallable bestMove.first
                    val moveByTree = getMoveByTree(gameState, nodesManager)
                    if (moveByTree != null)
                        return@fromCallable moveByTree
                    searchForChains(gameState)
                    var move: Pair<Int, Int>?
                    move = getMoveByTemplateType(
                        gameState,
                        TemplateUtils.BEGIN,
                        Pair(gameState.addedCell.column, gameState.addedCell.row),
                        nodesManager,
                        templates
                    )
                    if (move != null)
                        return@fromCallable move
                    try {
                        if (TemplateFieldSideType.getFieldSideType(
                                gameState,
                                Pair(gameState.addedCell.column, gameState.addedCell.row)
                            ) != TemplateFieldSideType.templateFieldSideTypeINSIDE
                        ) {
                            move = getMoveByTemplateType(
                                gameState,
                                TemplateUtils.SQUARE_SIDE,
                                Pair(gameState.addedCell.column, gameState.addedCell.row),
                                nodesManager,
                                templates
                            )
                            if (move != null)
                                return@fromCallable move
                        }
                    } catch (e: Exception) {
                    }
                    try {
                        move = getMoveByTemplateType(
                            gameState,
                            TemplateUtils.SQUARE,
                            Pair(gameState.addedCell.column, gameState.addedCell.row),
                            nodesManager,
                            templates
                        )
                        if (move != null)
                            return@fromCallable move
                    } catch (e: Exception) {
                    }
                    if (!gameState.aiState!!.canGround) {
                        for (i in 0 until gameState.columns) {
                            for (j in 0 until gameState.rows) {
                                gameState.aiState!!.fieldSpectrumAI[i][j] = 0.0
                                gameState.aiState!!.fieldSpectrumHuman[i][j] = 0.0
                            }
                        }
                        val depth = 5
                        for (i in 0 until gameState.columns) {
                            for (j in 0 until gameState.rows) {
                                if (gameState.board[i][j].dot?.player == gameState.aiPlayer!!) {
                                    for (i1 in -depth..depth) {
                                        for (j1 in -depth..depth) {
                                            if ((i1 == 0) and (j1 == 0)) continue
                                            try {
                                                gameState.aiState!!.fieldSpectrumAI[i + i1][j + j1] += 1.0 / Math.sqrt((i1 * i1 + j1 * j1).toDouble())
                                            } catch (e: Exception) {
                                                continue
                                            }
                                        }
                                    }
                                } else if (gameState.board[i][j].dot?.player == gameState.aiOpponent!!) {
                                    for (i1 in -depth..depth) {
                                        for (j1 in -depth..depth) {
                                            if ((i1 == 0) and (j1 == 0)) continue
                                            try {
                                                gameState.aiState!!.fieldSpectrumHuman[i + i1][j + j1] += 1.0 / sqrt((i1 * i1 + j1 * j1).toDouble())
                                            } catch (e: Exception) {
                                                continue
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        gameState.aiState!!.fieldSpectrumAIMax = 0.0
                        gameState.aiState!!.fieldSpectrumHumanMax = 0.0
                        for (i in 0 until gameState.columns) {
                            for (j in 0 until gameState.rows) {
                                if (gameState.board[i][j].dot?.player == gameState.aiOpponent!!) {
                                    gameState.aiState!!.fieldSpectrumHuman[i][j] = Integer.MAX_VALUE.toDouble()
                                    continue
                                }
                                if (gameState.board[i][j].dot?.player == gameState.aiPlayer!!) {
                                    gameState.aiState!!.fieldSpectrumAI[i][j] = Integer.MAX_VALUE.toDouble()
                                    continue
                                }
                                if (gameState.aiState!!.fieldSpectrumHuman[i][j] > gameState.aiState!!.fieldSpectrumHumanMax)
                                    gameState.aiState!!.fieldSpectrumHumanMax =
                                        gameState.aiState!!.fieldSpectrumHuman[i][j]
                                if (gameState.aiState!!.fieldSpectrumAI[i][j] > gameState.aiState!!.fieldSpectrumAIMax)
                                    gameState.aiState!!.fieldSpectrumAIMax = gameState.aiState!!.fieldSpectrumAI[i][j]
                            }
                        }
                    }
                    gameState.aiState!!.closestAIChainIdx = getSmallestAIChainId(gameState)
                    gameState.aiState!!.canGround = gameState.aiState!!.closestAIChainIdx == -1
                    if (!gameState.aiState!!.canGround) {
                        move = getAIOrientedMove(
                            nodesManager,
                            templates,
                            gameState,
                            gameState.aiState!!.aiChains!![gameState.aiState!!.closestAIChainIdx],
                            level
                        )
                        if (move != null)
                            return@fromCallable move
                    }
                    for (i in gameState.aiState!!.aiChains!!.indices) {
                        if (!gameState.aiState!!.canGround) {
                            if (i == gameState.aiState!!.closestAIChainIdx) {
                                continue
                            }
                        }
                        move = getAIOrientedMove(
                            nodesManager,
                            templates,
                            gameState,
                            gameState.aiState!!.aiChains!![i],
                            level
                        )
                        if (move != null)
                            return@fromCallable move
                    }
                    move = getGroundingMove(
                        gameState,
                        nodesManager,
                        templates
                    )
                    return@fromCallable move ?: getRandomMove(gameState)
                }
            }
        }

        private fun getAIOrientedMove(
            nodesManager: HashMap<Long, Node>,
            templates: MutableList<Template>,
            gameState: GameState,
            chain: Chain,
            level: Int
        ): Pair<Int, Int>? {
            var move: Pair<Int, Int>? = getMoveByTemplateType(
                gameState,
                TemplateUtils.WALL_DESTROY,
                Pair(chain.chainEndX, chain.chainEndY),
                nodesManager,
                templates
            )
            if (move != null) return move
            move = getMoveByTemplateType(
                gameState,
                TemplateUtils.FINAL_AI_ATTACK,
                Pair(chain.chainEndX, chain.chainEndY),
                nodesManager,
                templates
            )
            if (move != null) return move
            move = getMoveByPoints(
                gameState,
                TemplateUtils.WALL_DESTROY,
                chain.points,
                nodesManager,
                templates
            )
            if (move != null) return move
            move = getMoveByPoints(
                gameState,
                TemplateUtils.FINAL_AI_ATTACK,
                chain.points,
                nodesManager,
                templates
            )
            if (move != null) return move
            move = getMoveByPoints(
                gameState,
                TemplateUtils.CONTINUED_AI_ATTACK,
                chain.points,
                nodesManager,
                templates
            )
            if (move != null) return move
            if (gameState.aiState!!.currentMove > 20) {
                for (i in chain.abstractPoints.indices.reversed()) {
                    try {
                        move = getMoveByTemplateType(
                            gameState,
                            TemplateUtils.ABSTRACT_ATTACK_WALL,
                            chain.abstractPoints[i],
                            nodesManager,
                            templates
                        )
                        if (move != null) {
                            chain.abstractPoints.add(
                                Pair(
                                    move.first,
                                    move.second
                                )
                            )
                            return move
                        }
                    } catch (e: Exception) {
                    }

                }
                for (i in chain.points.size downTo 0) {
                    move = getMoveByTemplateType(
                        gameState,
                        TemplateUtils.ABSTRACT_ATTACK_WALL,
                        chain.points[i],
                        nodesManager,
                        templates
                    )
                    if (move != null) {
                        chain.abstractPoints.add(
                            Pair(
                                move.first,
                                move.second
                            )
                        )
                        return move
                    }
                }
            }

            if (!chain.isAtSide(gameState)) {
                val k = 1.0 + (10 - level) / 10.0
                var s = 0.0
                while (s <= gameState.aiState!!.fieldSpectrumHumanMax) {
                    var i = 0
                    while (i < chain.points.size / k) {
                        if (gameState.aiState!!.fieldSpectrumHuman[chain.points[i].first][chain.points[i].second] < s || gameState.aiState!!.fieldSpectrumHuman[chain.points[i].first][chain.points[i].second] > s + gameState.aiState!!.fieldSpectrumHumanMax / 10.0) {
                            i++
                            continue
                        }
                        try {
                            move = getMoveByChain(
                                gameState,
                                nodesManager,
                                templates,
                                chain.points[i].first,
                                chain.points[i].second
                            )
                            if (move != null) return move
                        } catch (e: Exception) {
                        }
                        i++
                    }
                    s += gameState.aiState!!.fieldSpectrumHumanMax / 10.0
                }
            }
            return null
        }

        private fun getGroundingMove(
            gameState: GameState,
            nodesManager: HashMap<Long, Node>,
            templates: MutableList<Template>
        ): Pair<Int, Int>? {
            for (i in gameState.aiState!!.aiChains!!.indices) {
                val move = getMoveByPoints(
                    gameState,
                    TemplateUtils.GROUND,
                    gameState.aiState!!.aiChains!![i].points,
                    nodesManager,
                    templates
                )
                if (move != null) return move
            }
            for (i in gameState.aiState!!.aiChains!!.indices) {
                for (j in gameState.aiState!!.aiChains!![i].points.indices) {
                    val x = gameState.aiState!!.aiChains!![i].points[j].first
                    val y = gameState.aiState!!.aiChains!![i].points[j].second
                    if (x < 7 || x > gameState.columns - 8 || y < 7 || y > gameState.rows - 8) {
                        val move = getMoveByTemplateType(
                            gameState,
                            TemplateUtils.GROUND_SIDE,
                            Pair(x, y),
                            nodesManager,
                            templates
                        )
                        if (move != null) return move
                    }
                }
            }
            return null
        }

        private fun getMoveByTree(gameState: GameState, nodesManager: HashMap<Long, Node>): Pair<Int, Int>? {
            for (i in gameState.aiState!!.gameTree.indices.reversed()) {
                val ta = gameState.aiState!!.gameTree[i]
                var isMoveOnlyByDefault = false
                if (abs(ta.center.first - Pair(gameState.addedCell.column, gameState.addedCell.row).first) > 7 ||
                    abs(ta.center.second - Pair(gameState.addedCell.column, gameState.addedCell.row).second) > 7
                ) isMoveOnlyByDefault = true
                try {
                    if (i != gameState.aiState!!.gameTree.size - 1) {
                        if (!ta.templateAppearence.situationEquals(
                                Pair(gameState.addedCell.column, gameState.addedCell.row),
                                TemplateUtils.isSide(ta.templateAppearence.templateType),
                                TemplateFieldSideType.getFieldSideType(
                                    gameState,
                                    Pair(gameState.addedCell.column, gameState.addedCell.row)
                                ),
                                gameState.getFieldState()
                            )
                        ) {
                            gameState.aiState!!.gameTree.removeAt(i)
                            continue
                        }
                    }
                    if (levelMoveExists(
                            Pair(gameState.addedCell.column, gameState.addedCell.row),
                            isMoveOnlyByDefault,
                            ta,
                            gameState,
                            nodesManager
                        )
                    ) {
                        val move = ta.transformAIPoint!!
                        if (GameLogic.canPutDot(move.first, move.second, gameState)) return move
                        gameState.aiState!!.gameTree.removeAt(i)
                    }
                } catch (e: Exception) {
                    return null
                }
            }
            return null
        }

        private fun getMoveByChain(
            gameState: GameState,
            nodesManager: HashMap<Long, Node>,
            templates: MutableList<Template>,
            x: Int,
            y: Int
        ): Pair<Int, Int>? {
            if (x < 7 || x > gameState.columns - 8 || y < 7 || y > gameState.rows - 8) {
                val move = getMoveByTemplateType(
                    gameState,
                    TemplateUtils.WALL_SIDE,
                    Pair(x, y),
                    nodesManager,
                    templates
                )
                if (move != null) return move
                if ((x > 3) and (y > 3) and (x < gameState.columns - 4) and (y < gameState.rows - 4)) {
                    return getMoveByTemplateType(
                        gameState,
                        TemplateUtils.TypeWALL,
                        Pair(x, y),
                        nodesManager,
                        templates
                    )
                }
            } else {
                return getMoveByTemplateType(
                    gameState,
                    TemplateUtils.TypeWALL,
                    Pair(x, y),
                    nodesManager,
                    templates
                )
            }
            return null
        }

        private fun searchForChains(gameState: GameState) {
            gameState.aiState!!.fieldOfChains = Array(gameState.columns) { Array(gameState.rows) { "N" } }
            gameState.aiState!!.aiChains = ArrayList()
            gameState.aiState!!.humanChains = ArrayList()
            gameState.aiState!!.crossCount = 0
            for (i in 0 until gameState.columns - 1) {
                for (j in 0 until gameState.rows - 1) {
                    if (gameState.board[i][j].dot?.player == gameState.aiPlayer!! && gameState.board[i + 1][j + 1].dot?.player == gameState.aiPlayer!! && gameState.board[i + 1][j].dot?.player == gameState.aiOpponent!! && gameState.board[i][j + 1].dot?.player == gameState.aiOpponent!!) {
                        gameState.aiState!!.crossCount++
                        addNewWallPoints(gameState, i, j, "R")
                        addNewWallPoints(gameState, i + 1, j, "B")
                        addNewWallPoints(gameState, i + 1, j + 1, "R")
                        addNewWallPoints(gameState, i, j + 1, "B")
                    } else if (gameState.board[i][j].dot?.player == gameState.aiOpponent!! && gameState.board[i + 1][j + 1].dot?.player == gameState.aiOpponent!! && gameState.board[i + 1][j].dot?.player == gameState.aiPlayer!! && gameState.board[i][j + 1].dot?.player == gameState.aiPlayer!!) {
                        gameState.aiState!!.crossCount++
                        addNewWallPoints(gameState, i + 1, j, "R")
                        addNewWallPoints(gameState, i, j, "B")
                        addNewWallPoints(gameState, i, j + 1, "R")
                        addNewWallPoints(gameState, i + 1, j + 1, "B")
                    }
                }
            }
            if (gameState.aiState!!.crossCount == 0) {
                var point = getCentralPointForChainSearch(
                    gameState,
                    gameState.aiPlayer!!
                )
                if (point != null) addNewWallPoints(
                    gameState,
                    point.first,
                    point.second,
                    "R"
                )
                point = getCentralPointForChainSearch(
                    gameState,
                    gameState.aiOpponent!!
                )
                if (point != null) addNewWallPoints(
                    gameState,
                    point.first,
                    point.second,
                    "B"
                )
            }
            for (i in gameState.aiState!!.humanChains!!.indices) {
                addPointsToChain(
                    gameState,
                    gameState.aiState!!.humanChains!![i],
                    gameState.aiOpponent!!
                )
            }
            for (i in gameState.aiState!!.aiChains!!.indices) {
                addPointsToChain(
                    gameState,
                    gameState.aiState!!.aiChains!![i],
                    gameState.aiPlayer!!
                )
            }
            for (i in gameState.aiState!!.humanChains!!.indices) {
                gameState.aiState!!.humanChains!![i].searchForChainEnd(gameState, gameState.aiState!!.fieldOfChains!!)
            }
            for (i in gameState.aiState!!.aiChains!!.indices) {
                gameState.aiState!!.aiChains!![i].searchForChainEnd(gameState, gameState.aiState!!.fieldOfChains!!)
            }
        }

        private fun getSmallestAIChainId(gameState: GameState): Int {
            var count = 0
            for (i in gameState.aiState!!.aiChains!!.indices) {
                if (gameState.aiState!!.aiChains!![i].isAtSide(gameState)) count++
            }
            if (count == gameState.aiState!!.aiChains!!.size) return -1
            if (count == gameState.aiState!!.aiChains!!.size - 1)
                for (i in gameState.aiState!!.aiChains!!.indices) {
                    if (!gameState.aiState!!.aiChains!![i].isAtSide(gameState)) return i
                }
            var minLengthFromLastBlue = java.lang.Double.MAX_VALUE
            var smaller = 0
            for (i in gameState.aiState!!.aiChains!!.indices) {
                if (!gameState.aiState!!.aiChains!![i].isAtSide(gameState)) {
                    if (gameState.aiState!!.aiChains!![i].getLengthFromLastBlue(
                            Pair(
                                gameState.addedCell.column,
                                gameState.addedCell.row
                            )
                        ) <= minLengthFromLastBlue
                    ) {
                        minLengthFromLastBlue = gameState.aiState!!.aiChains!![i].getLengthFromLastBlue(
                            Pair(
                                gameState.addedCell.column,
                                gameState.addedCell.row
                            )
                        )
                        smaller = i
                    }
                }
            }
            return smaller
        }

        private fun addNewWallPoints(gameState: GameState, x: Int, y: Int, sign: String) {
            if (gameState.aiState!!.fieldOfChains!![x][y] != "N") {
                return
            }
            if (sign == "R") {
                gameState.aiState!!.aiChains!!.add(
                    Chain(
                        x,
                        y,
                        gameState.aiState!!.aiChains!!.size,
                        "R",
                        gameState.aiState!!.fieldOfChains!!
                    )
                )
                addWallPoints(
                    gameState,
                    gameState.aiState!!.aiChains!![gameState.aiState!!.aiChains!!.size - 1],
                    gameState.aiPlayer!!
                )
            } else {
                gameState.aiState!!.humanChains!!.add(
                    Chain(
                        x,
                        y,
                        gameState.aiState!!.humanChains!!.size,
                        "B",
                        gameState.aiState!!.fieldOfChains!!
                    )
                )
                addWallPoints(
                    gameState,
                    gameState.aiState!!.humanChains!![gameState.aiState!!.humanChains!!.size - 1],
                    gameState.aiOpponent!!
                )
            }
        }

        private fun addWallPoints(gameState: GameState, wall: Chain, sign: Player) {
            if (gameState.aiState?.fieldOfChains == null) return
            val antiSign: Player = if (sign == gameState.aiPlayer!!)
                gameState.aiOpponent!!
            else
                gameState.aiPlayer!!
            for (i in wall.points.indices) {
                val x = wall.points[i].first
                val y = wall.points[i].second
                try {
                    if (gameState.board[x - 1][y - 1].dot?.player == sign && (gameState.board[x - 1][y].dot?.player != antiSign || gameState.board[x][y - 1].dot?.player != antiSign))
                        wall.addPoint(x - 1, y - 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x - 1][y + 1].dot?.player == sign && (gameState.board[x - 1][y].dot?.player != antiSign || gameState.board[x][y + 1].dot?.player != antiSign))
                        wall.addPoint(x - 1, y + 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x + 1][y - 1].dot?.player == sign && (gameState.board[x + 1][y].dot?.player != antiSign || gameState.board[x][y - 1].dot?.player != antiSign))
                        wall.addPoint(x + 1, y - 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x + 1][y + 1].dot?.player == sign && (gameState.board[x + 1][y].dot?.player != antiSign || gameState.board[x][y + 1].dot?.player != antiSign))
                        wall.addPoint(x + 1, y + 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x + 1][y].dot?.player == sign)
                        wall.addPoint(x + 1, y, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x][y + 1].dot?.player == sign)
                        wall.addPoint(x, y + 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x - 1][y].dot?.player == sign)
                        wall.addPoint(x - 1, y, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x][y - 1].dot?.player == sign)
                        wall.addPoint(x, y - 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
            }
        }

        private fun addPointsToChain(gameState: GameState, wall: Chain, sign: Player) {
            if (gameState.aiState?.fieldOfChains == null) return
            val antiSign = if (sign == gameState.aiPlayer!!)
                gameState.aiOpponent!!
            else
                gameState.aiPlayer!!
            for (i in wall.points.indices) {
                val x = wall.points[i].first
                val y = wall.points[i].second
                try {
                    if (gameState.board[x - 1][y - 1].dot?.player == sign && (gameState.board[x - 1][y].dot?.player != antiSign || gameState.board[x][y - 1].dot?.player != antiSign))
                        wall.addPoint(x - 1, y - 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x - 1][y + 1].dot?.player == sign && (gameState.board[x - 1][y].dot?.player != antiSign || gameState.board[x][y + 1].dot?.player != antiSign))
                        wall.addPoint(x - 1, y + 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x + 1][y - 1].dot?.player == sign && (gameState.board[x + 1][y].dot?.player != antiSign || gameState.board[x][y - 1].dot?.player != antiSign))
                        wall.addPoint(x + 1, y - 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x + 1][y + 1].dot?.player == sign && (gameState.board[x + 1][y].dot?.player != antiSign || gameState.board[x][y + 1].dot?.player != antiSign))
                        wall.addPoint(x + 1, y + 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x + 1][y].dot?.player == sign)
                        wall.addPoint(x + 1, y, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x][y + 1].dot?.player == sign)
                        wall.addPoint(x, y + 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x - 1][y].dot?.player == sign)
                        wall.addPoint(x - 1, y, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x][y - 1].dot?.player == sign)
                        wall.addPoint(x, y - 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x + 1][y].dot == null && gameState.board[x + 2][y].dot?.player == sign)
                        wall.addPoint(x + 2, y, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x][y + 1].dot == null && gameState.board[x][y + 2].dot?.player == sign)
                        wall.addPoint(x, y + 2, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x - 1][y].dot == null && gameState.board[x - 2][y].dot?.player == sign)
                        wall.addPoint(x - 2, y, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x][y - 1].dot == null && gameState.board[x][y - 2].dot?.player == sign)
                        wall.addPoint(x, y - 2, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x + 1][y + 1].dot == null && gameState.board[x + 2][y + 2].dot?.player == sign)
                        wall.addPoint(x + 2, y + 2, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x - 1][y + 1].dot == null && gameState.board[x - 2][y + 2].dot?.player == sign)
                        wall.addPoint(x - 2, y + 2, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x + 1][y - 1].dot == null && gameState.board[x + 2][y - 2].dot?.player == sign)
                        wall.addPoint(x + 2, y - 2, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x - 1][y - 1].dot == null && gameState.board[x - 2][y - 2].dot?.player == sign)
                        wall.addPoint(x - 2, y - 2, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x + 1][y].dot == null || gameState.board[x + 1][y - 1].dot == null) && gameState.board[x + 2][y - 1].dot?.player == sign)
                        wall.addPoint(x + 2, y - 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x + 1][y].dot == null || gameState.board[x + 1][y + 1].dot == null) && gameState.board[x + 2][y + 1].dot?.player == sign)
                        wall.addPoint(x + 2, y + 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x][y + 1].dot == null || gameState.board[x + 1][y + 1].dot == null) && gameState.board[x + 1][y + 2].dot?.player == sign)
                        wall.addPoint(x + 1, y + 2, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x][y + 1].dot == null || gameState.board[x - 1][y + 1].dot == null) && gameState.board[x - 1][y + 2].dot?.player == sign)
                        wall.addPoint(x - 1, y + 2, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x - 1][y].dot == null || gameState.board[x - 1][y + 1].dot == null) && gameState.board[x - 2][y + 1].dot?.player == sign)
                        wall.addPoint(x - 2, y + 21, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x - 1][y].dot == null || gameState.board[x - 1][y - 1].dot == null) && gameState.board[x - 2][y - 1].dot?.player == sign)
                        wall.addPoint(x - 2, y - 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x][y - 1].dot == null || gameState.board[x - 1][y - 1].dot == null) && gameState.board[x - 1][y - 2].dot?.player == sign)
                        wall.addPoint(x - 1, y - 2, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x][y - 1].dot == null || gameState.board[x + 1][y - 1].dot == null) && gameState.board[x + 1][y - 2].dot?.player == sign)
                        wall.addPoint(x + 1, y - 2, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x + 1][y].dot == null && gameState.board[x + 2][y].dot == null && gameState.board[x + 3][y].dot?.player == sign)
                        wall.addPoint(x + 3, y, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x][y + 1].dot == null && gameState.board[x][y + 2].dot == null && gameState.board[x][y + 3].dot?.player == sign)
                        wall.addPoint(x, y + 3, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x - 1][y].dot == null && gameState.board[x - 2][y].dot == null && gameState.board[x - 3][y].dot?.player == sign)
                        wall.addPoint(x - 3, y, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x][y - 1].dot == null && gameState.board[x][y - 2].dot == null && gameState.board[x][y - 3].dot?.player == sign)
                        wall.addPoint(x, y - 3, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x + 1][y].dot == null && gameState.board[x + 2][y].dot == null && gameState.board[x + 3][y].dot == null && gameState.board[x + 4][y].dot?.player == sign)
                        wall.addPoint(x + 4, y, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x][y + 1].dot == null && gameState.board[x][y + 2].dot == null && gameState.board[x][y + 3].dot == null && gameState.board[x][y + 4].dot?.player == sign)
                        wall.addPoint(x, y + 4, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x - 1][y].dot == null && gameState.board[x - 2][y].dot == null && gameState.board[x - 3][y].dot == null && gameState.board[x - 4][y].dot?.player == sign)
                        wall.addPoint(x - 4, y, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if (gameState.board[x][y - 1].dot == null && gameState.board[x][y - 2].dot == null && gameState.board[x][y - 3].dot == null && gameState.board[x][y - 4].dot?.player == sign)
                        wall.addPoint(x, y - 4, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x + 1][y].dot == null || gameState.board[x + 1][y - 1].dot == null) && (gameState.board[x + 2][y].dot == null || gameState.board[x + 2][y - 1].dot == null) && gameState.board[x + 3][y - 1].dot?.player == sign)
                        wall.addPoint(x + 3, y - 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x + 1][y].dot == null || gameState.board[x + 1][y + 1].dot == null) && (gameState.board[x + 2][y].dot == null || gameState.board[x + 2][y + 1].dot == null) && gameState.board[x + 3][y + 1].dot?.player == sign)
                        wall.addPoint(x + 3, y + 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x][y + 1].dot == null || gameState.board[x + 1][y + 1].dot == null) && (gameState.board[x][y + 2].dot == null || gameState.board[x + 1][y + 2].dot == null) && gameState.board[x + 1][y + 3].dot?.player == sign)
                        wall.addPoint(x + 1, y + 3, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x][y + 1].dot == null || gameState.board[x - 1][y + 1].dot == null) && (gameState.board[x][y + 2].dot == null || gameState.board[x - 1][y + 2].dot == null) && gameState.board[x - 1][y + 3].dot?.player == sign)
                        wall.addPoint(x - 1, y + 3, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x - 1][y].dot == null || gameState.board[x - 1][y + 1].dot == null) && (gameState.board[x - 2][y].dot == null || gameState.board[x - 2][y + 1].dot == null) && gameState.board[x - 3][y + 1].dot?.player == sign)
                        wall.addPoint(x - 3, y + 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x - 1][y].dot == null || gameState.board[x - 1][y - 1].dot == null) && (gameState.board[x - 2][y].dot == null || gameState.board[x - 2][y - 1].dot == null) && gameState.board[x - 3][y - 1].dot?.player == sign)
                        wall.addPoint(x - 3, y - 1, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x][y - 1].dot == null || gameState.board[x - 1][y - 1].dot == null) && (gameState.board[x][y - 2].dot == null || gameState.board[x - 1][y - 2].dot == null) && gameState.board[x - 1][y - 3].dot?.player == sign)
                        wall.addPoint(x - 1, y - 3, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
                try {
                    if ((gameState.board[x][y - 1].dot == null || gameState.board[x + 1][y - 1].dot == null) && (gameState.board[x][y - 2].dot == null || gameState.board[x + 1][y - 2].dot == null) && gameState.board[x + 1][y - 3].dot?.player == sign)
                        wall.addPoint(x + 1, y - 3, gameState.aiState!!.fieldOfChains!!)
                } catch (e: IndexOutOfBoundsException) {
                }
            }
        }

        private fun getCentralPointForChainSearch(gameState: GameState, sign: Player): Pair<Int, Int>? {
            val centralX = (gameState.columns / 2.0).roundToInt()
            val centralY = (gameState.rows / 2.0).roundToInt()

            val depth = (max(gameState.columns, gameState.rows) / 2.0).roundToInt()

            for (i in 0..depth) {
                try {
                    if (gameState.board[centralX - i][centralY - i].dot?.player == sign) return Pair(
                        centralX - i,
                        centralY - i
                    )
                } catch (e: Exception) {
                }

                try {
                    if (gameState.board[centralX - i][centralY + i].dot?.player == sign) return Pair(
                        centralX - i,
                        centralY + i
                    )
                } catch (e: Exception) {
                }

                try {
                    if (gameState.board[centralX + i][centralY - i].dot?.player == sign) return Pair(
                        centralX + i,
                        centralY - i
                    )
                } catch (e: Exception) {
                }

                try {
                    if (gameState.board[centralX + i][centralY + i].dot?.player == sign) return Pair(
                        centralX + i,
                        centralY + i
                    )
                } catch (e: Exception) {
                }

                try {
                    if (gameState.board[centralX][centralY - i].dot?.player == sign) return Pair(
                        centralX,
                        centralY - i
                    )
                } catch (e: Exception) {
                }

                try {
                    if (gameState.board[centralX][centralY + i].dot?.player == sign) return Pair(
                        centralX,
                        centralY + i
                    )
                } catch (e: Exception) {
                }

                try {
                    if (gameState.board[centralX - i][centralY].dot?.player == sign) return Pair(
                        centralX - i,
                        centralY
                    )
                } catch (e: Exception) {
                }

                try {
                    if (gameState.board[centralX + i][centralY].dot?.player == sign) return Pair(
                        centralX + i,
                        centralY
                    )
                } catch (e: Exception) {
                }

                if (i > 0) {
                    for (j in 0 until i - 1) {
                        try {
                            if (gameState.board[centralX - i + j][centralY - i].dot?.player == sign)
                                return Pair(centralX - i + j, centralY - i)
                        } catch (e: Exception) {
                        }

                        try {
                            if (gameState.board[centralX - i][centralY + i - j].dot?.player == sign)
                                return Pair(centralX - i, centralY + i - j)
                        } catch (e: Exception) {
                        }

                        try {
                            if (gameState.board[centralX + i][centralY - i + j].dot?.player == sign)
                                return Pair(centralX + i, centralY - i + j)
                        } catch (e: Exception) {
                        }

                        try {
                            if (gameState.board[centralX + i - j][centralY + i].dot?.player == sign)
                                return Pair(centralX + i - j, centralY + i)
                        } catch (e: Exception) {
                        }

                        try {
                            if (gameState.board[centralX + j][centralY - i].dot?.player == sign)
                                return Pair(centralX + j, centralY - i)
                        } catch (e: Exception) {
                        }

                        try {
                            if (gameState.board[centralX - j][centralY + i].dot?.player == sign)
                                return Pair(centralX - j, centralY + i)
                        } catch (e: Exception) {
                        }

                        try {
                            if (gameState.board[centralX - i][centralY - j].dot?.player == sign)
                                return Pair(centralX - i, centralY - j)
                        } catch (e: Exception) {
                        }

                        try {
                            if (gameState.board[centralX + i][centralY + j].dot?.player == sign)
                                return Pair(centralX + i, centralY + j)
                        } catch (e: Exception) {
                        }

                    }
                }
            }
            return null
        }

        fun levelMoveExists(
            lastBluePoint: Pair<Int, Int>,
            isMoveOnlyByDefault: Boolean,
            gameTree: GameTree,
            gameState: GameState,
            nodesManager: HashMap<Long, Node>
        ): Boolean {
            if ((lastBluePoint.first != gameTree.center.first || lastBluePoint.second != gameTree.center.second) && !isMoveOnlyByDefault) {
                for (i in gameTree.currentNode!!.childNodes.indices) {
                    val parent = gameTree.currentNode!!.childNodes[i]
                    if (nodesManager[parent]!!.blueIsDefault) continue
                    if (nodesManager[parent]!!.moveType == AI) continue
                    for (j in nodesManager[parent]!!.points.indices) {
                        val p = TemplateRotationType.getTransformPoint(
                            gameTree.templateRotationType,
                            nodesManager[parent]!!.points[j]
                        )
                        if (gameTree.isSearchBySymmetry == 0 || gameTree.isSearchBySymmetry == 1) {
                            if (p.first + gameTree.center.first == lastBluePoint.first && p.second + gameTree.center.second == lastBluePoint.second) {
                                val n: Node = getRandomChildNode(
                                    gameState,
                                    nodesManager,
                                    gameTree,
                                    nodesManager[parent]!!,
                                    AI
                                ) ?: continue
                                val newPoint =
                                    TemplateRotationType.getTransformPoint(gameTree.templateRotationType, n.points[0])
                                gameTree.transformAIPoint =
                                    Pair(
                                        gameTree.center.first + newPoint.first,
                                        gameTree.center.second + newPoint.second
                                    )
                                gameTree.currentNode = n
                                gameTree.isSearchBySymmetry = 1
                                return true
                            }
                        }

                        if (gameTree.isSearchBySymmetry == 0 || gameTree.isSearchBySymmetry == 2) {
                            if (gameTree.tree!!.symmetryType == -1) continue
                            val pSymmetry = gameTree.tree!!.getSymmetryPoint(p, gameTree.templateRotationType)
                            if (pSymmetry.first + gameTree.center.first == lastBluePoint.first && pSymmetry.second + gameTree.center.second == lastBluePoint.second) {
                                val n: Node =
                                    getRandomChildNode(
                                        gameState,
                                        nodesManager,
                                        gameTree,
                                        nodesManager[parent]!!,
                                        AI
                                    ) ?: continue
                                val newPoint =
                                    TemplateRotationType.getTransformPoint(gameTree.templateRotationType, n.points[0])
                                val newPointSymmetry =
                                    gameTree.tree!!.getSymmetryPoint(newPoint, gameTree.templateRotationType)
                                gameTree.transformAIPoint = Pair(
                                    gameTree.center.first + newPointSymmetry.first,
                                    gameTree.center.second + newPointSymmetry.second
                                )
                                gameTree.currentNode = n
                                gameTree.isSearchBySymmetry = 2
                                return true
                            }
                        }
                    }
                }
            }
            for (i in gameTree.currentNode!!.childNodes.indices) {
                val parent = gameTree.currentNode!!.childNodes[i]
                if (!nodesManager[parent]!!.blueIsDefault) continue
                if (nodesManager[parent]!!.moveType == AI) continue
                val n: Node? = getRandomChildNode(gameState, nodesManager, gameTree, nodesManager[parent]!!, AI)
                var randomDouble = Random().nextDouble()
                if (gameTree.tree!!.symmetryType == -1) randomDouble = 0.6
                if (n == null) continue
                val newPoint = TemplateRotationType.getTransformPoint(gameTree.templateRotationType, n.points[0])
                if (gameTree.isSearchBySymmetry == 0) {
                    if (randomDouble > 0.5) {
                        gameTree.transformAIPoint =
                            Pair(gameTree.center.first + newPoint.first, gameTree.center.second + newPoint.second)
                        gameTree.isSearchBySymmetry = 1
                    } else {
                        val pSymmetry = gameTree.tree!!.getSymmetryPoint(newPoint, gameTree.templateRotationType)
                        gameTree.transformAIPoint =
                            Pair(gameTree.center.first + pSymmetry.first, gameTree.center.second + pSymmetry.second)
                        gameTree.isSearchBySymmetry = 2
                    }
                } else if (gameTree.isSearchBySymmetry == 1) {
                    gameTree.transformAIPoint =
                        Pair(gameTree.center.first + newPoint.first, gameTree.center.second + newPoint.second)
                } else if (gameTree.isSearchBySymmetry == 2) {
                    val pSymmetry = gameTree.tree!!.getSymmetryPoint(newPoint, gameTree.templateRotationType)
                    gameTree.transformAIPoint =
                        Pair(gameTree.center.first + pSymmetry.first, gameTree.center.second + pSymmetry.second)
                }
                gameTree.currentNode = n
                return true
            }
            return false
        }

        private fun getRandomChildNode(
            gameState: GameState,
            nodesManager: HashMap<Long, Node>,
            gameTree: GameTree,
            parentNode: Node,
            type: Int
        ): Node? {
            var childCount = 0
            var childCountWithoutConditions = 0
            for (i in parentNode.childNodes.indices) {
                if (nodesManager[parentNode.childNodes[i]]!!.moveType == type) childCount++
            }
            if (childCount == 0)
                return null
            else if (childCount == 1) {
                if (nodesManager[parentNode.childNodes[0]]?.moveType == type) {
                    return if (nodesManager[parentNode.childNodes[0]]?.condition != null) {
                        if (conditionSatisfied(
                                gameState,
                                gameTree,
                                nodesManager[parentNode.childNodes[0]]!!
                            )
                        )
                            nodesManager[parentNode.childNodes[0]]!!
                        else {
                            addSecurityMoves(
                                nodesManager,
                                gameTree,
                                parentNode
                            )
                            null
                        }
                    } else
                        nodesManager[parentNode.childNodes[0]]!!
                }
            } else {
                var maxLengthOfIfMoves = 0
                for (i in parentNode.childNodes.indices) {
                    if (nodesManager[parentNode.childNodes[i]]?.condition != null) {
                        if (nodesManager[parentNode.childNodes[i]]!!.condition!!.ifmoves.size > maxLengthOfIfMoves) {
                            maxLengthOfIfMoves = nodesManager[parentNode.childNodes[i]]!!.condition!!.ifmoves.size
                        }
                    }
                }
                var currentLengthOfIfMoves = 1
                while (currentLengthOfIfMoves <= maxLengthOfIfMoves) {
                    for (i in parentNode.childNodes.indices) {
                        if (nodesManager[parentNode.childNodes[i]]?.condition != null) {
                            if (
                                nodesManager[parentNode.childNodes[i]]!!.condition!!.ifmoves.size == currentLengthOfIfMoves && nodesManager[parentNode.childNodes[i]]!!.condition!!.enclosureType.equals(
                                    "r",
                                    ignoreCase = true
                                )
                            ) {
                                if (conditionSatisfied(
                                        gameState,
                                        gameTree,
                                        nodesManager[parentNode.childNodes[i]]!!
                                    )
                                ) {
                                    return nodesManager[parentNode.childNodes[i]]!!
                                }
                            }
                        }
                    }
                    for (i in parentNode.childNodes.indices) {
                        if (nodesManager[parentNode.childNodes[i]]?.condition != null) {
                            if (
                                nodesManager[parentNode.childNodes[i]]!!.condition!!.ifmoves.size == currentLengthOfIfMoves && nodesManager[parentNode.childNodes[i]]!!.condition!!.enclosureType.equals(
                                    "b",
                                    ignoreCase = true
                                )
                            ) {
                                if (conditionSatisfied(
                                        gameState,
                                        gameTree,
                                        nodesManager[parentNode.childNodes[i]]!!
                                    )
                                ) {
                                    return nodesManager[parentNode.childNodes[i]]!!
                                }
                            }
                        }
                    }
                    currentLengthOfIfMoves++
                }
                addSecurityMoves(
                    nodesManager,
                    gameTree,
                    parentNode
                )
                for (i in parentNode.childNodes.indices) {
                    if (nodesManager[parentNode.childNodes[i]]?.moveType == type && nodesManager[parentNode.childNodes[i]]?.condition == null)
                        childCountWithoutConditions++
                }

                val childIdx = Random().nextInt(childCountWithoutConditions)
                var findedIdx = 0
                for (i in parentNode.childNodes.indices) {
                    if (nodesManager[parentNode.childNodes[i]]?.moveType == type && nodesManager[parentNode.childNodes[i]]?.condition == null) {
                        if (childIdx == findedIdx) {
                            return nodesManager[parentNode.childNodes[i]]!!
                        } else {
                            findedIdx++
                        }
                    }
                }
            }
            return null
        }

        private fun conditionSatisfied(gs: GameState, gameTree: GameTree, node: Node): Boolean {
            val gameState = gs.clone()

            var pendingDot: Pair<Int, Int>? = null

            for (i in node.condition!!.ifmoves.indices) {

                if (node.condition!!.ifmoves[i].first == node.condition!!.move.first && node.condition!!.ifmoves[i].second == node.condition!!.move.second) {
                    pendingDot = TemplateRotationType.getTransformPoint(
                        gameTree.templateRotationType,
                        Pair(node.condition!!.move.first, node.condition!!.move.second)
                    )
                    pendingDot =
                        Pair(pendingDot.first + gameTree.center.first, pendingDot.second + gameTree.center.second)
                    continue
                }

                var p =
                    TemplateRotationType.getTransformPoint(gameTree.templateRotationType, node.condition!!.ifmoves[i])
                p = Pair(p.first + gameTree.center.first, p.second + gameTree.center.second)

                GameLogic.putDot(
                    p.first,
                    p.second,
                    if (node.condition!!.enclosureType == "b") gameState.aiOpponent!! else gameState.aiPlayer!!,
                    gameState
                ).subscribe()
            }

            if (pendingDot != null) {
                GameLogic.putDot(
                    pendingDot.first,
                    pendingDot.second,
                    if (node.condition!!.enclosureType == "b") gameState.aiOpponent!! else gameState.aiPlayer!!,
                    gameState
                ).subscribe()
            }

            if (node.condition!!.enclosureType == "b") {
                return gameState.lines.filter { it.color == gameState.aiOpponent!!.color }.size > gameState.lines.filter { it.color == gameState.aiOpponent!!.color }.size
            } else if (node.condition!!.enclosureType == "r") {
                return gameState.lines.filter { it.color == gameState.aiPlayer!!.color }.size > gameState.lines.filter { it.color == gameState.aiPlayer!!.color }.size
            }
            return false
        }

        private fun addSecurityMoves(nodesManager: HashMap<Long, Node>, gameTree: GameTree, parentNode: Node) {
            for (i in parentNode.childNodes.indices) {
                if (nodesManager[parentNode.childNodes[i]]?.condition != null) {
                    if ((nodesManager[parentNode.childNodes[i]]!!.condition!!.enclosureType == "b") and (nodesManager[parentNode.childNodes[i]]!!.condition!!.ifmoves.size == 1)) {
                        var p = Pair(
                            nodesManager[parentNode.childNodes[i]]!!.condition!!.move.first,
                            nodesManager[parentNode.childNodes[i]]!!.condition!!.move.second
                        )
                        p = TemplateRotationType.getTransformPoint(gameTree.templateRotationType, p)
                        p = Pair(p.first + gameTree.center.first, p.second + gameTree.center.second)
//                        nodesManager.securityPoints.addChild(p) ToDo
                    }
                }
            }
        }

        private fun getMoveByPoints(
            gameState: GameState,
            templateType: Int,
            pointList: MutableList<Pair<Int, Int>>,
            nodesManager: HashMap<Long, Node>,
            baseOfTemplates: MutableList<Template>
        ): Pair<Int, Int>? {
            for (i in baseOfTemplates.indices) {
                try {
                    val idx = if (TemplateUtils.isTemplateBaseSearchFromStartIdx(templateType))
                        i
                    else
                        baseOfTemplates.size - 1 - i
                    if (baseOfTemplates[idx].templateType != templateType) {
                        continue
                    }
                    val res = baseOfTemplates[idx].getMoveByPoints(
                        gameState,
                        nodesManager,
                        pointList,
                        gameState.getFieldState(),
                        templateType
                    )
                    if (res != null) return res
                } catch (e: Exception) {
                }
            }
            return null
        }

        private fun getMoveByTemplateType(
            gameState: GameState,
            templateType: Int,
            point: Pair<Int, Int>,
            nodesManager: HashMap<Long, Node>,
            baseOfTemplates: MutableList<Template>
        ): Pair<Int, Int>? {
            for (i in baseOfTemplates.indices) {
                try {
                    val idx =
                        if (TemplateUtils.isTemplateBaseSearchFromStartIdx(
                                templateType
                            )
                        ) i else baseOfTemplates.size - 1 - i
                    if (baseOfTemplates[idx].templateType != templateType) {
                        continue
                    }
                    val result = baseOfTemplates[idx].getByTemplateType(
                        gameState,
                        nodesManager,
                        point,
                        gameState.getFieldState(),
                        templateType
                    )
                    if (result != null) return result
                } catch (e: Exception) {
                }
            }
            return null
        }

        private fun getRandomMove(gameState: GameState): Pair<Int, Int> {
            val rand = Random()
            var move: Pair<Int, Int>
            do {
                move = Pair(rand.nextInt(gameState.columns), rand.nextInt(gameState.rows))
            } while (!GameLogic.canPutDot(
                    move.first,
                    move.second,
                    gameState
                )
            )
            println("Random move (${move.first},${move.second})")
            return move
        }

        const val MAX_SIZE: Int = 15
        const val DOT_ANY: Int = 0
        const val DOT_LAND: Int = 1
        const val DOT_EMPTY: Int = 2
        const val DOT_HUMAN: Int = 3
        const val DOT_HUMAN_or_EMPTY: Int = 4
        const val DOT_AI: Int = 5
        const val DOT_AI_OR_EMPTY: Int = 6
    }
}
