package com.aeolus.dots.model.shared_preferences

import android.content.Context
import android.preference.PreferenceManager
import android.provider.Settings

class SharedPreferencesManagerImpl(private val context: Context) :
    SharedPreferencesManager {

    companion object {
        private const val FIREBASE_DEVICE_TOKEN_KEY = "com.aeolus.dots.FIREBASE_DEVICE_TOKEN_KEY"
    }

    override fun saveDeviceToken(deviceToken: String) {
        PreferenceManager
            .getDefaultSharedPreferences(context)
            .edit()
            .putString(FIREBASE_DEVICE_TOKEN_KEY, deviceToken)
            .apply()
    }

    override fun getDeviceToken(): String {
        return PreferenceManager.getDefaultSharedPreferences(context)
            .getString(FIREBASE_DEVICE_TOKEN_KEY, "") ?: ""

    }

}