package com.aeolus.dots.model.shared_preferences

interface SharedPreferencesManager {
    fun saveDeviceToken(deviceToken: String)
    fun getDeviceToken(): String
}