package com.aeolus.dots.model.database

class DBScheme {

    companion object {
        const val NAME = "DotsDB"
    }

    class Tables {

        class Game {
            companion object {
                const val NAME = "game"
            }

            class Columns {
                companion object {
                    const val ID = "id"
                    const val PLAYERS = "players"
                    const val DATE_STARTED = "date_started"
                    const val DATE_LAST_TURN = "date_last_turn"
                }
            }
        }

        class GameState {
            companion object {
                const val NAME = "game_state"
            }

            class Columns {
                companion object {
                    const val ID = "id"
                    const val GAME_ID = "gameId"
                    const val TURN_NUMBER = "turn_number"
                    const val BOARD = "board"
                    const val RULES = "rules"
                    const val GAME_FINISHED = "game_finished"
                    const val PLAYERS = "players"
                    const val LINES = "lines"
                    const val CELL_TO_PREVIEW = "cell_to_preview"
                    const val AI_STATE = "ai_state"
                }
            }
        }

    }
}