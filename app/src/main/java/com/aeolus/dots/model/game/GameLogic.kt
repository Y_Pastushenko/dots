package com.aeolus.dots.model.game

import com.aeolus.dots.entities.*
import io.reactivex.Single

class GameLogic {

    companion object {

        fun canPutDot(column: Int, row: Int, gameState: GameState): Boolean {
            val cell = gameState.board[column][row]
            return !(cell.hasDot() || cell.isSurrounded())
        }

        fun putDot(column: Int, row: Int, player: Player, gameState: GameState): Single<GameState> {
            return Single.fromCallable {
                gameState.board[column][row].dot = DotsFactory.getDot(player)
                gameState.addedCell = gameState.board[column][row]
                if (canCloseCircle(column, row, gameState))
                    tryToPerformSurrounding(column, row, gameState)
                if (
                    gameState.rules.housesAllowed &&
                    gameState.board[column][row].isInHouse() &&
                    gameState.board[column][row].inHouseBy?.dot?.player != null &&
                    gameState.board[column][row].inHouseBy!!.dot!!.player != gameState.board[column][row].dot?.player
                ) {
                    tryToPerformSurrounding(
                        gameState.board[column][row].inHouseBy!!.column,
                        gameState.board[column][row].inHouseBy!!.row,
                        gameState
                    )
                }
                if (gameState.gameFinished) return@fromCallable gameState
                var hasEmptyDots = false
                for (c in gameState.board) {
                    if (hasEmptyDots)
                        break
                    for (cell in c)
                        if (!(cell.hasDot() || cell.isSurrounded())) {
                            hasEmptyDots = true
                            break
                        }
                }
                if (!hasEmptyDots) {
                    gameState.gameFinished = true
                    return@fromCallable gameState
                }
                gameState.turnNumber++
                return@fromCallable gameState
            }
        }

        private fun tryToPerformSurrounding(column: Int, row: Int, gameState: GameState) {
            val i = getGroupOfCells8(
                column,
                row,
                gameState.board
            ) { it.dot?.player == gameState.board[column][row].dot?.player && !it.isSurrounded() }
            val surrounded = getSurroundedCells(i, gameState)
            val allSurrounded = mutableListOf<Cell>()
            surrounded.removeAll { it.isSurrounded() }
            allSurrounded.addAll(surrounded)
            surrounded.removeAll { it.dot?.player == gameState.board[column][row].dot?.player }
            when {
                surrounded.any { it.hasDot() } -> surround(
                    gameState,
                    surrounded,
                    allSurrounded,
                    gameState.board[column][row]
                )
                gameState.rules.housesAllowed -> for (cell in surrounded) cell.inHouseBy = gameState.board[column][row]
                else -> surround(gameState, surrounded, allSurrounded, gameState.board[column][row])
            }
        }

        private fun surround(
            gameState: GameState,
            surrounded: MutableList<Cell>,
            allSurrounded: MutableList<Cell>,
            initialCell: Cell
        ) {
            for (cell in surrounded) {
                cell.surroundedBy = initialCell
                cell.inHouseBy = null
            }
            val i = getLines(initialCell, allSurrounded, gameState)
            gameState.lines.addAll(i)
            val currentPlayer = initialCell.dot?.player
            if (currentPlayer != null)
                gameState.setPlayerScore(
                    currentPlayer,
                    gameState.getPlayerScore(currentPlayer) +
                            surrounded.filter { it.hasDot() }.size +
                            surrounded.filter { !it.hasDot() }.size * gameState.rules.emptyFieldCost
                )
            if (gameState.rules.scoreUntil > 0 && gameState.getCurrentPlayerScore() >= gameState.rules.scoreUntil) {
                gameState.gameFinished = true
//            onGameFinished(gameState)
            }
        }

        private fun getGroupOfCells8(
            column: Int,
            row: Int,
            board: Array<Array<Cell>>,
            predicate: (Cell) -> Boolean
        ): MutableList<Cell> {
            val res: MutableSet<Cell> = mutableSetOf()
            var toSee: MutableSet<Cell> = mutableSetOf()
            val seen: MutableSet<Cell> = mutableSetOf()
            val initCell = board[column][row]
            res.add(initCell)
            toSee.add(initCell)
            while (toSee.isNotEmpty()) {
                val tl: MutableSet<Cell> = mutableSetOf()
                tl.addAll(toSee)
                for (c in toSee) {
                    seen.add(c)
                    var sd = get8SurroundingCells(c.column, c.row, board, predicate)
                    sd = sd.filter { d -> !seen.contains(d) }
                    res.addAll(sd)
                    tl.addAll(sd)
                    tl.remove(c)
                }
                toSee = tl
            }
            return res.toMutableList()
        }

        private fun getGroupOfCells4(
            column: Int,
            row: Int,
            board: Array<Array<Cell>>,
            predicate: (Cell) -> Boolean
        ): MutableList<Cell> {
            val res: MutableSet<Cell> = mutableSetOf()
            var toSee: MutableSet<Cell> = mutableSetOf()
            val seen: MutableSet<Cell> = mutableSetOf()
            val initCell: Cell = board[column][row]
            res.add(initCell)
            toSee.add(initCell)
            while (toSee.isNotEmpty()) {
                val tl: MutableSet<Cell> = mutableSetOf()
                tl.addAll(toSee)
                for (c in toSee) {
                    seen.add(c)
                    var sd = get4SurroundingCells(c.column, c.row, board, predicate)
                    sd = sd.filter { d -> !seen.contains(d) }
                    res.addAll(sd)
                    tl.addAll(sd)
                    tl.remove(c)
                }
                toSee = tl
            }
            return res.toMutableList()
        }

        private fun get8SurroundingCells(
            x: Int,
            y: Int,
            board: Array<Array<Cell>>,
            predicate: (Cell) -> Boolean
        ): List<Cell> {
            val cells: MutableList<Cell?> = mutableListOf()
            cells.add(if (y + 1 >= board[0].size) null else board[x][y + 1])
            cells.add(if (y + 1 >= board[0].size || x + 1 >= board.size) null else board[x + 1][y + 1])
            cells.add(if (x + 1 >= board.size) null else board[x + 1][y])
            cells.add(if (y <= 0 || x + 1 >= board.size) null else board[x + 1][y - 1])
            cells.add(if (y <= 0) null else board[x][y - 1])
            cells.add(if (y <= 0 || x <= 0) null else board[x - 1][y - 1])
            cells.add(if (x <= 0) null else board[x - 1][y])
            cells.add(if (y + 1 >= board[0].size || x <= 0) null else board[x - 1][y + 1])
            return cells.filterNotNull().filter { predicate(it) }
        }

        private fun get4SurroundingCells(
            column: Int,
            row: Int,
            board: Array<Array<Cell>>,
            predicate: (Cell) -> Boolean
        ): List<Cell> {
            val cells: MutableList<Cell?> = mutableListOf()
            cells.add(if (row + 1 >= board[0].size) null else board[column][row + 1])
            cells.add(if (column + 1 >= board.size) null else board[column + 1][row])
            cells.add(if (row <= 0) null else board[column][row - 1])
            cells.add(if (column <= 0) null else board[column - 1][row])
            return cells.filterNotNull().filter { predicate(it) }
        }

        private fun getSurroundedCells(by: MutableList<Cell>, gameState: GameState): MutableList<Cell> {
            val tBoard = createBoard(gameState.board.size + 2, gameState.board[0].size + 2)
            for (cell in by) {
                tBoard[cell.column + 1][cell.row + 1].dot = by[0].dot
            }
            val surrounding = getGroupOfCells4(0, 0, tBoard) { it.dot?.player != by[0].dot?.player }
            val res: MutableList<Cell> = mutableListOf()
            for (i in 0 until gameState.board.size + 2)
                for (j in 0 until gameState.board[0].size + 2)
                    if (!surrounding.contains(tBoard[i][j]) && tBoard[i][j].dot?.player != by[0].dot?.player) {
                        res.add(gameState.board[i - 1][j - 1])
                    }
            return res
        }

        private fun getLines(initialCell: Cell, surrounded: MutableList<Cell>, gameState: GameState): Array<Line> {
            var line: MutableSet<Cell> = mutableSetOf()
            for (cell in surrounded)
                line.addAll(
                    get4SurroundingCells(
                        cell.column,
                        cell.row,
                        gameState.board
                    ) { it.dot?.player == initialCell.dot?.player && !it.isSurrounded() }
                )
            val tBoard = createBoard(gameState.board.size + 2, gameState.board[0].size + 2)
            val tInitialCell = Cell(
                initialCell.column + 1,
                initialCell.row + 1,
                initialCell.dot,
                initialCell.surroundedBy,
                initialCell.inHouseBy
            )
            for (cell in line) {
                tBoard[cell.column + 1][cell.row + 1].dot = tInitialCell.dot
            }
            line = mutableSetOf()
            val surrounding = getGroupOfCells4(
                tBoard[0][0].column,
                tBoard[0][0].row,
                tBoard
            ) { it.dot?.player != tInitialCell.dot?.player }
            for (cell in surrounding)
                line.addAll(
                    get4SurroundingCells(
                        cell.column,
                        cell.row,
                        tBoard
                    ) { it.dot?.player == tInitialCell.dot?.player && !it.isSurrounded() }
                )
            val seen: MutableSet<Cell> = mutableSetOf()
            val res: MutableList<Line> = mutableListOf()
            var currentCell = tInitialCell
            var allSeen = true
            while (allSeen) {
                var next = get8SurroundingCells(
                    currentCell.column,
                    currentCell.row,
                    tBoard
                ) { it.dot?.player == tInitialCell.dot?.player && !seen.contains(it) && it != tInitialCell }
                if (next.isEmpty()) {
                    next = get8SurroundingCells(
                        currentCell.column,
                        currentCell.row,
                        tBoard
                    ) { it.dot?.player == tInitialCell.dot?.player && !seen.contains(it) }
                    if (next.isEmpty()) {
                        allSeen = false
                    } else {
                        res.add(
                            Line(
                                currentCell.column - 1,
                                currentCell.row - 1,
                                next[0].column - 1,
                                next[0].row - 1,
                                if (initialCell.dot != null) initialCell.dot!!.player.color else -1
                            )
                        )
                        currentCell = next[0]
                        if (currentCell != tInitialCell)
                            seen.add(currentCell)
                    }
                } else {
                    res.add(
                        Line(
                            currentCell.column - 1,
                            currentCell.row - 1,
                            next[0].column - 1,
                            next[0].row - 1,
                            if (initialCell.dot != null) initialCell.dot!!.player.color else -1
                        )
                    )
                    currentCell = next[0]
                    if (currentCell != tInitialCell)
                        seen.add(currentCell)
                }
            }
            return res.toTypedArray()
        }

        private fun canPutDotAndCloseCircle(x: Int, y: Int, player: Player, gameState: GameState): Boolean {
            if (!canPutDot(x, y, gameState)) return false
            val cells = get8SurroundingCells(x, y, gameState.board) { it.dot?.player == player }
            val size = cells.size
            if (size < 2 || size > 6) return false
            return true
        }

        private fun canCloseCircle(x: Int, y: Int, gameState: GameState): Boolean {
            val player = gameState.board[x][y].dot?.player ?: return false
            val cells = get8SurroundingCells(x, y, gameState.board) { it.dot?.player == player }
            val size = cells.size
            if (size < 2 || size > 6) return false
            return true
        }

        private fun createBoard(columns: Int, rows: Int): Array<Array<Cell>> {
            val res: MutableList<Array<Cell>> = mutableListOf()
            for (column in 0 until columns) {
                val l: MutableList<Cell> = mutableListOf()
                for (row in 0 until rows)
                    l.add(Cell(column, row, null, null, null))
                res.add(l.toTypedArray())
            }
            return res.toTypedArray()
        }

        private fun getMoveScore(x: Int, y: Int, player: Player, gameState: GameState): Single<Float> {
            val testState = gameState.clone()
            if (!canPutDotAndCloseCircle(x, y, player, testState)) return Single.just(0f)
            val score = testState.getPlayerScore(player)
            return putDot(x, y, player, testState).flatMap { Single.just(it.getPlayerScore(player) - score) }

        }

        fun findBestMove(player: Player, gameState: GameState): Single<Pair<Pair<Int, Int>, Float>?> {
            val list = mutableListOf<Pair<Int, Int>>()
            for (x in 0 until gameState.board.size)
                for (y in 0 until gameState.board[0].size)
                    if (canPutDotAndCloseCircle(x, y, player, gameState))
                        list.add(Pair(x, y))
            return if (list.isEmpty()) {
                Single.just(Pair(Pair(0, 0), 0f))
            } else {
                Single
                    .merge(list.map { pair ->
                        getMoveScore(
                            pair.first,
                            pair.second,
                            player,
                            gameState
                        ).flatMap {
                            Single.just(Pair(pair, it))
                        }
                    })
                    .toSortedList { o1, o2 -> o2.second.compareTo(o1.second) }
                    .flatMap {
                        Single.just(it[0])
                    }
            }
        }

        fun putInitialCross(gameState: GameState) {
            val firstX1 = (gameState.columns - 1) / 2
            val firstY1 = (gameState.rows - 1) / 2
            val firstX2 = firstX1 + 1
            val firstY2 = firstY1 + 1
            val secondX1 = firstX1
            val secondY1 = firstY1 + 1
            val secondX2 = firstX1 + 1
            val secondY2 = firstY1
            gameState.board[firstX1][firstY1].dot = DotsFactory.getDot(gameState.players[0].player)
            gameState.board[secondX1][secondY1].dot = DotsFactory.getDot(gameState.players[1].player)
            gameState.board[firstX2][firstY2].dot = DotsFactory.getDot(gameState.players[0].player)
            gameState.board[secondX2][secondY2].dot = DotsFactory.getDot(gameState.players[1].player)
            gameState.addedCell = gameState.board[secondX2][secondY2]
        }

        fun canGround(board: Array<Array<Cell>>, player: Player): Boolean {
            for (x in 0 until board.size) {
                var cell = board[x][0]
                if (cell.hasDot() && cell.dot!!.player == player)
                    return true
                cell = board[x][board[0].size - 1]
                if (cell.hasDot() && cell.dot!!.player == player)
                    return true
            }
            for (y in 0 until board[0].size) {
                var cell = board[0][y]
                if (cell.hasDot() && cell.dot!!.player == player)
                    return true
                cell = board[board.size - 1][y]
                if (cell.hasDot() && cell.dot!!.player == player)
                    return true
            }
            return false
        }

        fun ground(gameState: GameState, player: Player) {
            val tGameState = gameState.clone()
            tGameState.lines.clear()
            for (playerState in tGameState.players) playerState.score = 0f
            val tBoard = tGameState.board
            val cellsToGround: MutableList<Cell> = mutableListOf()
            for (x in 0 until tBoard.size) {
                var cell = tBoard[x][0]
                if (cell.hasDot() && cell.dot!!.player == player)
                    cellsToGround.add(cell)
                cell = tBoard[x][tBoard[0].size - 1]
                if (cell.hasDot() && cell.dot!!.player == player)
                    cellsToGround.add(cell)
            }
            for (y in 0 until tBoard[0].size) {
                var cell = tBoard[0][y]
                if (cell.hasDot() && cell.dot!!.player == player)
                    cellsToGround.add(cell)
                cell = tBoard[tBoard.size - 1][y]
                if (cell.hasDot() && cell.dot!!.player == player)
                    cellsToGround.add(cell)
            }
            if (cellsToGround.isEmpty()) return
            for (cell in cellsToGround)
                for (c in getGroupOfCells4(
                    cell.column,
                    cell.row,
                    tBoard
                ) { it.dot?.player == player })
                    tBoard[c.column][c.row].dot = null

            val opponent = gameState.players.find { it.player != player }?.player ?: return
            val unSurroundedPlayerCells: MutableList<Cell> = mutableListOf()
            for (column in tBoard)
                for (cell in column)
                    if (!cell.isSurrounded() && cell.hasDot() && cell.dot!!.player == player)
                        unSurroundedPlayerCells.add(cell)
            val playerCellsGroups: MutableList<MutableList<Cell>> = mutableListOf()
            while (unSurroundedPlayerCells.isNotEmpty()) {
                val cell = unSurroundedPlayerCells[0]
                val group = getGroupOfCells8(
                    cell.column,
                    cell.row,
                    tBoard
                ) { it.dot?.player == player && !it.isSurrounded() }
                unSurroundedPlayerCells.removeAll(group)
                playerCellsGroups.add(group)
            }
            val surroundedCells: MutableList<Pair<Cell, MutableList<Cell>>> = mutableListOf()
            val surroundingCells: MutableList<Cell> = mutableListOf()
            for (group in playerCellsGroups) {
                for (column in tBoard)
                    for (cell in column)
                        cell.dot = DotsFactory.getDot(opponent)
                for (cell in group)
                    tBoard[cell.column][cell.row].dot = DotsFactory.getDot(player)
                var initCell: Cell? = null
                for (column in tBoard)
                    for (cell in column)
                        if (cell.dot?.player == opponent) {
                            if (get4SurroundingCells(
                                    cell.column,
                                    cell.row,
                                    tBoard
                                ) { it.dot?.player == player }.isEmpty()
                            )
                                tBoard[cell.column][cell.row].dot = null
                            else
                                initCell = cell
                        }
                if (initCell == null) return
                tryToPerformSurrounding(initCell.column, initCell.row, tGameState)
                val surrounded: MutableList<Cell> = mutableListOf()
                for (column in tBoard)
                    for (cell in column) {
                        if (cell.isSurrounded())
                            surrounded.add(cell)
                        if (cell.dot?.player == opponent)
                            surroundingCells.add(cell)
                    }
                surroundedCells.add(Pair(surroundingCells.last(), surrounded))
            }
            for (cell in surroundingCells)
                gameState.board[cell.column][cell.row].dot = DotsFactory.getDot(opponent)
            for (pair in surroundedCells)
                for (cell in pair.second)
                    gameState.board[cell.column][cell.row].surroundedBy = pair.first
            gameState.lines.addAll(tGameState.lines)
            for (playerState in tGameState.players) gameState.setPlayerScore(
                playerState.player,
                gameState.getPlayerScore(playerState.player) + playerState.score
            )
            gameState.gameFinished = true
        }
    }
}
