package com.aeolus.dots.model.database.dao

import androidx.room.*
import com.aeolus.dots.entities.Game
import com.aeolus.dots.model.database.DBScheme
import io.reactivex.Single

@Dao
interface GameDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveGame(game: Game): Single<Long>

    @Delete
    fun deleteGame(game: Game): Single<Void>

    @Query("SELECT * FROM ${DBScheme.Tables.Game.NAME} WHERE ${DBScheme.Tables.Game.Columns.ID} == :id")
    fun getGameById(id: Long): Single<Game>

    @Query("SELECT * FROM ${DBScheme.Tables.Game.NAME}")
    fun getAllGames(): Single<List<Game>>

    @Query(
        "UPDATE ${DBScheme.Tables.Game.NAME} " +
                "SET ${DBScheme.Tables.Game.Columns.DATE_LAST_TURN} = :dateLastTurn " +
                "WHERE ${DBScheme.Tables.Game.Columns.ID} = :gameID"
    )
    fun updateLastTurnTime(gameID: Long, dateLastTurn: Long): Single<Void>

    @Query(
        "SELECT * FROM ${DBScheme.Tables.Game.NAME} " +
                "WHERE ${DBScheme.Tables.Game.Columns.DATE_LAST_TURN} = " +
                "(SELECT MAX(${DBScheme.Tables.Game.Columns.DATE_LAST_TURN}) FROM ${DBScheme.Tables.Game.NAME})"
    )
    fun getLatestGame(): Single<Game>
}