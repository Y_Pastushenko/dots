package com.aeolus.dots.model.database

import com.aeolus.dots.entities.Game
import com.aeolus.dots.entities.GameState
import com.aeolus.dots.entities.templates.Template
import com.aeolus.dots.entities.tree.Node
import io.reactivex.Single

interface DBManager {
    fun saveGame(game: Game): Single<Long>
    fun deleteGame(game: Game): Single<Void>
    fun getGameById(id: Long): Single<Game>
    fun getAllGames(): Single<List<Game>>
    fun saveGameState(gameState: GameState): Single<Long>
    fun deleteGameState(gameState: GameState): Single<Void>
    fun getGameStatesByGameId(gameId: Long): Single<List<GameState>>
    fun getLatestStateByGameId(id: Long): Single<GameState>
    fun updateLastTurnTime(gameID: Long, currentTimeMillis: Long): Single<Void>
    fun getLatestGame(): Single<Game>
}
