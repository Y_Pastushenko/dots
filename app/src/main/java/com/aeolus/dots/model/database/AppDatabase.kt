package com.aeolus.dots.model.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.aeolus.dots.entities.Game
import com.aeolus.dots.entities.GameState
import com.aeolus.dots.model.database.dao.GameDao
import com.aeolus.dots.model.database.dao.GameStateDao

@Database(
    entities = [
        Game::class,
        GameState::class
    ], version = 1, exportSchema = false
)
@TypeConverters(DotsTypeConverters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun gameDao(): GameDao

    abstract fun gameStateDao(): GameStateDao


    companion object {
        private var instance: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase {
            if (instance == null)
                instance = Room.databaseBuilder(context, AppDatabase::class.java, DBScheme.NAME).build()
            return instance as AppDatabase
        }
    }
}