package com.aeolus.dots.model.tree

import android.content.Context
import com.aeolus.dots.entities.templates.*
import com.aeolus.dots.entities.tree.Node
import com.aeolus.dots.entities.tree.Tree
import com.aeolus.dots.model.game.AIL
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.io.IOException

data class TreeManager(val context: Context) {

    private var nodes: HashMap<Long, Node>? = null

    private var templates: MutableList<Template>? = null

    fun getNodesAndTemplates(): Single<Pair<HashMap<Long, Node>, MutableList<Template>>> {
        if (nodes != null && templates != null) return Single.just(Pair(nodes!!, templates!!))
        return setUp().flatMap {
            nodes = it.first
            templates = it.second
            return@flatMap Single.just(it)
        }.subscribeOn(Schedulers.io())
    }

    private fun setUp(): Single<Pair<HashMap<Long, Node>, MutableList<Template>>> {
        return Single.fromCallable {
            val nodes: HashMap<Long, Node> = hashMapOf()
            val templates: MutableList<Template> = mutableListOf()
            val templateTypeCount = IntArray(TemplateUtils.lastIndexOfTemplateType)
            var templateCount = 0
            var treeCount = 0
            for (i in templateTypeCount.indices) {
                templateTypeCount[i] = 0
            }
            var str = getTemplates(context)
            while (str.isNotEmpty()) {
                val idx = str.indexOf("</" + TML.template + ">") + TML.template.length + 3
                val strTemplate = str.substring(0, idx)
                val template =
                    Template(0, 0, intArrayOf(), null, AIL.MAX_SIZE, arrayListOf())
                template.templateID = Integer.valueOf(
                    strTemplate.substring(
                        strTemplate.indexOf("<" + TML.id + ":") + TML.id.length + 2,
                        strTemplate.indexOf("<" + TML.id + ":") + TML.id.length + 7
                    )
                )
                template.templateType = TemplateUtils.getTemplateType(
                    strTemplate.substring(
                        strTemplate.indexOf("<" + TML.type + ":") + TML.type.length + 2,
                        strTemplate.indexOf("<" + TML.type + ":") + TML.type.length + 5
                    )
                )
                val content = strTemplate.substring(
                    strTemplate.indexOf("<" + TML.content + ":") + TML.content.length + 2,
                    strTemplate.indexOf("<" + TML.content + ":") + TML.content.length + AIL.MAX_SIZE * AIL.MAX_SIZE + 2
                )
                template.templateContent = IntArray(AIL.MAX_SIZE * AIL.MAX_SIZE)
                for (i in 0 until AIL.MAX_SIZE * AIL.MAX_SIZE) {
                    template.templateContent[i] = (content.substring(i, i + 1)).toInt()
                }
                if (strTemplate.contains("<" + TML.tree + ">") and strTemplate.contains("</" + TML.tree + ">")) {
                    template.tree = Tree(
                    ).setTree(
                        strTemplate.substring(
                            strTemplate.indexOf("<" + TML.tree + ">") + TML.tree.length + 2,
                            strTemplate.indexOf("</" + TML.tree + ">")
                        ),
                        nodes
                    )
                }
                template.templateAppearance = ArrayList()
                template.templateAppearance.add(
                    TemplateAppearence(
                        template.sizeWithNotAny,
                        template.templateType,
                        TemplateRotationType.templateRotationTypeR0,
                        TemplateRotationType.getTransformArray(
                            TemplateRotationType.templateRotationTypeR0,
                            template.templateContent
                        )
                    )
                )
                val strTemplate180 =
                    TemplateRotationType.getTransformArray(
                        TemplateRotationType.templateRotationTypeR180,
                        template.templateContent
                    )
                if (template.isEqualsLikeTemplateView(
                        strTemplate180,
                        template.templateContent
                    )
                ) {
                } else {
                    template.templateAppearance.add(
                        TemplateAppearence(
                            template.sizeWithNotAny,
                            template.templateType,
                            TemplateRotationType.templateRotationTypeR180,
                            TemplateRotationType.getTransformArray(
                                TemplateRotationType.templateRotationTypeR180,
                                template.templateContent
                            )
                        )
                    )
                }

                val strTemplateHorizontal =
                    TemplateRotationType.getTransformArray(
                        TemplateRotationType.templateRotationTypeGORIZONTAL,
                        template.templateContent
                    )
                if (template.isEqualsLikeTemplateView(
                        strTemplateHorizontal,
                        template.templateContent
                    )
                    || template.isEqualsLikeTemplateView(strTemplateHorizontal, strTemplate180)
                ) {
                } else {
                    template.templateAppearance.add(
                        TemplateAppearence(
                            template.sizeWithNotAny,
                            template.templateType,
                            TemplateRotationType.templateRotationTypeGORIZONTAL,
                            TemplateRotationType.getTransformArray(
                                TemplateRotationType.templateRotationTypeGORIZONTAL,
                                template.templateContent
                            )
                        )
                    )
                }

                val strTemplateVertical =
                    TemplateRotationType.getTransformArray(
                        TemplateRotationType.templateRotationTypeVERTICAL,
                        template.templateContent
                    )
                if (template.isEqualsLikeTemplateView(
                        strTemplateVertical,
                        template.templateContent
                    )
                    || template.isEqualsLikeTemplateView(strTemplateVertical, strTemplate180)
                    || template.isEqualsLikeTemplateView(strTemplateVertical, strTemplateHorizontal)
                ) {
                } else {
                    template.templateAppearance.add(
                        TemplateAppearence(
                            template.sizeWithNotAny,
                            template.templateType,
                            TemplateRotationType.templateRotationTypeVERTICAL,
                            TemplateRotationType.getTransformArray(
                                TemplateRotationType.templateRotationTypeVERTICAL,
                                template.templateContent
                            )
                        )
                    )
                }

                val strTemplate90 =
                    TemplateRotationType.getTransformArray(
                        TemplateRotationType.templateRotationTypeR90,
                        template.templateContent
                    )
                if (template.isEqualsLikeTemplateView(
                        strTemplate90,
                        template.templateContent
                    )
                    || template.isEqualsLikeTemplateView(strTemplate90, strTemplate180)
                    || template.isEqualsLikeTemplateView(strTemplate90, strTemplateHorizontal)
                    || template.isEqualsLikeTemplateView(strTemplate90, strTemplateVertical)
                ) {
                } else {
                    template.templateAppearance.add(
                        TemplateAppearence(
                            template.sizeWithNotAny,
                            template.templateType,
                            TemplateRotationType.templateRotationTypeR90,
                            TemplateRotationType.getTransformArray(
                                TemplateRotationType.templateRotationTypeR90,
                                template.templateContent
                            )
                        )
                    )
                }

                val strTemplate270 =
                    TemplateRotationType.getTransformArray(
                        TemplateRotationType.templateRotationTypeR270,
                        template.templateContent
                    )
                if (template.isEqualsLikeTemplateView(
                        strTemplate270,
                        template.templateContent
                    )
                    || template.isEqualsLikeTemplateView(strTemplate270, strTemplate180)
                    || template.isEqualsLikeTemplateView(strTemplate270, strTemplateHorizontal)
                    || template.isEqualsLikeTemplateView(strTemplate270, strTemplateVertical)
                    || template.isEqualsLikeTemplateView(strTemplate270, strTemplate90)
                ) {

                } else {
                    template.templateAppearance.add(
                        TemplateAppearence(
                            template.sizeWithNotAny,
                            template.templateType,
                            TemplateRotationType.templateRotationTypeR270,
                            TemplateRotationType.getTransformArray(
                                TemplateRotationType.templateRotationTypeR270,
                                template.templateContent
                            )
                        )
                    )
                }

                val strTemplateGorizontal90 = TemplateRotationType.getTransformArray(
                    TemplateRotationType.templateRotationTypeGORIZONTAL90,
                    template.templateContent
                )
                if (template.isEqualsLikeTemplateView(
                        strTemplateGorizontal90,
                        template.templateContent
                    )

                    || template.isEqualsLikeTemplateView(strTemplateGorizontal90, strTemplate180)
                    || template.isEqualsLikeTemplateView(strTemplateGorizontal90, strTemplateHorizontal)
                    || template.isEqualsLikeTemplateView(strTemplateGorizontal90, strTemplateVertical)
                    || template.isEqualsLikeTemplateView(strTemplateGorizontal90, strTemplate90)
                    || template.isEqualsLikeTemplateView(strTemplateGorizontal90, strTemplate270)
                ) {
                } else {
                    template.templateAppearance.add(
                        TemplateAppearence(
                            template.sizeWithNotAny,
                            template.templateType,
                            TemplateRotationType.templateRotationTypeGORIZONTAL90,
                            TemplateRotationType.getTransformArray(
                                TemplateRotationType.templateRotationTypeGORIZONTAL90,
                                template.templateContent
                            )
                        )
                    )
                }

                val strTemplateVertical90 =
                    TemplateRotationType.getTransformArray(
                        TemplateRotationType.templateRotationTypeVERTICAL90,
                        template.templateContent
                    )
                if (template.isEqualsLikeTemplateView(
                        strTemplateVertical90,
                        template.templateContent
                    )

                    || template.isEqualsLikeTemplateView(strTemplateVertical90, strTemplate180)
                    || template.isEqualsLikeTemplateView(strTemplateVertical90, strTemplateHorizontal)
                    || template.isEqualsLikeTemplateView(strTemplateVertical90, strTemplateVertical)
                    || template.isEqualsLikeTemplateView(strTemplateVertical90, strTemplate90)
                    || template.isEqualsLikeTemplateView(strTemplateVertical90, strTemplate270)
                    || template.isEqualsLikeTemplateView(strTemplateVertical90, strTemplateGorizontal90)
                ) {
                } else {
                    template.templateAppearance.add(
                        TemplateAppearence(
                            template.sizeWithNotAny,
                            template.templateType,
                            TemplateRotationType.templateRotationTypeVERTICAL90,
                            TemplateRotationType.getTransformArray(
                                TemplateRotationType.templateRotationTypeVERTICAL90,
                                template.templateContent
                            )
                        )
                    )
                }

                if (!TemplateUtils.isSide(template.templateType)) {

                    template.sizeWithNotAny = 15
                    var notAnyCount = 0
                    for (i in 0..14) if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                    for (i in 210..224) if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                    run {
                        var i = 15
                        while (i < 210) {
                            if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                            i += 15
                        }
                    }
                    run {
                        var i = 29
                        while (i < 224) {
                            if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                            i += 15
                        }
                    }

                    if (notAnyCount == 0) {
                        template.sizeWithNotAny = 13
                        notAnyCount = 0
                        for (i in 16..28) if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                        for (i in 196..208) if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                        run {
                            var i = 31
                            while (i < 196) {
                                if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                i += 15
                            }
                        }
                        run {
                            var i = 43
                            while (i < 208) {
                                if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                i += 15
                            }
                        }

                        if (notAnyCount == 0) {
                            template.sizeWithNotAny = 11
                            notAnyCount = 0
                            for (i in 32..42) if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                            for (i in 182..192)
                                if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                            run {
                                var i = 47
                                while (i < 182) {
                                    if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                    i += 15
                                }
                            }
                            run {
                                var i = 57
                                while (i < 192) {
                                    if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                    i += 15
                                }
                            }

                            if (notAnyCount == 0) {
                                template.sizeWithNotAny = 9
                                notAnyCount = 0
                                for (i in 48..56)
                                    if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                for (i in 168..176)
                                    if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                run {
                                    var i = 63
                                    while (i < 168) {
                                        if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                        i += 15
                                    }
                                }
                                run {
                                    var i = 71
                                    while (i < 176) {
                                        if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                        i += 15
                                    }
                                }

                                if (notAnyCount == 0) {
                                    template.sizeWithNotAny = 7
                                    notAnyCount = 0
                                    for (i in 64..70)
                                        if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                    for (i in 154..160)
                                        if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                    run {
                                        var i = 79
                                        while (i < 154) {
                                            if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                            i += 15
                                        }
                                    }
                                    run {
                                        var i = 85
                                        while (i < 160) {
                                            if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                            i += 15
                                        }
                                    }

                                    if (notAnyCount == 0) {
                                        template.sizeWithNotAny = 5
                                        notAnyCount = 0
                                        for (i in 80..84)
                                            if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                        for (i in 140..144)
                                            if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                        run {
                                            var i = 95
                                            while (i < 136) {
                                                if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                                i += 15
                                            }
                                        }
                                        var i = 99
                                        while (i < 140) {
                                            if (template.templateContent[i] != AIL.DOT_ANY) notAnyCount++
                                            i += 15
                                        }
                                        if (notAnyCount == 0) template.sizeWithNotAny = 3
                                    }
                                }
                            }
                        }
                    }
                } else {
                    template.sizeWithNotAny = 15
                    when {
                        template.templateContent[114] == AIL.DOT_LAND -> template.sizeWithNotAny = 5
                        template.templateContent[115] == AIL.DOT_LAND -> template.sizeWithNotAny = 7
                        template.templateContent[116] == AIL.DOT_LAND -> template.sizeWithNotAny = 9
                        template.templateContent[117] == AIL.DOT_LAND -> template.sizeWithNotAny = 11
                        template.templateContent[118] == AIL.DOT_LAND -> template.sizeWithNotAny = 13
                        template.templateContent[119] == AIL.DOT_LAND -> template.sizeWithNotAny = 15
                    }
                }

                templates.add(template)
                templateCount++
                if (template.tree != null) {
                    treeCount++
                }
                templateTypeCount[template.templateType]++
                str = str.substring(idx)
            }
            return@fromCallable Pair(nodes, templates)
        }.subscribeOn(Schedulers.io())
    }

    private fun getTemplates(context: Context): String {
        var res = ""
        try {
            val stream = context.assets.open("template_base.txt")
            val size = stream.available()
            val buffer = ByteArray(size)
            stream.read(buffer)
            stream.close()
            res = String(buffer)
        } catch (e: IOException) {
        }

        return res
    }
}