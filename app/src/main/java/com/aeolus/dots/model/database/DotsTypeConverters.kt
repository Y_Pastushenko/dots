package com.aeolus.dots.model.database

import androidx.room.TypeConverter
import com.aeolus.dots.entities.*
import com.aeolus.dots.entities.templates.TemplateAppearence
import com.aeolus.dots.entities.tree.Tree
import com.aeolus.dots.entities.tree.TreeCondition
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class DotsTypeConverters {

    @TypeConverter
    fun boardToString(board: Array<Array<Cell>>): String {
        val list: MutableList<Cell> = mutableListOf()
        for (row in board)
            for (cell in row)
                if (cell.isSurrounded() || cell.hasDot())
                    list.add(cell)
        list.add(board[board.size - 1][board[0].size - 1])
        val type = object : TypeToken<List<Cell>>() {}.type
        return Gson().toJson(list, type)
    }

    @TypeConverter
    fun stringToBoard(json: String): Array<Array<Cell>> {
        val list: MutableList<Cell> = mutableListOf()
        val type = object : TypeToken<List<Cell>>() {}.type
        list.addAll(Gson().fromJson<List<Cell>>(json, type))
        val board = createBoard(list[list.size - 1].column + 1, list[list.size - 1].row + 1)
        for (cell in list) board[cell.column][cell.row] = cell
        return board
    }

    @TypeConverter
    fun playersToString(players: MutableList<Player>): String {
        val type = object : TypeToken<MutableList<Player>>() {}.type
        return Gson().toJson(players, type)
    }

    @TypeConverter
    fun stringToPlayers(json: String): MutableList<Player> {
        val type = object : TypeToken<MutableList<Player>>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun playersStateToString(players: MutableList<PlayerState>): String {
        val type = object : TypeToken<MutableList<PlayerState>>() {}.type
        return Gson().toJson(players, type)
    }

    @TypeConverter
    fun stringToPlayersState(json: String): MutableList<PlayerState> {
        val type = object : TypeToken<MutableList<PlayerState>>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun linesToString(players: MutableList<Line>): String {
        val type = object : TypeToken<MutableList<Line>>() {}.type
        return Gson().toJson(players, type)
    }

    @TypeConverter
    fun stringToLines(json: String): MutableList<Line> {
        val type = object : TypeToken<MutableList<Line>>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun cellToString(cell: Cell): String {
        val type = object : TypeToken<Cell>() {}.type
        return Gson().toJson(cell, type)
    }

    @TypeConverter
    fun stringToCell(json: String): Cell {
        val type = object : TypeToken<Cell>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun intArrayToString(value: IntArray?): String {
        val type = object : TypeToken<IntArray?>() {}.type
        return Gson().toJson(value, type)
    }

    @TypeConverter
    fun stringToIntArray(json: String): IntArray? {
        val type = object : TypeToken<IntArray?>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun templateViewListToString(value: List<TemplateAppearence>?): String {
        val type = object : TypeToken<List<TemplateAppearence>?>() {}.type
        return Gson().toJson(value, type)
    }

    @TypeConverter
    fun stringToTemplateViewList(json: String): List<TemplateAppearence>? {
        val type = object : TypeToken<List<TemplateAppearence>?>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun vectorToString(value: Vector<Long>?): String {
        val type = object : TypeToken<Vector<Long>?>() {}.type
        return Gson().toJson(value, type)
    }

    @TypeConverter
    fun stringToVector(json: String): Vector<Long>? {
        val type = object : TypeToken<Vector<Long>?>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun intPairListToString(value: List<Pair<Int, Int>>?): String {
        val type = object : TypeToken<List<Pair<Int, Int>>?>() {}.type
        return Gson().toJson(value, type)
    }

    @TypeConverter
    fun stringToIntPairList(json: String): List<Pair<Int, Int>>? {
        val type = object : TypeToken<List<Pair<Int, Int>>?>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun longListToString(value: List<Long>?): String {
        val type = object : TypeToken<List<Long>?>() {}.type
        return Gson().toJson(value, type)
    }

    @TypeConverter
    fun stringToLongList(json: String): List<Long>? {
        val type = object : TypeToken<List<Long>?>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun treeToString(value: Tree?): String {
        val type = object : TypeToken<Tree?>() {}.type
        return Gson().toJson(value, type)
    }

    @TypeConverter
    fun stringToTree(json: String): Tree? {
        val type = object : TypeToken<Tree?>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun treeConditionToString(treeCondition: TreeCondition?): String {
        val type = object : TypeToken<TreeCondition?>() {}.type
        return Gson().toJson(treeCondition, type)
    }

    @TypeConverter
    fun stringToTreeCondition(json: String): TreeCondition? {
        val type = object : TypeToken<TreeCondition?>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun aiStateToString(aiState: AIState?): String {
        val type = object : TypeToken<AIState?>() {}.type
        return Gson().toJson(aiState, type)
    }

    @TypeConverter
    fun stringToAiState(json: String): AIState? {
        val type = object : TypeToken<AIState?>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun rulesToString(rules: Rules): String {
        val type = object : TypeToken<Rules>() {}.type
        return Gson().toJson(rules, type)
    }

    @TypeConverter
    fun stringToRules(json: String): Rules {
        val type = object : TypeToken<Rules>() {}.type
        return Gson().fromJson(json, type)
    }

    private fun createBoard(columns: Int, rows: Int): Array<Array<Cell>> {
        val res: MutableList<Array<Cell>> = mutableListOf()
        for (column in 0 until columns) {
            val l: MutableList<Cell> = mutableListOf()
            for (row in 0 until rows)
                l.add(Cell(column, row, null, null, null))
            res.add(l.toTypedArray())
        }
        return res.toTypedArray()
    }
}