package com.aeolus.dots.screens.menu

import com.aeolus.dots.entities.Game
import com.aeolus.dots.entities.Player
import com.aeolus.dots.root.view.RootView

interface MenuView : RootView{
    fun showGeneralMenu(showResume: Boolean)
    fun showNewGameMenu()
    fun addPlayerToList(player: Player)
    fun removePlayerFromList(player: Player)
    fun showStartGameButton()
    fun hideStartGameButton()
    fun showHistory(games: List<Game>)
    fun deleteGameFromHistory(game: Game)
}