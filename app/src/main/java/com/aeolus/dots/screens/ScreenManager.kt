package com.aeolus.dots.screens

import android.content.Context
import com.aeolus.dots.entities.Game
import com.aeolus.dots.entities.Player
import com.aeolus.dots.entities.Rules
import com.aeolus.dots.screens.game_screen.impl.GameScreenActivity
import com.aeolus.dots.screens.initial_loading.impl.InitialLoadingActivity
import com.aeolus.dots.screens.menu.impl.MenuActivity

class ScreenManager {

    private var currentContext: Context? = null

    fun setCurrentContext(context: Context) {
        currentContext = context
    }

    fun switchToInitialLoadingPage() {
        if (currentContext != null)
            InitialLoadingActivity.startActivity(currentContext!!)
    }

    fun switchToMainMenu() {
        if (currentContext != null)
            MenuActivity.startActivity(currentContext!!)
    }

    fun switchToGameScreen(
        players: MutableList<Player>,
        columns: Int,
        rows: Int,
        rules: Rules
    ) {
        if (currentContext != null) GameScreenActivity.startActivity(currentContext!!, players, columns, rows, rules)
    }

    fun switchToGameScreen(game: Game) {
        if (currentContext != null) GameScreenActivity.startActivity(currentContext!!, game)
    }
}