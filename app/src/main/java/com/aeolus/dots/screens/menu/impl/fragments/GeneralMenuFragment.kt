package com.aeolus.dots.screens.menu.impl.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.aeolus.dots.R
import com.aeolus.dots.screens.menu.impl.MenuActivity
import kotlinx.android.synthetic.main.fragment_general_menu.*

class GeneralMenuFragment : Fragment() {

    companion object {
        const val RESUME_BUTTON_SHOWN = "com.aeolus.dots.screens.menu.impl.fragments.RESUME_BUTTON_SHOWN"
    }

    private var showResume = false

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(RESUME_BUTTON_SHOWN, b_resume_game.visibility == VISIBLE)
        super.onSaveInstanceState(outState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        val resumeVisible = savedInstanceState?.getBoolean(RESUME_BUTTON_SHOWN, false)
        if (resumeVisible != null) b_resume_game.visibility = if (resumeVisible) VISIBLE else GONE
        super.onActivityCreated(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_general_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        b_resume_game.visibility = if (showResume) VISIBLE else GONE
        b_quick_game.setOnClickListener { getMenuActivity()?.presenter?.onQuickGamePressed() }
        b_new_game.setOnClickListener {
            getMenuActivity()?.presenter?.onNewGamePressed()
        }
        b_resume_game.setOnClickListener {
            getMenuActivity()?.presenter?.onResumeGamePressed()
        }
        b_history.setOnClickListener {
            getMenuActivity()?.presenter?.onHistoryPressed()
        }
    }

    private fun getMenuActivity(): MenuActivity? {
        return activity as MenuActivity
    }

    fun setResumeButtonVisibility(showResume: Boolean) {
        this.showResume = showResume
        b_resume_game?.visibility = if (showResume) VISIBLE else GONE
    }

    fun isResumeButtonShown(): Boolean {
        if (b_resume_game == null) return false
        return b_resume_game.visibility == VISIBLE
    }
}