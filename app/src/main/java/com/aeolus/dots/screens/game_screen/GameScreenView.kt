package com.aeolus.dots.screens.game_screen

import com.aeolus.dots.entities.GameState
import com.aeolus.dots.entities.Player
import com.aeolus.dots.root.view.RootView

interface GameScreenView : RootView {
    fun initBoard(gameState: GameState)
    fun showState(gameState: GameState)
    fun askForConfirmation()
    fun showGameFinishedMessage(gameState: GameState)
    fun showHistory(statesList: List<GameState>)
    fun hideHistory()
    fun showGroundButton(canGround: Boolean)
}