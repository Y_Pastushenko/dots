package com.aeolus.dots.screens.menu.impl

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.fragment.app.Fragment
import com.aeolus.dots.App
import com.aeolus.dots.R
import com.aeolus.dots.entities.Game
import com.aeolus.dots.entities.Player
import com.aeolus.dots.root.view.RootActivity
import com.aeolus.dots.screens.menu.MenuPresenter
import com.aeolus.dots.screens.menu.MenuView
import com.aeolus.dots.screens.menu.impl.fragments.GeneralMenuFragment
import com.aeolus.dots.screens.menu.impl.fragments.HistoryFragment
import com.aeolus.dots.screens.menu.impl.fragments.NewGameMenuFragment
import kotlinx.android.synthetic.main.activity_menu.*
import javax.inject.Inject


class MenuActivity : RootActivity(), MenuView {

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, MenuActivity::class.java))
        }
    }

    @Inject
    lateinit var presenter: MenuPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as App).component.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        setSupportActionBar(tb_menu)
        b_back.setOnClickListener { super.onBackPressed() }
        if (configurationChanged)
            presenter = rootPresenter as MenuPresenter
        inflatePresenter(presenter)
    }

    override fun onBackPressed() {
        val currentFragment = getCurrentFragment()
        if (currentFragment is NewGameMenuFragment) {
            if (currentFragment.colorPickerVisible()) {
                currentFragment.hideColorPicker()
                return
            }
        }
        super.onBackPressed()
    }

    override fun showNewGameMenu() {
        showFragment(NewGameMenuFragment())
        b_back.visibility = VISIBLE
    }

    override fun showHistory(games: List<Game>) {
        showFragment(HistoryFragment().setUp(games))
        b_back.visibility = VISIBLE
    }

    override fun showGeneralMenu(showResume: Boolean) {
        val fragment = GeneralMenuFragment()
        fragment.setResumeButtonVisibility(showResume)
        showFragment(fragment)
        b_back.visibility = GONE
    }

    override fun addPlayerToList(player: Player) {
        (getCurrentFragment() as? NewGameMenuFragment ?: return).addPlayerToList(player)
    }

    override fun removePlayerFromList(player: Player) {
        (getCurrentFragment() as? NewGameMenuFragment ?: return).removePlayerFromList(player)
    }

    override fun showStartGameButton() {
        (getCurrentFragment() as? NewGameMenuFragment ?: return).showStartGameButton()
    }

    override fun hideStartGameButton() {
        (getCurrentFragment() as? NewGameMenuFragment ?: return).hideStartGameButton()
    }

    override fun deleteGameFromHistory(game: Game) {
        (getCurrentFragment() as? HistoryFragment ?: return).removeGameFromList(game)
    }

    private fun showFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fl_menu_fragment_container, fragment, fragment.javaClass.name)
            .commit()
    }

    private fun getCurrentFragment(): Fragment? {
        return supportFragmentManager.findFragmentById(R.id.fl_menu_fragment_container)
    }

}
