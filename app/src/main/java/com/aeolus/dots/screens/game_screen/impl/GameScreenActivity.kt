package com.aeolus.dots.screens.game_screen.impl

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.fragment.app.DialogFragment
import com.aeolus.dots.App
import com.aeolus.dots.R
import com.aeolus.dots.entities.Game
import com.aeolus.dots.entities.GameState
import com.aeolus.dots.entities.Player
import com.aeolus.dots.entities.Rules
import com.aeolus.dots.root.view.RootActivity
import com.aeolus.dots.screens.game_screen.GameScreenPresenter
import com.aeolus.dots.screens.game_screen.GameScreenView
import kotlinx.android.synthetic.main.activity_game_screen.*
import javax.inject.Inject

class GameScreenActivity : RootActivity(), GameScreenView {

    companion object {

        private const val GAME_EXTRA_KEY = "com.aeolus.dots.screens.game_screen.impl.GAME_EXTRA_KEY"
        private const val PLAYERS_EXTRA_KEY = "com.aeolus.dots.screens.game_screen.impl.PLAYERS_EXTRA_KEY"
        private const val COLUMNS_EXTRA_KEY = "com.aeolus.dots.screens.game_screen.impl.COLUMNS_EXTRA_KEY"
        private const val ROWS_EXTRA_KEY = "com.aeolus.dots.screens.game_screen.impl.ROWS_EXTRA_KEY"
        private const val RULES_KEY = "com.aeolus.dots.screens.game_screen.impl.RULES_KEY"
        private const val CELL_PREVIEW_ON = "com.aeolus.dots.screens.game_screen.impl.CELL_PREVIEW_ON"
        private const val HISTORY_ON = "com.aeolus.dots.screens.game_screen.impl.HISTORY_ON"

        fun startActivity(
            context: Context,
            players: MutableList<Player>,
            columns: Int,
            rows: Int,
            rules: Rules
        ) {
            val intent = Intent(context, GameScreenActivity::class.java)
            intent.putExtra(PLAYERS_EXTRA_KEY, ArrayList(players))
            intent.putExtra(COLUMNS_EXTRA_KEY, columns)
            intent.putExtra(ROWS_EXTRA_KEY, rows)
            intent.putExtra(RULES_KEY, rules)
            context.startActivity(intent)
        }

        fun startActivity(context: Context, game: Game) {
            val intent = Intent(context, GameScreenActivity::class.java)
            intent.putExtra(GAME_EXTRA_KEY, game)
            context.startActivity(intent)
        }

    }

    @Inject
    lateinit var presenter: GameScreenPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as App).component.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_screen)
        b_history.setOnClickListener { presenter.onHistoryPressed() }
        b_exit.setOnClickListener { presenter.onExitClicked() }
        b_ground.setOnClickListener { presenter.onGroundPressed() }
        turn_selector.setClickListener { presenter.onTurnPress(it) }
        if (configurationChanged) {
            presenter = rootPresenter as GameScreenPresenter
            inflatePresenter(presenter)
            board.setOnCellClickListener { x: Int, y: Int -> presenter.onCellClicked(x, y) }
        } else {
            inflatePresenter(presenter)
            val game: Game? = intent.getParcelableExtra(GAME_EXTRA_KEY)
            if (game != null) {
                presenter.onGameDataRetrieved(game)
                return
            }
            val players: MutableList<Player> = intent.getParcelableArrayListExtra(PLAYERS_EXTRA_KEY)
            val columns = intent.getIntExtra(COLUMNS_EXTRA_KEY, -1)
            val rows = intent.getIntExtra(ROWS_EXTRA_KEY, -1)
            val rules = intent.getParcelableExtra<Rules>(RULES_KEY)
            if (players.isNotEmpty() && columns > 0 && rows > 0 && rules != null)
                presenter.onGameDataRetrieved(players, columns, rows, rules)
        }
    }

    override fun initBoard(gameState: GameState) {
        board.setup(gameState.board.size, gameState.board[0].size)
        board.showState(gameState)
        board.setOnCellClickListener { x: Int, y: Int -> presenter.onCellClicked(x, y) }
        score_view.setState(gameState)
    }

    override fun showState(gameState: GameState) {
        score_view.setState(gameState)
        board.showState(gameState)
    }

    override fun showHistory(statesList: List<GameState>) {
        turn_selector.setUp(statesList)
        turn_selector.visibility = VISIBLE
    }

    override fun hideHistory() {
        turn_selector.visibility = GONE
    }

    override fun askForConfirmation() {
        ll_confirm_click.visibility = VISIBLE
        b_ok.setOnClickListener {
            ll_confirm_click.visibility = GONE
            presenter.onCellClickConfirmationGot(true)
        }
        b_cancel.setOnClickListener {
            ll_confirm_click.visibility = GONE
            presenter.onCellClickConfirmationGot(false)
        }
    }

    override fun showGroundButton(canGround: Boolean) {
        b_ground.visibility = if (canGround) VISIBLE else GONE
    }

    override fun showGameFinishedMessage(gameState: GameState) {
        GameFinishedDialogFragment()
            .setup(gameState)
            .show(supportFragmentManager, GameFinishedDialogFragment::javaClass.name)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putBoolean(CELL_PREVIEW_ON, ll_confirm_click.visibility == VISIBLE)
        outState?.putBoolean(HISTORY_ON, turn_selector.visibility == VISIBLE)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        if (savedInstanceState == null) return
        if (savedInstanceState.getBoolean(CELL_PREVIEW_ON))
            askForConfirmation()
        if (savedInstanceState.getBoolean(HISTORY_ON))
            turn_selector.visibility = VISIBLE
    }

    class GameFinishedDialogFragment : DialogFragment() {

        companion object {

            private const val STATE_KEY =
                "com.aeolus.dots.screens.game_screen.impl.GameFinishedDialogFragment.STATE_KEY"
        }

        var gameState: GameState? = null

        fun setup(gameState: GameState): GameFinishedDialogFragment {
            this.gameState = gameState
            return this
        }

        override fun onSaveInstanceState(outState: Bundle) {
            outState.putParcelable(STATE_KEY, gameState)
            super.onSaveInstanceState(outState)
        }

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val restored: GameState? = savedInstanceState?.getParcelable(STATE_KEY)
            if (restored != null)
                this.gameState = restored
            return if (gameState == null)
                super.onCreateDialog(savedInstanceState)
            else {
                val winner = gameState!!.getBestPlayer()
                AlertDialog
                    .Builder(context)
                    .setTitle("Game finished")
                    .setMessage(if (winner == null) "draw!" else "${winner.name} won!")
                    .setPositiveButton("Ok") { dialog, _ -> dialog.dismiss() }
                    .create()
            }
        }

    }
}
