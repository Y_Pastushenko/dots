package com.aeolus.dots.screens.game_screen.impl

import android.os.Handler
import com.aeolus.dots.entities.*
import com.aeolus.dots.model.database.DBManager
import com.aeolus.dots.model.game.AIL
import com.aeolus.dots.model.game.GameLogic
import com.aeolus.dots.model.tree.TreeManager
import com.aeolus.dots.root.view.RootView
import com.aeolus.dots.screens.ScreenManager
import com.aeolus.dots.screens.game_screen.GameScreenPresenter
import com.aeolus.dots.screens.game_screen.GameScreenView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class GameScreenPresenterImpl(
    private val screenManager: ScreenManager,
    private val dbManager: DBManager,
    private val treeManager: TreeManager
) :
    GameScreenPresenter {

    private var view: GameScreenView? = null
    private var gameState: GameState? = null
    private var isWaitingForConfirmation = false
    private var isWaitingForAi = false
    private var isInHistoryMode = false
    private var backClickedBefore = false
    private val compositeDisposable = CompositeDisposable()

    override fun onViewCreated(rootView: RootView) {
        view = rootView as GameScreenView
    }

    override fun onGameDataRetrieved(
        players: MutableList<Player>,
        columns: Int,
        rows: Int,
        rules: Rules
    ) {
        val game = Game(players = players, dateStarted = System.currentTimeMillis(), dateLastTurn = 0)
        compositeDisposable.add(
            dbManager.saveGame(game)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ gameID ->
                    gameState = GameState(
                        gameID = gameID,
                        board = createBoard(columns, rows),
                        rules = rules,
                        players = players.map { player -> PlayerState(player, 0f) }.toMutableList(),
                        turnNumber = 0,
                        lines = mutableListOf(),
                        aiState = if (players.contains(Player.AI)) AIState(columns, rows) else null
                    )
                    if (rules.initialCross) GameLogic.putInitialCross(gameState!!)
                    onInit()
                }, {})
        )
    }

    private fun onGameFinished(gameState: GameState) {
//        gameFinished = true
        view?.showGameFinishedMessage(gameState)
    }

    override fun onGameDataRetrieved(game: Game) {
        compositeDisposable.add(
            dbManager
                .getLatestStateByGameId(game.id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { gameState, err ->
                    if (gameState != null) {
                        this.gameState = gameState
                        onInit()
                    }
                }
        )
    }

    override fun onInit() {
        if (gameState == null) return
        view?.initBoard(gameState!!)
        view?.showState(gameState!!)
        saveGameState(gameState!!)
        if (gameState!!.getCurrentPlayer() == Player.AI) {
            isWaitingForAi = gameState!!.getCurrentPlayer() == Player.AI
            if (isWaitingForAi) processAIMove()
        }
    }

    override fun onBackPressed() {
        if (backClickedBefore) exit()
        backClickedBefore = true
        view?.showMessage("Press back again to exit")
        Handler().postDelayed({ backClickedBefore = false }, 2000)
    }

    override fun onCellClicked(column: Int, row: Int) {
        if (gameState == null || gameState!!.gameFinished || isWaitingForConfirmation || isWaitingForAi || isInHistoryMode) return
        if (!GameLogic.canPutDot(column, row, gameState!!)) {
            view?.showMessage("Cannot put dot here")
            return
        }
        isWaitingForConfirmation = true
        gameState!!.addedCell =
            Cell(column, row, DotsFactory.getDot(gameState!!.getCurrentPlayer()), null, null)
        view?.showState(gameState!!)
        view?.askForConfirmation()
    }

    override fun onCellClickConfirmationGot(confirmed: Boolean) {
        if (gameState == null || gameState!!.addedCell == Cell.EMPTY) return
        if (confirmed) GameLogic.putDot(
            gameState!!.addedCell.column,
            gameState!!.addedCell.row,
            gameState!!.getCurrentPlayer(),
            gameState!!
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { gameState ->
                view?.showState(gameState)
                view?.showGroundButton(GameLogic.canGround(gameState.board, gameState.getCurrentPlayer()))
                saveGameState(gameState)
                if (gameState.gameFinished) onGameFinished(gameState) else {
                    isWaitingForAi = gameState.getCurrentPlayer() == Player.AI
                    if (isWaitingForAi) processAIMove()
                }
            }
        else {
            gameState!!.addedCell = Cell.EMPTY
            view?.showState(gameState!!)
        }
        isWaitingForConfirmation = false
    }

    override fun onGroundPressed() {
        GameLogic.ground(gameState!!, gameState!!.getCurrentPlayer())
        view?.showGroundButton(false)
        view?.showState(gameState!!)
        onGameFinished(gameState!!)
    }

    private fun processAIMove() {
        compositeDisposable.add(treeManager.getNodesAndTemplates().observeOn(AndroidSchedulers.mainThread()).subscribe { nAT, t2 ->
            if (!(gameState == null || gameState!!.aiState == null || nAT.first.isEmpty() || nAT.second.isEmpty()))
                AIL.move(gameState!!, nAT.first, nAT.second)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { gameState ->
                        view?.showState(gameState)
                        view?.showGroundButton(GameLogic.canGround(gameState.board, gameState.getCurrentPlayer()))
                        saveGameState(gameState)
                        if (gameState.gameFinished) onGameFinished(gameState) else {
                            isWaitingForAi = gameState.getCurrentPlayer() == Player.AI
                            if (isWaitingForAi) processAIMove()
                        }
                    }
        })
    }

    override fun onRevertPressed() {

    }

    override fun onHistoryPressed() {
        if (gameState == null) return
        if (isInHistoryMode) {
            view?.showState(gameState!!)
            view?.hideHistory()
            isInHistoryMode = false
        } else {
            isInHistoryMode = true
            compositeDisposable.add(dbManager
                .getGameStatesByGameId(gameState!!.gameID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { list, _ -> if (list != null && list.isNotEmpty()) view?.showHistory(list) }
            )
        }
    }

    override fun onExitClicked() {
        exit()
    }

    override fun onTurnPress(gameState: GameState) {
        view?.showState(gameState)
    }

    private fun createBoard(columns: Int, rows: Int): Array<Array<Cell>> {
        val res: MutableList<Array<Cell>> = mutableListOf()
        for (column in 0 until columns) {
            val l: MutableList<Cell> = mutableListOf()
            for (row in 0 until rows) l.add(Cell(column, row, null, null, null))
            res.add(l.toTypedArray())
        }
        return res.toTypedArray()
    }

    private fun saveGameState(gameState: GameState) {
        compositeDisposable.add(dbManager
            .saveGameState(gameState)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { _, _ -> })
    }

    private fun exit() {
        compositeDisposable.dispose()
        screenManager.switchToMainMenu()
    }

}