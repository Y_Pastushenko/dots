package com.aeolus.dots.screens.menu.impl.fragments

import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aeolus.dots.R
import com.aeolus.dots.entities.Player
import com.aeolus.dots.screens.menu.MenuPresenter
import com.aeolus.dots.screens.menu.impl.MenuActivity
import kotlinx.android.synthetic.main.fragment_new_game_menu.*
import kotlinx.android.synthetic.main.list_item_added_player.view.*

class NewGameMenuFragment : Fragment() {

    companion object {
        const val COLOR_SELECTED = "com.aeolus.dots.screens.menu.impl.fragments.SELECTED_COLOR"
        const val ADDED_PLAYERS = "com.aeolus.dots.screens.menu.impl.fragments.GAMES"
        const val START_GAME_VISIBLE = "com.aeolus.dots.screens.menu.impl.fragments.START_GAME_VISIBLE"
        const val RULES_VISIBLE = "com.aeolus.dots.screens.menu.impl.fragments.RULES_VISIBLE"
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(COLOR_SELECTED, color_picker_view.getSelectedColor())
        outState.putBoolean(START_GAME_VISIBLE, b_start_game.visibility == VISIBLE)
        outState.putBoolean(RULES_VISIBLE, ll_customize_rules.visibility == VISIBLE)
        outState.putParcelableArrayList(ADDED_PLAYERS, ArrayList((rv_players.adapter as AddedPlayersAdapter).players))
        super.onSaveInstanceState(outState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        val colorSelected = savedInstanceState?.getInt(COLOR_SELECTED, -1)
        if (colorSelected != null && colorSelected != -1)
            b_pick_color.setBackgroundColor(colorSelected)
        val startGameVisible = savedInstanceState?.getBoolean(START_GAME_VISIBLE, false)
        if (startGameVisible != null) b_start_game.visibility = if (startGameVisible) VISIBLE else GONE
        val rulesVisible = savedInstanceState?.getBoolean(RULES_VISIBLE, false)
        if (rulesVisible != null) ll_customize_rules.visibility = if (rulesVisible) VISIBLE else GONE
        val addedPlayers: MutableList<Player>? = savedInstanceState?.getParcelableArrayList(ADDED_PLAYERS)
        if (addedPlayers != null && addedPlayers.isNotEmpty()) {
            (rv_players.adapter as AddedPlayersAdapter).setPlayers(addedPlayers)
        }
        super.onActivityCreated(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_new_game_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_players.isNestedScrollingEnabled = false
        rv_players.layoutManager = LinearLayoutManager(context)
        rv_players.adapter = AddedPlayersAdapter(getMenuActivity()?.presenter, context)
        b_pick_color.setOnClickListener {
            color_picker_view.show()
        }
        b_add_player.setOnClickListener {
            if (et_name.text.isBlank()) {
                getMenuActivity()?.showMessage("PlayingUntil name")
            } else {
                getMenuActivity()?.presenter?.onPlayerAdd(
                    Player(
                        et_name.text.toString(),
                        color_picker_view.getSelectedColor(),
                        false
                    )
                )
            }
        }
        b_start_game.setOnClickListener {
            if (et_columns.text.isBlank()) {
                getMenuActivity()?.showMessage("Type columns number")
                return@setOnClickListener
            }
            if (et_rows.text.isBlank()) {
                getMenuActivity()?.showMessage("Type rows number")
                return@setOnClickListener
            }
            if (radio_until_score.isChecked && et_until_score.text.isBlank()) {
                getMenuActivity()?.showMessage("Type score")
                return@setOnClickListener
            }
            val columns = et_columns.text.toString().toInt()
            val rows = et_rows.text.toString().toInt()
            getMenuActivity()
                ?.presenter
                ?.onStartGamePressed(
                    columns,
                    rows,
                    if (radio_whole_board.isChecked) null else et_until_score.text.toString().toInt(),
                    et_empty_field_cost.text.toString().toFloat(),
                    radio_releasing_dots_on.isChecked,
                    radio_additional_turn_on.isChecked,
                    radio_dot_into_surrounded_on.isChecked
                )
        }
        color_picker_view.onClosedListener = { b_pick_color.setBackgroundColor(color_picker_view.getSelectedColor()) }
        b_pick_color.setBackgroundColor(color_picker_view.getSelectedColor())
        et_columns.setText("15")
        et_rows.setText("15")
        radio_whole_board.setOnClickListener {
            et_until_score.setText("")
            et_until_score.inputType = InputType.TYPE_NULL
        }
        radio_until_score.setOnClickListener {
            et_until_score.setText("20")
            et_until_score.inputType = InputType.TYPE_CLASS_NUMBER
        }
        radio_whole_board.performClick()
        radio_releasing_dots_off.performClick()
        et_empty_field_cost.setText("0")
        radio_additional_turn_off.performClick()
        radio_dot_into_surrounded_off.performClick()
        b_customize_rules.setOnClickListener {
            if (ll_customize_rules.visibility == VISIBLE) ll_customize_rules.visibility = GONE
            else ll_customize_rules.visibility = VISIBLE
        }
        updateDefaultName()
    }

    private fun updateDefaultName() {
        val defName = "Player ${(rv_players.adapter)?.itemCount}"
        et_name.setText(defName)
    }

    fun addPlayerToList(player: Player) {
        (rv_players.adapter as AddedPlayersAdapter).addPlayer(player)
        color_picker_view.setRandomColor()
        b_pick_color.setBackgroundColor(color_picker_view.getSelectedColor())
        updateDefaultName()
    }

    fun removePlayerFromList(player: Player) {
        (rv_players.adapter as AddedPlayersAdapter).deletePlayer(player)
        updateDefaultName()
    }

    fun showStartGameButton() {
        b_start_game.visibility = View.VISIBLE
    }

    fun hideStartGameButton() {
        b_start_game.visibility = View.GONE
    }

    private fun getMenuActivity(): MenuActivity? {
        return activity as MenuActivity
    }

    fun colorPickerVisible(): Boolean {
        return color_picker_view.isVisible()
    }

    fun hideColorPicker() {
        color_picker_view.hide()
    }

    class AddedPlayersAdapter(
        private val presenter: MenuPresenter?,
        private val context: Context?
    ) :
        RecyclerView.Adapter<AddedPlayerViewHolder>() {

        val players: MutableList<Player> = mutableListOf()

        fun addPlayer(player: Player) {
            players.add(player)
            notifyDataSetChanged()
        }

        fun setPlayers(players: MutableList<Player>) {
            this.players.clear()
            this.players.addAll(players)
            notifyDataSetChanged()
        }

        fun deletePlayer(player: Player) {
            players.remove(player)
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): AddedPlayerViewHolder {
            return AddedPlayerViewHolder(
                LayoutInflater.from(context)
                    .inflate(R.layout.list_item_added_player, parent, false)
            )
        }

        override fun onBindViewHolder(holder: AddedPlayerViewHolder, position: Int) {
            holder.rlBackground.setBackgroundColor(players[position].color)
            holder.tvName.text = players[position].name
            holder.ivCancel.setOnClickListener {
                presenter?.onDeletePlayerPressed(players[position])
            }
        }

        override fun getItemCount(): Int {
            return players.size
        }

    }

    class AddedPlayerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val rlBackground: RelativeLayout = view.rl_background
        val tvName: TextView = view.tv_name
        val ivCancel: ImageView = view.iv_cancel
    }

}