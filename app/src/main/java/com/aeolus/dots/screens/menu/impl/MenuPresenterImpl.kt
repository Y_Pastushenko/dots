package com.aeolus.dots.screens.menu.impl

import android.os.Handler
import com.aeolus.dots.entities.Game
import com.aeolus.dots.entities.Player
import com.aeolus.dots.entities.Rules
import com.aeolus.dots.model.database.DBManager
import com.aeolus.dots.root.view.RootView
import com.aeolus.dots.screens.ScreenManager
import com.aeolus.dots.screens.menu.MenuPresenter
import com.aeolus.dots.screens.menu.MenuView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class MenuPresenterImpl(val screenManager: ScreenManager, private val dbManager: DBManager) : MenuPresenter {

    private var view: MenuView? = null
    private val players: MutableList<Player> = mutableListOf()
    private var backClickedBefore = false
    private var isOnGeneralMenu = false
    private val compositeDisposable = CompositeDisposable()

    override fun onViewCreated(rootView: RootView) {
        view = rootView as MenuView
    }

    override fun onInit() {
        compositeDisposable.add(dbManager
            .getLatestGame()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { game, _ -> openGeneralMenu(game != null) }
        )
    }

    override fun onQuickGamePressed() {
        val players: MutableList<Player> = mutableListOf()
        players.add(Player.AI)
        players.add(Player.DEFAULT)
        screenManager.switchToGameScreen(
            players,
            15,
            15,
            Rules(releasingDots = false, additionalTurn = false, housesAllowed = true, initialCross = true)
        )
    }

    override fun onNewGamePressed() {
        openNewGameMenu()
    }

    override fun onPlayerAdd(player: Player) {
        players.add(player)
        view?.addPlayerToList(player)
        if (players.size > 1)
            view?.showStartGameButton()
    }

    override fun onDeletePlayerPressed(player: Player) {
        players.remove(player)
        view?.removePlayerFromList(player)
        if (players.size < 2)
            view?.hideStartGameButton()
    }

    override fun onStartGamePressed(
        columns: Int,
        rows: Int,
        scoreUntil: Int?,
        emptyFieldCost: Float,
        releasingDots: Boolean,
        additionalTurn: Boolean,
        putDotsInsideSurrounded: Boolean
    ) {
        compositeDisposable.dispose()
        screenManager.switchToGameScreen(
            players,
            columns,
            rows,
            Rules(scoreUntil ?: -1, emptyFieldCost, releasingDots, additionalTurn, putDotsInsideSurrounded, false)
        )
    }

    override fun onHistoryPressed() {
        openHistory()
    }

    override fun onResumeGamePressed() {
        compositeDisposable.add(dbManager
            .getLatestGame()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { game, err ->
                if (game != null) {
                    compositeDisposable.dispose()
                    screenManager.switchToGameScreen(game)
                }

            })
    }

    override fun onBackPressed() {
        if (!isOnGeneralMenu)
            compositeDisposable.add(dbManager
                .getLatestGame()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { game, _ -> openGeneralMenu(game != null) })
        else {
            if (backClickedBefore) {
                compositeDisposable.dispose()
                view?.exitApp()
            }
            backClickedBefore = true
            view?.showMessage("Press back again to exit")
            Handler().postDelayed({ backClickedBefore = false }, 2000)
        }
    }

    private fun openGeneralMenu(showResume: Boolean) {
        view?.showGeneralMenu(showResume)
        isOnGeneralMenu = true
    }

    private fun openNewGameMenu() {
        view?.showNewGameMenu()
        isOnGeneralMenu = false
    }

    private fun openHistory() {
        compositeDisposable.add(dbManager
            .getAllGames()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { list, _ ->
                if (list != null) {
                    view?.showHistory(list)
                } else {
                    view?.showHistory(mutableListOf())
                }
                isOnGeneralMenu = false
            }
        )
    }

    override fun onDeleteGameFromHistoryPressed(game: Game) {
        compositeDisposable.add(
            dbManager
                .deleteGame(game)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _, _ -> view?.deleteGameFromHistory(game) }
        )
    }

    override fun onHistoryGamePressed(game: Game) {
        compositeDisposable.dispose()
        screenManager.switchToGameScreen(game)
    }

}