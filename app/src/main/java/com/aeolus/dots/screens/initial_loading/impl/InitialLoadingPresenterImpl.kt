package com.aeolus.dots.screens.initial_loading.impl

import com.aeolus.dots.model.database.DBManager
import com.aeolus.dots.root.view.RootView
import com.aeolus.dots.screens.ScreenManager
import com.aeolus.dots.screens.initial_loading.InitialLoadingPresenter
import com.aeolus.dots.screens.initial_loading.InitialLoadingView
import io.reactivex.disposables.CompositeDisposable

class InitialLoadingPresenterImpl(
    val screenManager: ScreenManager,
    val dbManager: DBManager
) : InitialLoadingPresenter {

    private var view: InitialLoadingView? = null
    private val compositeDisposable = CompositeDisposable()

    override fun onViewCreated(rootView: RootView) {
        view = rootView as InitialLoadingView
    }

    override fun onInit() {
        screenManager.switchToMainMenu()
    }

    override fun onBackPressed() {

    }

}