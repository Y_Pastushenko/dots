package com.aeolus.dots.screens.menu

import com.aeolus.dots.entities.Game
import com.aeolus.dots.entities.Player
import com.aeolus.dots.root.presenter.RootPresenter

interface MenuPresenter : RootPresenter {
    fun onNewGamePressed()
    fun onPlayerAdd(player: Player)
    fun onDeletePlayerPressed(player: Player)
    fun onStartGamePressed(
        columns: Int,
        rows: Int,
        scoreUntil: Int?,
        emptyFieldCost: Float,
        releasingDots: Boolean,
        additionalTurn: Boolean,
        putDotsInsideSurrounded: Boolean
    )
    fun onResumeGamePressed()
    fun onDeleteGameFromHistoryPressed(game: Game)
    fun onHistoryPressed()
    fun onHistoryGamePressed(game: Game)
    fun onQuickGamePressed()
}