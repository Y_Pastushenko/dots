package com.aeolus.dots.screens.initial_loading.impl

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.aeolus.dots.App
import com.aeolus.dots.R
import com.aeolus.dots.root.view.RootActivity
import com.aeolus.dots.screens.initial_loading.InitialLoadingPresenter
import com.aeolus.dots.screens.initial_loading.InitialLoadingView
import javax.inject.Inject

class InitialLoadingActivity : RootActivity(), InitialLoadingView {

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, InitialLoadingActivity::class.java))
        }
    }

    @Inject
    lateinit var presenter: InitialLoadingPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as App).component.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_initial_loading)
        if (configurationChanged)
            presenter = rootPresenter as InitialLoadingPresenter
        inflatePresenter(presenter)
    }

}
