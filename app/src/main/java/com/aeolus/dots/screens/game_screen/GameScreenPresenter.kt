package com.aeolus.dots.screens.game_screen

import com.aeolus.dots.entities.Game
import com.aeolus.dots.entities.GameState
import com.aeolus.dots.entities.Player
import com.aeolus.dots.entities.Rules
import com.aeolus.dots.root.presenter.RootPresenter

interface GameScreenPresenter : RootPresenter {
    fun onCellClicked(column: Int, row: Int)
    fun onRevertPressed()
    fun onGameDataRetrieved(players: MutableList<Player>, columns: Int, rows: Int, rules: Rules)
    fun onGameDataRetrieved(game: Game)
    fun onCellClickConfirmationGot(confirmed: Boolean)
    fun onHistoryPressed()
    fun onExitClicked()
    fun onTurnPress(gameState: GameState)
    fun onGroundPressed()
}