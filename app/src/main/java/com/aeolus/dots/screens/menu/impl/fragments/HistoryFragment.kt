package com.aeolus.dots.screens.menu.impl.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aeolus.dots.R
import com.aeolus.dots.entities.Game
import com.aeolus.dots.screens.menu.MenuPresenter
import com.aeolus.dots.screens.menu.impl.MenuActivity
import kotlinx.android.synthetic.main.fragment_history.*
import kotlinx.android.synthetic.main.list_item_history_game.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HistoryFragment : Fragment() {

    companion object {
        const val GAMES = "com.aeolus.dots.screens.menu.impl.fragments.GAMES"
    }

    private val games: MutableList<Game> = mutableListOf()

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelableArrayList(GAMES, ArrayList(games))
        super.onSaveInstanceState(outState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        val list: List<Game>? = savedInstanceState?.getParcelableArrayList(GAMES)
        if (list != null && list.isNotEmpty() && rv_history.adapter != null) {
            setUp(list)
            (rv_history.adapter as GamesAdapter).setGames(list)
        }
        super.onActivityCreated(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_history.layoutManager = LinearLayoutManager(context)
        rv_history.adapter = GamesAdapter(getMenuActivity()?.presenter, context)
        (rv_history.adapter as GamesAdapter).setGames(games)
    }

    fun setUp(games: List<Game>): HistoryFragment {
        this.games.clear()
        this.games.addAll(games)
        return this
    }

    fun addGameToList(game: Game) {
        (rv_history.adapter as GamesAdapter).addGame(game)
    }

    fun removeGameFromList(game: Game) {
        (rv_history.adapter as GamesAdapter).deleteGame(game)
    }

    private fun getMenuActivity(): MenuActivity? {
        return activity as MenuActivity
    }

    class GamesAdapter(
        private val presenter: MenuPresenter?,
        private val context: Context?
    ) :
        RecyclerView.Adapter<AddedGameViewHolder>() {

        private val games: MutableList<Game> = mutableListOf()

        fun addGame(game: Game) {
            games.add(game)
            notifyDataSetChanged()
        }

        fun setGames(games: List<Game>) {
            this.games.clear()
            this.games.addAll(games)
            notifyDataSetChanged()
        }

        fun deleteGame(game: Game) {
            games.remove(game)
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): AddedGameViewHolder {
            return AddedGameViewHolder(
                LayoutInflater.from(context)
                    .inflate(R.layout.list_item_history_game, parent, false)
            )
        }

        override fun onBindViewHolder(holder: AddedGameViewHolder, position: Int) {
            val name = StringBuilder()
            for (player in games[position].players)
                name.append("${player.name}, ")
            name.delete(name.length - 2, name.length - 1)
            holder.tvNames.text = name.toString()
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:ss", Locale.getDefault())
            holder.tvDateCreated.text = sdf.format(Date(games[position].dateStarted))
            holder.tvDateLastModified.text = sdf.format(Date(games[position].dateLastTurn))
            holder.ivCancel.setOnClickListener {
                presenter?.onDeleteGameFromHistoryPressed(games[position])
            }
            holder.rlBackground.setOnClickListener { presenter?.onHistoryGamePressed(games[position]) }
        }

        override fun getItemCount(): Int {
            return games.size
        }

    }

    class AddedGameViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val rlBackground: RelativeLayout = view.rl_background
        val tvNames: TextView = view.tv_names
        val tvDateCreated: TextView = view.tv_date_created
        val tvDateLastModified: TextView = view.tv_date_last_modified
        val ivCancel: ImageView = view.iv_cancel
    }

}