package com.aeolus.dots.di.modules

import android.content.Context
import com.aeolus.dots.model.database.DBManager
import com.aeolus.dots.model.database.DBManagerImpl
import com.aeolus.dots.model.network.NetworkManager
import com.aeolus.dots.model.network.NetworkManagerImpl
import com.aeolus.dots.model.network.Retrofit
import com.aeolus.dots.model.network.RetrofitService
import com.aeolus.dots.model.shared_preferences.SharedPreferencesManager
import com.aeolus.dots.model.shared_preferences.SharedPreferencesManagerImpl
import com.aeolus.dots.model.tree.TreeManager
import com.aeolus.dots.screens.ScreenManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun provideScreenManager(): ScreenManager {
        return ScreenManager()
    }

    @Provides
    @Singleton
    fun provideSharedPreferencesManager(context: Context): SharedPreferencesManager {
        return SharedPreferencesManagerImpl(context)
    }

    @Provides
    @Singleton
    fun provideDBManager(context: Context): DBManager {
        return DBManagerImpl(context)
    }

    @Provides
    fun provideRetrofit(): RetrofitService {
        return Retrofit.makeRetrofitService()
    }

    @Provides
    @Singleton
    fun provideNetworkManager(retrofitService: RetrofitService): NetworkManager {
        return NetworkManagerImpl(retrofitService)
    }

    @Provides
    @Singleton
    fun provideTreeManager(context: Context): TreeManager {
        return TreeManager(context)
    }


}
