package com.aeolus.dots.di.modules

import com.aeolus.dots.model.database.DBManager
import com.aeolus.dots.screens.ScreenManager
import com.aeolus.dots.screens.initial_loading.InitialLoadingPresenter
import com.aeolus.dots.screens.initial_loading.impl.InitialLoadingPresenterImpl
import dagger.Module
import dagger.Provides

@Module
class InitialLoadingModule {

    @Provides
    fun providePresenter(
        screenManager: ScreenManager,
        dbManager: DBManager
    ): InitialLoadingPresenter {
        return InitialLoadingPresenterImpl(screenManager, dbManager)
    }

}