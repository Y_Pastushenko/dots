package com.aeolus.dots.di.components

import com.aeolus.dots.App
import com.aeolus.dots.di.modules.*
import com.aeolus.dots.screens.game_screen.impl.GameScreenActivity
import com.aeolus.dots.screens.initial_loading.impl.InitialLoadingActivity
import com.aeolus.dots.screens.menu.impl.MenuActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidModule::class,
        DataModule::class,
        InitialLoadingModule::class,
        MenuModule::class,
        GameScreenModule::class
    ]
)
interface ApplicationComponent {

    //<editor-fold desc="App">
    fun inject(application: App)
    //</editor-fold>

    //<editor-fold desc="Activity">
    fun inject(activity: InitialLoadingActivity)
    fun inject(activity: MenuActivity)
    fun inject(activity: GameScreenActivity)
    //</editor-fold>


}