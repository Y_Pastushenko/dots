package com.aeolus.dots.di.modules

import com.aeolus.dots.model.database.DBManager
import com.aeolus.dots.screens.ScreenManager
import com.aeolus.dots.screens.menu.MenuPresenter
import com.aeolus.dots.screens.menu.impl.MenuPresenterImpl
import dagger.Module
import dagger.Provides

@Module
class MenuModule {

    @Provides
    fun providePresenter(screenManager: ScreenManager, dbManager: DBManager): MenuPresenter {
        return MenuPresenterImpl(screenManager, dbManager)
    }

}