package com.aeolus.dots.di.modules

import android.content.Context
import com.aeolus.dots.App
import dagger.Module
import dagger.Provides

@Module
class AndroidModule(private val application: App) {

    @Provides
    fun provideContext(): Context {
        return application.applicationContext
    }

    @Provides
    fun provideApplication(): App {
        return application
    }

}
