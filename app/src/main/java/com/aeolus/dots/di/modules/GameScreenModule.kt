package com.aeolus.dots.di.modules

import com.aeolus.dots.model.database.DBManager
import com.aeolus.dots.model.tree.TreeManager
import com.aeolus.dots.screens.ScreenManager
import com.aeolus.dots.screens.game_screen.GameScreenPresenter
import com.aeolus.dots.screens.game_screen.impl.GameScreenPresenterImpl
import dagger.Module
import dagger.Provides

@Module
class GameScreenModule {

    @Provides
    fun providePresenter(
        screenManager: ScreenManager,
        dbManager: DBManager,
        treeManager: TreeManager
    ): GameScreenPresenter {
        return GameScreenPresenterImpl(screenManager, dbManager, treeManager)
    }

}