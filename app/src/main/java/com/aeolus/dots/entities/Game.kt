package com.aeolus.dots.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aeolus.dots.model.database.DBScheme
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = DBScheme.Tables.Game.NAME)
data class Game(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = DBScheme.Tables.Game.Columns.ID)
    val id: Long = 0,

    @ColumnInfo(name = DBScheme.Tables.Game.Columns.PLAYERS)
    val players: MutableList<Player>,

    @ColumnInfo(name = DBScheme.Tables.Game.Columns.DATE_STARTED)
    val dateStarted: Long,

    @ColumnInfo(name = DBScheme.Tables.Game.Columns.DATE_LAST_TURN)
    val dateLastTurn: Long
) : Parcelable