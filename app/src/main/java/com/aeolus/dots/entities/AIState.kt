package com.aeolus.dots.entities

import android.os.Parcelable
import com.aeolus.dots.entities.tree.GameTree
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AIState(
    var columns: Int,
    var rows: Int,
    var gameTree: MutableList<GameTree> = mutableListOf(),
    var crossCount: Int = 0,
    var closestAIChainIdx: Int = -1,
    var aiChains: MutableList<Chain>? = null,
    var humanChains: MutableList<Chain>? = null,
    var fieldOfChains: Array<Array<String>>? = null,
    var fieldSpectrumAI: Array<DoubleArray> = Array(columns) { DoubleArray(rows) { 0.0 } },
    var fieldSpectrumHuman: Array<DoubleArray> = Array(columns) { DoubleArray(rows) { 0.0 } },
    var fieldSpectrumAIMax: Double = 0.0,
    var fieldSpectrumHumanMax: Double = 0.0,
    var currentMove: Int = 0,
    var canGround: Boolean = false
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as AIState
        if (columns != other.columns) return false
        if (rows != other.rows) return false
        if (gameTree != other.gameTree) return false
        if (crossCount != other.crossCount) return false
        if (closestAIChainIdx != other.closestAIChainIdx) return false
        if (aiChains != other.aiChains) return false
        if (humanChains != other.humanChains) return false
        if (fieldOfChains != null) {
            if (other.fieldOfChains == null) return false
            if (!fieldOfChains!!.contentDeepEquals(other.fieldOfChains!!)) return false
        } else if (other.fieldOfChains != null) return false
        if (!fieldSpectrumAI.contentDeepEquals(other.fieldSpectrumAI)) return false
        if (!fieldSpectrumHuman.contentDeepEquals(other.fieldSpectrumHuman)) return false
        if (fieldSpectrumAIMax != other.fieldSpectrumAIMax) return false
        if (fieldSpectrumHumanMax != other.fieldSpectrumHumanMax) return false
        if (currentMove != other.currentMove) return false
        if (canGround != other.canGround) return false
        return true
    }

    override fun hashCode(): Int {
        var result = columns
        result = 31 * result + rows
        result = 31 * result + gameTree.hashCode()
        result = 31 * result + crossCount
        result = 31 * result + closestAIChainIdx
        result = 31 * result + (aiChains?.hashCode() ?: 0)
        result = 31 * result + (humanChains?.hashCode() ?: 0)
        result = 31 * result + (fieldOfChains?.contentDeepHashCode() ?: 0)
        result = 31 * result + fieldSpectrumAI.contentDeepHashCode()
        result = 31 * result + fieldSpectrumHuman.contentDeepHashCode()
        result = 31 * result + fieldSpectrumAIMax.hashCode()
        result = 31 * result + fieldSpectrumHumanMax.hashCode()
        result = 31 * result + currentMove
        result = 31 * result + canGround.hashCode()
        return result
    }

}