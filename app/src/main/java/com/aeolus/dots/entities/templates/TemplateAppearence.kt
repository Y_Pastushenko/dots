package com.aeolus.dots.entities.templates

import android.os.Parcelable
import com.aeolus.dots.model.game.AIL
import kotlinx.android.parcel.Parcelize

@Parcelize
class TemplateAppearence(
    var sizeWithNotAny: Int,
    var templateType: Int,
    var templateRotationType: Int,
    var templateContentArray: IntArray
) : Parcelable {

    fun situationEquals(
        point: Pair<Int, Int>,
        isSide: Boolean,
        fieldSideType: Int,
        fieldState: Array<IntArray>
    ): Boolean {
        try {
            if (!isSide) {
                for (i in 6..8) {
                    for (j in 6..8) {
                        val idx = i * 15 + j
                        if (!isEqualsBySymbol(
                                TemplateUtils.getContentSymbolNotSide(
                                    point,
                                    fieldState,
                                    idx
                                ),
                                templateContentArray[idx]
                            )
                        ) return false
                    }
                }
                if (sizeWithNotAny == 3)
                    return true

                for (i in 80..84)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                for (i in 140..144)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                var i = 95
                while (i < 136) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 99
                while (i < 140) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 5)
                    return true

                for (i in 64..70)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                for (i in 154..160)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                i = 79
                while (i < 154) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 85
                while (i < 160) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 7) return true
                for (i in 48..56)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                for (i in 168..176)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                i = 63
                while (i < 168) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 71
                while (i < 176) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 9) return true
                for (i in 32..42)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                for (i in 182..192)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                i = 47
                while (i < 182) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 57
                while (i < 192) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 11) return true
                for (i in 16..28)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false

                for (i in 196..208)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                i = 31
                while (i < 196) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 43
                while (i < 208) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 13) return true
                for (i in 0..14)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                for (i in 210..224)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                i = 15
                while (i < 210) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 29
                while (i < 224) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolNotSide(
                                point,
                                fieldState,
                                i
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                return sizeWithNotAny == 15

            } else if (fieldSideType == TemplateFieldSideType.templateFieldSideTypeLEFT) {
                for (i in 6..8) {
                    for (j in 6..8) {
                        val idx = i * 15 + j
                        if (!isEqualsBySymbol(
                                TemplateUtils.getContentSymbolLeft(

                                    point,
                                    fieldState,
                                    idx,
                                    sizeWithNotAny
                                ), templateContentArray[idx]
                            )
                        ) return false
                    }
                }
                if (sizeWithNotAny == 3) return true
                for (i in 80..84)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 140..144)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                var i = 95
                while (i < 136) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 99
                while (i < 140) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 5) return true
                for (i in 64..70)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 154..160)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 79
                while (i < 154) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 85
                while (i < 160) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 7) return true
                for (i in 48..56)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 168..176)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 63
                while (i < 168) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 71
                while (i < 176) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 9) return true
                for (i in 32..42)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 182..192)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 47
                while (i < 182) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 57
                while (i < 192) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 11) return true
                for (i in 16..28)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 196..208)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 31
                while (i < 196) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 43
                while (i < 208) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 13) return true
                for (i in 0..14)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 210..224)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 15
                while (i < 210) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 29
                while (i < 224) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolLeft(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                return sizeWithNotAny == 15

            } else if (fieldSideType == TemplateFieldSideType.templateFieldSideTypeRIGHT) {
                for (i in 6..8) {
                    for (j in 6..8) {
                        val idx = i * 15 + j
                        if (!isEqualsBySymbol(
                                TemplateUtils.getContentSymbolRight(

                                    point,
                                    fieldState,
                                    idx,
                                    sizeWithNotAny
                                ), templateContentArray[idx]
                            )
                        ) return false
                    }
                }
                if (sizeWithNotAny == 3) return true
                for (i in 80..84)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 140..144)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                var i = 95
                i = 95
                while (i < 136) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 99
                while (i < 140) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 5) return true
                for (i in 64..70)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 154..160)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 79
                while (i < 154) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 85
                while (i < 160) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 7) return true
                for (i in 48..56)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 168..176)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 63
                while (i < 168) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 71
                while (i < 176) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 9) return true
                for (i in 32..42)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 182..192)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 47
                while (i < 182) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 57
                while (i < 192) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 11) return true
                for (i in 16..28)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 196..208)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 31
                while (i < 196) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 43
                while (i < 208) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 13) return true
                for (i in 0..14)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 210..224)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 15
                while (i < 210) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 29
                while (i < 224) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolRight(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                return sizeWithNotAny == 15

            } else if (fieldSideType == TemplateFieldSideType.templateFieldSideTypeTOP) {
                for (i in 6..8) {
                    for (j in 6..8) {
                        val idx = i * 15 + j
                        if (!isEqualsBySymbol(
                                TemplateUtils.getContentSymbolTop(

                                    point,
                                    fieldState,
                                    idx,
                                    sizeWithNotAny
                                ), templateContentArray[idx]
                            )
                        ) return false
                    }
                }
                if (sizeWithNotAny == 3) return true
                for (i in 80..84)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 140..144)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                var i = 95
                while (i < 136) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(
                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 99
                while (i < 140) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(
                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 5) return true
                for (i in 64..70)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 154..160)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 79
                while (i < 154) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(
                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 85
                while (i < 160) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(
                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 7)
                    return true
                for (i in 48..56)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 168..176)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 63
                while (i < 168) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(
                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 71
                while (i < 176) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(
                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ),
                            templateContentArray[i]
                        )
                    ) {
                        return false
                    }
                    i += 15
                }
                if (sizeWithNotAny == 9) return true
                for (i in 32..42)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 182..192)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 47
                while (i < 182) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(
                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 57
                while (i < 192) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(
                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 11) return true
                for (i in 16..28)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 196..208)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 31
                while (i < 196) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(
                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 43
                while (i < 208) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(
                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 13) return true
                for (i in 0..14)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 210..224)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 15
                while (i < 210) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(
                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ),
                            templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 29
                while (i < 224) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolTop(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                return sizeWithNotAny == 15

            } else if (fieldSideType == TemplateFieldSideType.templateFieldSideTypeBOTTOM) {
                for (i in 6..8) {
                    for (j in 6..8) {
                        val idx = i * 15 + j
                        if (!isEqualsBySymbol(
                                TemplateUtils.getContentSymbolBottom(

                                    point,
                                    fieldState,
                                    idx,
                                    sizeWithNotAny
                                ), templateContentArray[idx]
                            )
                        ) return false
                    }
                }
                if (sizeWithNotAny == 3) return true
                for (i in 80..84)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 140..144)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                var i = 95
                i = 95
                while (i < 136) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 99
                while (i < 140) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 5) return true
                for (i in 64..70)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 154..160)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 79
                while (i < 154) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 85
                while (i < 160) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 7) return true
                for (i in 48..56)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 168..176)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 63
                while (i < 168) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 71
                while (i < 176) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 9) return true
                for (i in 32..42)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 182..192)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 47
                while (i < 182) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 57
                while (i < 192) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 11) return true
                for (i in 16..28)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 196..208)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 31
                while (i < 196) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 43
                while (i < 208) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                if (sizeWithNotAny == 13)
                    return true
                for (i in 0..14)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                for (i in 210..224)
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                i = 15
                while (i < 210) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                i = 29
                while (i < 224) {
                    if (!isEqualsBySymbol(
                            TemplateUtils.getContentSymbolBottom(

                                point,
                                fieldState,
                                i,
                                sizeWithNotAny
                            ), templateContentArray[i]
                        )
                    ) return false
                    i += 15
                }
                return sizeWithNotAny == 15

            }
            return false
        } catch (e: Exception) {
            return false
        }

    }

    private fun isEqualsBySymbol(field: Int, template: Int): Boolean {
        return when {
            field == template -> true
            template == AIL.DOT_ANY -> true
            field == AIL.DOT_AI -> template == AIL.DOT_AI_OR_EMPTY
            field == AIL.DOT_HUMAN -> template == AIL.DOT_HUMAN_or_EMPTY
            field == AIL.DOT_EMPTY -> template == AIL.DOT_AI_OR_EMPTY || template == AIL.DOT_HUMAN_or_EMPTY
            else -> false
        }
    }

}
