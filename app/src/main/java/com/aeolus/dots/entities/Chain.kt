package com.aeolus.dots.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.sqrt

@Parcelize
data class Chain(
    var chainStartX: Int,
    var chainStartY: Int,
    var wallIndex: Int,
    var sign: String,
    var fieldOfChains: Array<Array<String>>,
    var points: ArrayList<Pair<Int, Int>> = ArrayList(),
    var abstractPoints: ArrayList<Pair<Int, Int>> = ArrayList(),
    var chainEndX: Int = 0,
    var chainEndY: Int = 0,
    var chainLength: Double = 0.toDouble()
) : Parcelable {

    init {
        points.add(Pair(chainStartX, chainStartY))
        fieldOfChains[chainStartX][chainStartY] = "P$sign$wallIndex"
    }

    fun searchForChainEnd(gameState: GameState, fieldOfChains: Array<Array<String>>) {
        chainEndX = chainStartX
        chainEndY = chainStartY
        chainLength = 0.0
        for (x in 0 until gameState.columns)
            for (y in 0 until gameState.rows) {
                if (fieldOfChains[x][y] == "P$sign$wallIndex") {
                    if (chainLength <= sqrt((abs(x - chainStartX) * abs(x - chainStartX) + abs(y - chainStartY) * abs(y - chainStartY)).toDouble()) && max(
                            abs(x - chainStartX),
                            abs(y - chainStartY)
                        ) > max(abs(chainEndX - chainStartX), abs(chainEndY - chainStartY))
                    ) {
                        chainLength =
                            sqrt((abs(x - chainStartX) * abs(x - chainStartX) + abs(y - chainStartY) * abs(y - chainStartY)).toDouble())
                        chainEndX = x
                        chainEndY = y
                    }
                }
            }
        chainLength = (abs(chainEndX - chainStartX) + abs(chainEndY - chainStartY)).toDouble()
    }

    fun addPoint(x: Int, y: Int, fieldOfWalls: Array<Array<String>>) {
        if (fieldOfWalls[x][y] != "N") {
            return
        }
        fieldOfWalls[x][y] = "P$sign$wallIndex"
        points.add(Pair(x, y))
    }

    fun getLengthFromLastBlue(p: Pair<Int, Int>): Double {
        chainLength =
            sqrt((abs(chainEndX - p.first) * abs(chainEndX - p.first) + abs(chainEndY - p.second) * abs(chainEndY - p.second)).toDouble())
        return chainLength
    }

    fun isAtSide(gameState: GameState): Boolean {
        return (chainEndX < 2) or (chainEndY < 2) or (chainEndX > gameState.columns - 3) or (chainEndY > gameState.rows - 3)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Chain
        if (chainStartX != other.chainStartX) return false
        if (chainStartY != other.chainStartY) return false
        if (wallIndex != other.wallIndex) return false
        if (sign != other.sign) return false
        if (!fieldOfChains.contentDeepEquals(other.fieldOfChains)) return false
        if (points != other.points) return false
        if (abstractPoints != other.abstractPoints) return false
        if (chainEndX != other.chainEndX) return false
        if (chainEndY != other.chainEndY) return false
        if (chainLength != other.chainLength) return false
        return true
    }

    override fun hashCode(): Int {
        var result = chainStartX
        result = 31 * result + chainStartY
        result = 31 * result + wallIndex
        result = 31 * result + sign.hashCode()
        result = 31 * result + fieldOfChains.contentDeepHashCode()
        result = 31 * result + points.hashCode()
        result = 31 * result + abstractPoints.hashCode()
        result = 31 * result + chainEndX
        result = 31 * result + chainEndY
        result = 31 * result + chainLength.hashCode()
        return result
    }
}