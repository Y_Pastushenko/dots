package com.aeolus.dots.entities.tree

import android.os.Parcelable
import com.aeolus.dots.entities.templates.TemplateAppearence
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GameTree(
    var templateAppearence: TemplateAppearence,
    var center: Pair<Int, Int>,
    var tree: Tree?,
    var transformAIPoint: Pair<Int, Int>? = null,
    var currentNode: Node? = tree!!.node,
    var isSearchBySymmetry: Int = 0,
    val templateRotationType: Int = templateAppearence.templateRotationType
) : Parcelable