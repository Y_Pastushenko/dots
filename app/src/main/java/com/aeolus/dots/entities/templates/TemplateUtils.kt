package com.aeolus.dots.entities.templates

import com.aeolus.dots.model.game.AIL

class TemplateUtils {

    companion object {

        const val BEGIN = 0
        const val SQUARE_SIDE = 1
        const val SQUARE = 2
        const val TypeWALL = 3
        const val WALL_SIDE = 4
        const val ABSTRACT_DEFENSE_WALL = 5
        const val ABSTRACT_ATTACK_WALL = 6
        const val GLOBAL_ATTACK = 7
        const val GLOBAL_DESTROY = 8
        const val WALL_DESTROY = 9
        const val FINAL_AI_ATTACK = 10
        const val CONTINUED_AI_ATTACK = 11
        const val GROUND_SIDE = 12
        const val GROUND = 13

        internal var lastIndexOfTemplateType = 14

        private fun toString(type: Int): String {
            return when (type) {
                SQUARE_SIDE -> "sst"
                SQUARE -> "sqt"
                TypeWALL -> "wlt"
                WALL_SIDE -> "wst"
                FINAL_AI_ATTACK -> "fra"
                CONTINUED_AI_ATTACK -> "cra"
                GROUND_SIDE -> "grs"
                GROUND -> "grt"
                WALL_DESTROY -> "wdt"
                BEGIN -> "bgt"
                GLOBAL_ATTACK -> "gat"
                GLOBAL_DESTROY -> "gdt"
                ABSTRACT_DEFENSE_WALL -> "adw"
                ABSTRACT_ATTACK_WALL -> "aaw"
                else -> "err"
            }
        }

        fun isSide(type: Int): Boolean {
            return when (type) {
                BEGIN ->  false
                SQUARE_SIDE ->  true
                SQUARE ->  false
                TypeWALL ->  false
                WALL_SIDE ->  true
                WALL_DESTROY ->  false
                FINAL_AI_ATTACK ->  false
                CONTINUED_AI_ATTACK ->  false
                GROUND ->  false
                GROUND_SIDE ->  true
                GLOBAL_ATTACK ->  false
                GLOBAL_DESTROY ->  false
                lastIndexOfTemplateType ->  false
                ABSTRACT_DEFENSE_WALL ->  false
                ABSTRACT_ATTACK_WALL ->  false
                else ->  false
            }
        }

        fun isTemplateBaseSearchFromStartIdx(type: Int): Boolean {
            return when (type) {
                BEGIN -> false
                SQUARE_SIDE -> false
                SQUARE -> false
                TypeWALL -> false
                WALL_SIDE -> false
                WALL_DESTROY -> false
                FINAL_AI_ATTACK -> true
                CONTINUED_AI_ATTACK -> false
                GROUND -> false
                GROUND_SIDE -> false
                GLOBAL_ATTACK -> false
                GLOBAL_DESTROY -> false
                lastIndexOfTemplateType -> false
                ABSTRACT_DEFENSE_WALL -> false
                ABSTRACT_ATTACK_WALL -> false
                else -> false
            }
        }

        fun getTemplateType(str: String): Int {
            return when (str) {
                toString(BEGIN) -> BEGIN
                toString(SQUARE_SIDE) -> SQUARE_SIDE
                toString(SQUARE) -> SQUARE
                toString(TypeWALL) -> TypeWALL
                toString(WALL_SIDE) -> WALL_SIDE
                toString(WALL_DESTROY) -> WALL_DESTROY
                toString(FINAL_AI_ATTACK) -> FINAL_AI_ATTACK
                toString(CONTINUED_AI_ATTACK) -> CONTINUED_AI_ATTACK
                toString(GROUND) -> GROUND
                toString(GROUND_SIDE) -> GROUND_SIDE
                toString(GLOBAL_ATTACK) -> GLOBAL_ATTACK
                toString(GLOBAL_DESTROY) -> GLOBAL_DESTROY
                toString(ABSTRACT_DEFENSE_WALL) -> ABSTRACT_DEFENSE_WALL
                toString(ABSTRACT_ATTACK_WALL) -> ABSTRACT_ATTACK_WALL
                else -> lastIndexOfTemplateType
            }
        }

        fun getContentSymbolNotSide(p: Pair<Int, Int>, fieldState: Array<IntArray>, i: Int): Int {
            return try {
                fieldState[i % AIL.MAX_SIZE + p.first - AIL.MAX_SIZE / 2][i / AIL.MAX_SIZE + p.second - AIL.MAX_SIZE / 2]
            } catch (e: Exception) {
                AIL.DOT_ANY
            }
        }

        fun getContentSymbolRight(
            p: Pair<Int, Int>,
            fieldState: Array<IntArray>,
            i: Int,
            tSize: Int
        ): Int {
            if (i % AIL.MAX_SIZE == AIL.MAX_SIZE - (AIL.MAX_SIZE - tSize) / 2 - 1) return AIL.DOT_LAND
            return try {
                fieldState[fieldState.size - tSize + i % AIL.MAX_SIZE - (AIL.MAX_SIZE - tSize) / 2 + 1][i / AIL.MAX_SIZE + p.second - AIL.MAX_SIZE / 2]
            } catch (e: Exception) {
                AIL.DOT_ANY
            }
        }

        fun getContentSymbolLeft(p: Pair<Int, Int>, fieldState: Array<IntArray>, i: Int, tSize: Int): Int {
            if (i % AIL.MAX_SIZE == (AIL.MAX_SIZE - tSize) / 2) return AIL.DOT_LAND
            return try {
                fieldState[i % AIL.MAX_SIZE - (AIL.MAX_SIZE - tSize) / 2 - 1][i / AIL.MAX_SIZE + p.second - AIL.MAX_SIZE / 2]
            } catch (e: Exception) {
                AIL.DOT_ANY
            }
        }

        fun getContentSymbolBottom(
            p: Pair<Int, Int>,
            fieldState: Array<IntArray>,
            i: Int,
            tSize: Int
        ): Int {
            if (i / AIL.MAX_SIZE == AIL.MAX_SIZE - (AIL.MAX_SIZE - tSize) / 2 - 1) return AIL.DOT_LAND
            return try {
                fieldState[i % AIL.MAX_SIZE + p.first - AIL.MAX_SIZE / 2][fieldState[0].size - tSize + i / AIL.MAX_SIZE - (AIL.MAX_SIZE - tSize) / 2 + 1]
            } catch (e: Exception) {
                AIL.DOT_ANY
            }
        }

        fun getContentSymbolTop(
            p: Pair<Int, Int>,
            fieldState: Array<IntArray>,
            i: Int,
            tSize: Int
        ): Int {
            if (i / AIL.MAX_SIZE == (AIL.MAX_SIZE - tSize) / 2) return AIL.DOT_LAND
            return try {
                fieldState[i % AIL.MAX_SIZE + p.first - AIL.MAX_SIZE / 2][i / AIL.MAX_SIZE - (AIL.MAX_SIZE - tSize) / 2 - 1]
            } catch (e: Exception) {
                AIL.DOT_ANY
            }
        }
    }
}
