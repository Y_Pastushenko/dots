package com.aeolus.dots.entities.tree

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Node(
    val level: Int,
    var condition: TreeCondition?,
    var parent: Long? = null,
    var children: Vector<Long>? = null,
    var points: MutableList<Pair<Int, Int>> = arrayListOf(),
    var childNodes: MutableList<Long> = arrayListOf(),
    var blueIsDefault: Boolean = false,
    var moveType: Int = 0,
    var id: Long = UUID.randomUUID().mostSignificantBits
) : Cloneable, Parcelable {

    companion object {
        const val HUMAN: Int = 1
        const val AI: Int = 2
    }

    public override fun clone(): Node {
        val newNode: Node
        try {
            newNode = super.clone() as Node
            newNode.children = null
            newNode.parent = null

        } catch (e: CloneNotSupportedException) {
            throw Error(e.toString())
        }

        return newNode
    }

    fun addChild(newChild: Node?, nodesManager: HashMap<Long, Node>) {
        if (newChild != null && newChild.parent == this.id)
            addChild(newChild, getChildCount() - 1, nodesManager)
        else
            addChild(newChild, getChildCount(), nodesManager)
    }

    fun getMovesFromString(string: String, type: Int): ArrayList<Pair<Int, Int>> {
        var str = string
        val points = ArrayList<Pair<Int, Int>>()
        if (type == AI) {
            val x = (str.substring(0, str.indexOf(","))).toInt()
            val y = (str.substring(str.indexOf(",") + 1)).toInt()
            points.add(Pair(x, y))
            return points
        }
        if (type == HUMAN) {
            if (str == "default") {
                return points
            }
            while (true) {
                val idx = str.indexOf(";")
                var s: String
                s = if (idx == -1) str else str.substring(0, idx)
                val x = (s.substring(0, str.indexOf(","))).toInt()
                val y = (s.substring(str.indexOf(",") + 1)).toInt()
                points.add(Pair(x, y))
                str = str.replaceFirst("$s;".toRegex(), "")
                if (idx == -1) return points
            }
        }
        return points
    }

    private fun getChildCount(): Int {
        return if (children == null) {
            0
        } else {
            children!!.size
        }
    }

    private fun addChild(newChild: Node?, childIndex: Int, nodesManager: HashMap<Long, Node>) {
        if (newChild == null) {
            throw IllegalArgumentException("new child is null")
        } else if (isNodeAncestor(newChild, nodesManager)) {
            throw IllegalArgumentException("new child is an ancestor")
        }
        val oldParent = nodesManager[newChild.parent]
        oldParent?.deleteChild(newChild, nodesManager)
        newChild.parent = this.id
        if (children == null) {
            children = Vector()
        }
        children!!.insertElementAt(newChild.id, childIndex)
    }

    private fun deleteChild(childIndex: Int, nodesManager: HashMap<Long, Node>) {
        val child = getChildAt(childIndex, nodesManager)
        children!!.removeElementAt(childIndex)
        child.parent = null
    }

    private fun deleteChild(aChild: Node?, nodesManager: HashMap<Long, Node>) {
        if (aChild == null) {
            throw IllegalArgumentException("argument is null")
        }

        if (!isNodeAChild(aChild)) {
            throw IllegalArgumentException("argument is not a child")
        }
        deleteChild(getIndex(aChild), nodesManager)
    }

    private fun getChildAt(index: Int, nodesManager: HashMap<Long, Node>): Node {
        if (children == null) {
            throw ArrayIndexOutOfBoundsException("node has no children")
        }
        return nodesManager[children!!.elementAt(index)]!!
    }

    private fun getIndex(aChild: Node?): Int {
        if (aChild == null) {
            throw IllegalArgumentException("argument is null")
        }

        return if (!isNodeAChild(aChild)) {
            -1
        } else children!!.indexOf(aChild.id)
    }

    private fun isNodeAncestor(anotherNode: Node?, nodesManager: HashMap<Long, Node>): Boolean {
        if (anotherNode == null) {
            return false
        }

        val ancestor: Node? = this
        var anc: Node?
        do {
            if (ancestor === anotherNode) {
                return true
            }
            anc = nodesManager[ancestor!!.parent]
        } while (anc != null)

        return false
    }

    private fun isNodeAChild(aNode: Node?): Boolean {
        return if (aNode == null) {
            false
        } else {
            if (getChildCount() == 0) {
                false
            } else {
                aNode.parent == id
            }
        }
    }

}
