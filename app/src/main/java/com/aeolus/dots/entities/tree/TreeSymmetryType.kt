package com.aeolus.dots.entities.tree

class TreeSymmetryType {

    companion object {

        var treeSymmetryTypeGORIZONTAL = 0
        var treeSymmetryTypeVERTICAL = 1
        var treeSymmetryTypeMAIN_DIAGONAL = 2
        var treeSymmetryTypeSECOND_DIAGONAL = 3

    }

}
