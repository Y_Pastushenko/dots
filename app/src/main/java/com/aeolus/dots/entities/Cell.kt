package com.aeolus.dots.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Cell(val column: Int, val row: Int, var dot: Dot?, var surroundedBy: Cell? = null, var inHouseBy: Cell?) :
    Parcelable, Cloneable {

    companion object {
        val EMPTY = Cell(-1, -1, null, null, null)
    }

    fun hasDot(): Boolean {
        return dot != null
    }

    fun isSurrounded(): Boolean {
        return surroundedBy != null
    }

    fun surroundedByPlayer(): Player? {
        return surroundedBy?.surroundedBy?.dot?.player
    }

    fun isInHouse(): Boolean {
        return inHouseBy != null
    }

    override fun toString(): String {
        return "$column:$row"
    }

    override fun equals(other: Any?): Boolean {
        if (other !is Cell)
            return false
        return (other.row == row && other.column == column && other.hasDot() == hasDot())
    }

    override fun hashCode(): Int {
        var result = column
        result = 31 * result + row
        result = 31 * result + (dot?.hashCode() ?: 0)
        result = 31 * result + (surroundedBy?.hashCode() ?: 0)
        return result
    }

    public override fun clone(): Cell {
        return Cell(column, row, dot, surroundedBy, inHouseBy)
    }

}