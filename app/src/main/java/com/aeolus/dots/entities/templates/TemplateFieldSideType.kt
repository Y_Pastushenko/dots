package com.aeolus.dots.entities.templates

import com.aeolus.dots.entities.GameState
import com.aeolus.dots.model.game.AIL

class TemplateFieldSideType {

    companion object {

        var templateFieldSideTypeTOP = 0
        var templateFieldSideTypeBOTTOM = 1
        var templateFieldSideTypeLEFT = 2
        var templateFieldSideTypeRIGHT = 3
        var templateFieldSideTypeINSIDE = 4

        fun getFieldSideType(gameState: GameState, p: Pair<Int,Int>): Int {
            if ((p.first < AIL.MAX_SIZE - 1) and (p.second < AIL.MAX_SIZE - 1)) {
                return if (p.first < p.second)
                    templateFieldSideTypeLEFT
                else
                    templateFieldSideTypeTOP
            }

            if ((p.first < AIL.MAX_SIZE - 1) and (p.second > gameState.rows - AIL.MAX_SIZE)) {
                return if (p.first < gameState.rows - p.second)
                    templateFieldSideTypeLEFT
                else
                    templateFieldSideTypeBOTTOM
            }

            if ((p.first > gameState.columns - AIL.MAX_SIZE) and (p.second < AIL.MAX_SIZE - 1)) {
                return if (gameState.columns - p.first < p.second)
                    templateFieldSideTypeRIGHT
                else
                    templateFieldSideTypeTOP
            }

            if ((p.first > gameState.columns - AIL.MAX_SIZE) and (p.second > gameState.rows - AIL.MAX_SIZE)) {
                return if (gameState.columns - p.first < gameState.rows - p.second)
                    templateFieldSideTypeRIGHT
                else
                    templateFieldSideTypeBOTTOM
            }

            if (p.first > gameState.columns - AIL.MAX_SIZE) return templateFieldSideTypeRIGHT
            if (p.first < AIL.MAX_SIZE - 1) return templateFieldSideTypeLEFT
            if (p.second > gameState.rows - AIL.MAX_SIZE) return templateFieldSideTypeBOTTOM
            return if (p.second < AIL.MAX_SIZE - 1) templateFieldSideTypeTOP else templateFieldSideTypeINSIDE
        }
    }
}
