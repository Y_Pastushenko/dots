package com.aeolus.dots.entities.tree

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class TreeCondition(
    var str: String,
    var move: Pair<Int, Int> = Pair(0, 0),
    var ifmoves: ArrayList<Pair<Int, Int>> = ArrayList(),
    var enclosureType: String = ""
) : Parcelable {

    fun string(): String {
        val strIfmoves = StringBuilder("ifmoves:")
        for (i in ifmoves.indices) {
            strIfmoves.append(ifmoves[i].first).append(",").append(ifmoves[i].second).append(";")
        }
        return "move:" + move.first + "," + move.second + ";" + strIfmoves + "enclosure:" + enclosureType
    }

    init {
        val i = str.indexOf("enclosure:")
        enclosureType = if (i != -1) str.substring(i + 10, i + 11) else ""
        var s1 = str.substring(str.indexOf("move:") + 5)
        s1 = s1.substring(0, s1.indexOf(";"))
        move =
            Pair(
                Integer.valueOf(s1.substring(0, s1.indexOf(","))),
                Integer.valueOf(s1.substring(s1.indexOf(",") + 1))
            )

        try {
            var str1 = str.substring(str.indexOf("ifmoves:") + 8)
            while (true) {
                val idx = str1.indexOf(";")
                val s: String
                if (idx == -1)
                    break
                else
                    s = str1.substring(0, idx)

                val x = Integer.valueOf(s.substring(0, str1.indexOf(",")))
                val y = Integer.valueOf(s.substring(str1.indexOf(",") + 1))
                ifmoves.add(Pair(x, y))
                str1 = str1.replaceFirst("$s;".toRegex(), "")
            }
        } catch (e: Exception) {
        }

    }
}
