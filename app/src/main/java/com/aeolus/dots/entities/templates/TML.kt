package com.aeolus.dots.entities.templates

class TML {

    companion object {

        internal var template = "tm"
        var id = "id"
        var type = "tp"
        var content = "ct"
        internal var tree = "tr"
        var symmetry = "sm"
    }

}
