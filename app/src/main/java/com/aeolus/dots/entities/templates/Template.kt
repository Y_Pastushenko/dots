package com.aeolus.dots.entities.templates

import android.os.Parcelable
import com.aeolus.dots.entities.GameState
import com.aeolus.dots.entities.tree.GameTree
import com.aeolus.dots.entities.tree.Node
import com.aeolus.dots.entities.tree.Tree
import com.aeolus.dots.model.game.AIL
import com.aeolus.dots.model.game.GameLogic
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Template(
    var templateID: Int,
    var templateType: Int,
    var templateContent: IntArray,
    var tree: Tree?,
    var sizeWithNotAny: Int,
    var templateAppearance: MutableList<TemplateAppearence>
) : Parcelable {

    internal fun isEqualsLikeTemplateView(arr1: IntArray, arr2: IntArray): Boolean {
        for (i in arr1.indices) {
            if (arr1[i] != arr2[i]) {
                return false
            }
        }
        return true
    }

    private fun getTemplateAppearence(
        gameState: GameState,
        point: Pair<Int, Int>,
        fieldState: Array<IntArray>,
        templateType: Int
    ): TemplateAppearence? {
        val isSide = TemplateUtils.isSide(templateType)
        val fieldSideType =
            TemplateFieldSideType.getFieldSideType(
                gameState,
                point
            )
        for (i in templateAppearance.indices) {
            if (templateAppearance[i].situationEquals(point, isSide, fieldSideType, fieldState)) {
                return templateAppearance[i]
            }
        }
        return null
    }

    internal fun getMoveByPoints(
        gameState: GameState,
        nodesManager: HashMap<Long, Node>,
        pointList: MutableList<Pair<Int, Int>>,
        fieldState: Array<IntArray>,
        templateType: Int
    ): Pair<Int,Int>? {
        for (j in pointList.indices) {
            val tw =
                getTemplateAppearence(gameState, pointList[j], fieldState, templateType) ?: continue
            try {
                var center: Pair<Int, Int> = Pair(pointList[j].first, pointList[j].second)
                if (TemplateUtils.isSide(templateType)) {
                    when (TemplateFieldSideType.getFieldSideType(
                        gameState,
                        Pair(pointList[j].first, pointList[j].second)
                    )) {
                        TemplateFieldSideType.templateFieldSideTypeTOP -> center =
                            Pair(pointList[j].first, sizeWithNotAny / 2 - 1)
                        TemplateFieldSideType.templateFieldSideTypeBOTTOM -> center =
                            Pair(pointList[j].first, gameState.rows - sizeWithNotAny / 2)
                        TemplateFieldSideType.templateFieldSideTypeLEFT -> center =
                            Pair(sizeWithNotAny / 2 - 1, pointList[j].second)
                        TemplateFieldSideType.templateFieldSideTypeRIGHT -> center =
                            Pair(gameState.columns - sizeWithNotAny / 2, pointList[j].second)
                    }
                }
                val ta = GameTree(tw, center, tree)
                if (AIL.levelMoveExists(
                        Pair(gameState.addedCell.column, gameState.addedCell.row),
                        false,
                        ta,
                        gameState,
                        nodesManager
                    )
                ) {
                    val res = ta.transformAIPoint!!
                    if (GameLogic.canPutDot(
                            res.first,
                            res.second,
                            gameState
                        )
                    ) {
                        gameState.aiState!!.gameTree.add(ta)
                        return res
                    } else
                        continue
                } else
                    continue
            } catch (e: Exception) {
                continue
            }

        }
        return null
    }

    internal fun getByTemplateType(
        gameState: GameState,
        nodesManager: HashMap<Long, Node>,
        point: Pair<Int, Int>,
        fieldState: Array<IntArray>,
        templateType: Int
    ): Pair<Int,Int>? {
        val tw = getTemplateAppearence(gameState, point, fieldState, templateType)
            ?: return null
        try {
            var center: Pair<Int, Int> = Pair(point.first, point.second)
            if (TemplateUtils.isSide(templateType)) {
                when (TemplateFieldSideType.getFieldSideType(
                    gameState,
                    Pair(point.first, point.second)
                )) {
                    TemplateFieldSideType.templateFieldSideTypeTOP -> center =
                        Pair(point.first, sizeWithNotAny / 2 - 1)
                    TemplateFieldSideType.templateFieldSideTypeBOTTOM -> center =
                        Pair(point.first, gameState.rows - sizeWithNotAny / 2)
                    TemplateFieldSideType.templateFieldSideTypeLEFT -> center =
                        Pair(sizeWithNotAny / 2 - 1, point.second)
                    TemplateFieldSideType.templateFieldSideTypeRIGHT -> center =
                        Pair(gameState.columns - sizeWithNotAny / 2, point.second)
                }
            }
            val ta = GameTree(tw, center, tree)
            return if (AIL.levelMoveExists(
                    Pair(gameState.addedCell.column, gameState.addedCell.row),
                    false,
                    ta,
                    gameState,
                    nodesManager
                )
            ) {
                val result = ta.transformAIPoint!!
                if ((GameLogic.canPutDot(
                        result.first,
                        result.second,
                        gameState
                    ))
                ) {
                    gameState.aiState!!.gameTree.add(ta)
                    result
                } else
                    null
            } else
                null
        } catch (e: Exception) {
            return null
        }

    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Template
        if (templateID != other.templateID) return false
        if (templateType != other.templateType) return false
        if (!templateContent.contentEquals(other.templateContent)) return false
        if (tree != other.tree) return false
        if (sizeWithNotAny != other.sizeWithNotAny) return false
        if (templateAppearance != other.templateAppearance) return false
        return true
    }

    override fun hashCode(): Int {
        var result = templateID
        result = 31 * result + templateType
        result = 31 * result + templateContent.contentHashCode()
        result = 31 * result + (tree?.hashCode() ?: 0)
        result = 31 * result + sizeWithNotAny
        result = 31 * result + templateAppearance.hashCode()
        return result
    }

}
