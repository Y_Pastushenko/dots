package com.aeolus.dots.entities

class DotsFactory {
    companion object {

        private val dotsPool: MutableList<Dot> = mutableListOf()

        fun getDot(player: Player): Dot {
            val td = dotsPool.filter { dot -> dot.player == player }
            return if (td.isNotEmpty())
                td[0]
            else {
                val dot = Dot(player)
                dotsPool.add(dot)
                dot
            }
        }

    }

}