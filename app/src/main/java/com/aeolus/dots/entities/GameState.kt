package com.aeolus.dots.entities

import android.os.Parcel
import android.os.Parcelable
import androidx.room.*
import com.aeolus.dots.model.database.DBScheme
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Entity(
    tableName = DBScheme.Tables.GameState.NAME,
    foreignKeys = [ForeignKey(
        entity = Game::class,
        parentColumns = arrayOf(DBScheme.Tables.Game.Columns.ID),
        childColumns = arrayOf(DBScheme.Tables.GameState.Columns.GAME_ID),
        onDelete = ForeignKey.CASCADE
    )]
)
@Parcelize
data class GameState(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = DBScheme.Tables.GameState.Columns.ID)
    var id: Long = 0,

    @ColumnInfo(name = DBScheme.Tables.GameState.Columns.GAME_ID, index = true)
    val gameID: Long,

    @ColumnInfo(name = DBScheme.Tables.GameState.Columns.TURN_NUMBER)
    var turnNumber: Int,

    @ColumnInfo(name = DBScheme.Tables.GameState.Columns.BOARD)
    val board: Array<Array<Cell>>,

    @ColumnInfo(name = DBScheme.Tables.GameState.Columns.RULES)
    val rules: Rules,

    @ColumnInfo(name = DBScheme.Tables.GameState.Columns.GAME_FINISHED)
    var gameFinished: Boolean = false,

    @ColumnInfo(name = DBScheme.Tables.GameState.Columns.PLAYERS)
    val players: MutableList<PlayerState>,

    @ColumnInfo(name = DBScheme.Tables.GameState.Columns.LINES)
    val lines: MutableList<Line>,

    @ColumnInfo(name = DBScheme.Tables.GameState.Columns.CELL_TO_PREVIEW)
    var addedCell: Cell = Cell.EMPTY,

    @ColumnInfo(name = DBScheme.Tables.GameState.Columns.AI_STATE)
    var aiState: AIState?

) : Parcelable, Cloneable {

    companion object {
        const val DOT_EMPTY: Int = 2
        const val DOT_PLAYER: Int = 3
        const val DOT_AI: Int = 5
    }

    @Transient
    @IgnoredOnParcel
    var aiPlayer: Player? = null

    @Transient
    @IgnoredOnParcel
    var aiOpponent: Player? = null

    @Transient
    @IgnoredOnParcel
    var columns: Int

    @Transient
    @IgnoredOnParcel
    var rows: Int

    init {
        if (players.size < 3) {
            val ai = players.firstOrNull { it.player.isAi }
            val opp = players.firstOrNull { !it.player.isAi }
            if (ai != null && opp != null) {
                aiPlayer = ai.player
                aiOpponent = opp.player
            }
        }
        columns = board.size
        rows = board[0].size
    }

    fun getFieldState(): Array<IntArray> {
        val arr = Array(board.size) { IntArray(board[0].size) }
        for (x in 0 until board.size)
            for (y in 0 until board[0].size)
                arr[x][y] = celToType(board[x][y])
        return arr
    }

    private fun celToType(cell: Cell): Int {
        return when {
            !cell.hasDot() -> DOT_EMPTY
            cell.isSurrounded() && cell.surroundedBy!!.dot!!.player == aiOpponent!! ||
                    cell.hasDot() && cell.dot!!.player == aiOpponent!! -> DOT_PLAYER
            cell.isSurrounded() && cell.surroundedBy!!.dot!!.player == aiPlayer!! ||
                    cell.hasDot() && cell.dot!!.player == aiPlayer!! -> DOT_AI
            else -> -5
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as GameState
        if (!board.contentDeepEquals(other.board)) return false
        if (players != other.players) return false
        return true
    }

    override fun hashCode(): Int {
        var result = board.contentDeepHashCode()
        result = 31 * result + players.hashCode()
        return result
    }

    private fun getCurrentPlayerIndex(): Int {
        return turnNumber % players.size
    }

    fun getCurrentPlayer(): Player {
        return players[getCurrentPlayerIndex()].player
    }

    fun getCurrentPlayerScore(): Float {
        return players[getCurrentPlayerIndex()].score
    }

    fun setCurrentPlayerScore(score: Float) {
        players[getCurrentPlayerIndex()].score = score
    }

    fun getPlayerScore(player: Player): Float {
        return players.find { it.player == player }?.score ?: 0f
    }

    fun setPlayerScore(player: Player, score: Float) {
        players.find { it.player == player }?.score = score
    }

    fun getBestPlayer(): Player? {
        if (players.isNotEmpty()) {
            val max = players.maxBy { it.score } ?: return null
            return if (players.filterNot { it == max }.maxBy { it.score }!!.score == max.score)
                null
            else
                max.player
        } else return null
    }

    public override fun clone(): GameState {
        var parcel: Parcel? = null
        try {
            parcel = Parcel.obtain()
            parcel!!.writeParcelable(this, 0)
            parcel.setDataPosition(0)
            return parcel.readParcelable(this.javaClass.classLoader)
        } finally {
            parcel?.recycle()
        }
    }

}