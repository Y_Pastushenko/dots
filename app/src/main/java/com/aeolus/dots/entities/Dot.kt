package com.aeolus.dots.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Dot(val player: Player) : Parcelable {

    override fun toString(): String {
        return "dot: $player"
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other !is Dot) return false
        return other.player == player
    }
}