package com.aeolus.dots.entities.templates

import com.aeolus.dots.model.game.AIL

class TemplateRotationType {

    companion object {

        internal var templateRotationTypeR0 = 0
        internal var templateRotationTypeR90 = 1
        internal var templateRotationTypeR180 = 2
        internal var templateRotationTypeR270 = 3
        internal var templateRotationTypeGORIZONTAL = 4
        internal var templateRotationTypeVERTICAL = 5
        internal var templateRotationTypeGORIZONTAL90 = 6
        internal var templateRotationTypeVERTICAL90 = 7

        internal fun getTransformArray(templateRotationType: Int, arr: IntArray): IntArray {
            var tArr = arr
            val strNew = IntArray(AIL.MAX_SIZE * AIL.MAX_SIZE)
            when (templateRotationType) {
                templateRotationTypeR0 -> {
                    System.arraycopy(tArr, 0, strNew, 0, AIL.MAX_SIZE * AIL.MAX_SIZE)
                    return tArr
                }
                templateRotationTypeR90 -> {
                    var idx = 0
                    for (i in 0 until AIL.MAX_SIZE) {
                        for (j in AIL.MAX_SIZE - 1 downTo 0) {
                            strNew[idx] = tArr[AIL.MAX_SIZE * j + i]
                            idx++
                        }
                    }
                    return strNew
                }
                templateRotationTypeR180 -> {
                    tArr = getTransformArray(
                        templateRotationTypeR90,
                        tArr
                    )
                    return getTransformArray(
                        templateRotationTypeR90,
                        tArr
                    )
                }
                templateRotationTypeR270 -> {
                    tArr = getTransformArray(
                        templateRotationTypeR180,
                        tArr
                    )
                    return getTransformArray(
                        templateRotationTypeR90,
                        tArr
                    )
                }
                templateRotationTypeGORIZONTAL -> {
                    tArr = getTransformArray(
                        templateRotationTypeR180,
                        tArr
                    )
                    return getTransformArray(
                        templateRotationTypeVERTICAL,
                        tArr
                    )
                }
                templateRotationTypeVERTICAL -> {
                    var idx = 0
                    for (i in AIL.MAX_SIZE - 1 downTo 0) {
                        for (j in 0 until AIL.MAX_SIZE) {
                            strNew[idx] = tArr[AIL.MAX_SIZE * i + j]
                            idx++
                        }
                    }
                    return strNew
                }
                templateRotationTypeGORIZONTAL90 -> {
                    tArr = getTransformArray(
                        templateRotationTypeGORIZONTAL,
                        tArr
                    )
                    return getTransformArray(
                        templateRotationTypeR90,
                        tArr
                    )
                }
                templateRotationTypeVERTICAL90 -> {
                    tArr = getTransformArray(
                        templateRotationTypeVERTICAL,
                        tArr
                    )
                    return getTransformArray(
                        templateRotationTypeR90,
                        tArr
                    )
                }
                else -> return tArr
            }
        }

        fun getTransformPoint(templateRotationType: Int, p: Pair<Int, Int>): Pair<Int, Int> {
            return when (templateRotationType) {
                templateRotationTypeR0 -> Pair(p.first, p.second)
                templateRotationTypeR90 -> Pair(-p.second, p.first)
                templateRotationTypeR180 -> Pair(-p.first, -p.second)
                templateRotationTypeR270 -> Pair(p.second, -p.first)
                templateRotationTypeGORIZONTAL -> Pair(-p.first, p.second)
                templateRotationTypeVERTICAL -> Pair(p.first, -p.second)
                templateRotationTypeGORIZONTAL90 -> Pair(-p.second, -p.first)
                else -> Pair(p.first, p.second)
            }
        }
    }
}
