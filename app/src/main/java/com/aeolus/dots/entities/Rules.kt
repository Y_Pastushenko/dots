package com.aeolus.dots.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Rules(
    val scoreUntil: Int = -1,
    val emptyFieldCost: Float = 0f,
    val releasingDots: Boolean,
    val additionalTurn: Boolean,
    val housesAllowed: Boolean,
    val initialCross: Boolean
) : Parcelable