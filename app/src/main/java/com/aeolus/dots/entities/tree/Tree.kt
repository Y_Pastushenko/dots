package com.aeolus.dots.entities.tree

import android.os.Parcelable
import com.aeolus.dots.entities.templates.TML
import com.aeolus.dots.entities.templates.TemplateRotationType
import kotlinx.android.parcel.Parcelize
import kotlin.math.abs

@Parcelize
data class Tree(
    var node: Node? = null,
    var symmetryType: Int = 0,
    var symmetryBorder: Double = 0.0
) : Parcelable {

    fun setTree(s: String, nodesManager: HashMap<Long, Node>): Tree {
        var str = s
        if (str.contains("<" + TML.symmetry + ":")) {
            parseSymmetryType(str.substring(0, str.indexOf(">")))
            str = str.substring(str.indexOf(">") + 1)
        } else {
            symmetryType = -1
        }
        val nodeIndexes = ArrayList<Int>()
        nodeIndexes.add(0)
        node = Node(0, null)
        nodesManager[node!!.id] = node!!
        inflateNodes(node!!, str, nodeIndexes, nodesManager)
        return this
    }

    private fun inflateNodes(node: Node, str: String, nodeIndexes: ArrayList<Int>, nodesManager: HashMap<Long, Node>) {
        addRedMoves(node, str, nodeIndexes, nodesManager)
        addBlueMoves(node, str, nodeIndexes, nodesManager)
    }

    private fun addBlueMoves(
        node: Node,
        str: String,
        nodeIndexes: java.util.ArrayList<Int>,
        nodesManager: HashMap<Long, Node>
    ) {
        var tStr = str
        var idx: Int
        val beginB = "<" + (node.level + 1) + "b:"
        val endB = "</" + (node.level + 1) + "b>"
        while (true) {
            idx = tStr.indexOf(beginB)
            if (idx == -1) break

            var s: String
            try {
                s = tStr.substring(idx, tStr.indexOf(endB) + endB.length)
            } catch (e: Exception) {
                break
            }
            tStr = tStr.replaceFirst(s.toRegex(), "")
            s = s.replaceFirst(beginB.toRegex(), "")
            s = s.replaceFirst(endB.toRegex(), "")
            val points = node.getMovesFromString(s.substring(0, s.indexOf(">")), Node.HUMAN)
            nodeIndexes.add(nodeIndexes.size)
            var n: Node?
            if (points.size > 0) {
                val blueMoves = StringBuilder()
                for (i in points.indices) {
                    blueMoves.append("(").append(points[i].first).append(",").append(points[i].second).append("); ")
                }
                n = Node(node.level + 1, null)
                nodesManager[n.id] = n
                inflateNodes(n, s, nodeIndexes, nodesManager)
                n.points = points
            } else {
                n = Node(node.level + 1, null)
                nodesManager[n.id] = n
                inflateNodes(n, s, nodeIndexes, nodesManager)
                n.blueIsDefault = true
            }
            node.childNodes.add(n.id)
            node.addChild(n, nodesManager)
            n.moveType = Node.HUMAN
        }
    }

    private fun addRedMoves(
        node: Node,
        str: String,
        nodeIndexes: java.util.ArrayList<Int>,
        nodesManager: HashMap<Long, Node>
    ) {
        var tStr = str
        var idx: Int
        val beginR = "<" + (node.level + 1) + "r:"
        val endR = "</" + (node.level + 1) + "r>"
        while (true) {
            idx = tStr.indexOf(beginR)
            if (idx == -1) break
            var s = ""
            try {
                s = tStr.substring(idx, tStr.indexOf(endR) + endR.length)
            } catch (e: Exception) {
                break
            }
            tStr = tStr.replaceFirst(s.toRegex(), "")
            s = s.replaceFirst(beginR.toRegex(), "")
            s = s.replaceFirst(endR.toRegex(), "")
            nodeIndexes.add(nodeIndexes.size)
            var condition: TreeCondition? = null
            var points: java.util.ArrayList<Pair<Int, Int>>? = null
            if (s.substring(0, s.indexOf(">")).startsWith("move:")) {
                condition = TreeCondition(s.substring(0, s.indexOf(">")))
                points = java.util.ArrayList()
                points.add(Pair(condition.move.first, condition.move.second))
            } else {
                points = node.getMovesFromString(s.substring(0, s.indexOf(">")), Node.AI)//получить красный ход
            }
            val n = Node(node.level + 1, condition)
            nodesManager[n.id] = n
            inflateNodes(n, s, nodeIndexes, nodesManager)
            node.childNodes.add(n.id)
            node.addChild(n, nodesManager)
            n.points = points
            n.moveType = Node.AI
        }
    }

    private fun parseSymmetryType(str: String) {
        when {
            str.contains("gor") -> symmetryType = TreeSymmetryType.treeSymmetryTypeGORIZONTAL
            str.contains("vert") -> symmetryType = TreeSymmetryType.treeSymmetryTypeVERTICAL
            str.contains("mdiag") -> symmetryType = TreeSymmetryType.treeSymmetryTypeMAIN_DIAGONAL
            str.contains("sdiag") -> symmetryType = TreeSymmetryType.treeSymmetryTypeSECOND_DIAGONAL
        }

        symmetryBorder = java.lang.Double.valueOf(str.substring(str.indexOf(",") + 1))
    }

    fun getSymmetryPoint(point: Pair<Int, Int>, templateRotationType: Int): Pair<Int, Int> {
        try {
            val p = TemplateRotationType.getTransformPoint(templateRotationType, point)
            if (symmetryType == TreeSymmetryType.treeSymmetryTypeGORIZONTAL) {
                val d = abs(p.first - symmetryBorder)
                if (symmetryBorder > p.first)
                    return TemplateRotationType.getTransformPoint(
                        templateRotationType,
                        Pair((symmetryBorder + d).toInt(), p.second)
                    )
                if (symmetryBorder < p.first)
                    return TemplateRotationType.getTransformPoint(
                        templateRotationType,
                        Pair((symmetryBorder - d).toInt(), p.second)
                    )
                if (symmetryBorder == p.first.toDouble()) {
                    println("error GORIZONTAL symmetryBorder==p.first")
                    return p
                }
            }
            if (symmetryType == TreeSymmetryType.treeSymmetryTypeVERTICAL) {
                val d = abs(p.second - symmetryBorder)
                if (symmetryBorder > p.second)
                    return TemplateRotationType.getTransformPoint(
                        templateRotationType,
                        Pair(p.first, (symmetryBorder + d).toInt())
                    )
                if (symmetryBorder < p.second)
                    return TemplateRotationType.getTransformPoint(
                        templateRotationType,
                        Pair(p.first, (symmetryBorder - d).toInt())
                    )
                if (symmetryBorder == p.second.toDouble()) {
                    println("error VERTICAL symmetryBorder==p.second")
                    return p
                }
            }
            return p
        } catch (e: Exception) {
            return point
        }
    }

}
