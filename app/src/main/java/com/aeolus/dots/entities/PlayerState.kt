package com.aeolus.dots.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlayerState(val player: Player, var score: Float) : Parcelable