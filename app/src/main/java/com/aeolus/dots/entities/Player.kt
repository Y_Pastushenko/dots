package com.aeolus.dots.entities

import android.graphics.Color.BLUE
import android.graphics.Color.RED
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Player(val name: String, val color: Int, val isAi: Boolean) : Parcelable {

    companion object {
        val AI = Player("ai", RED, true)
        val DEFAULT = Player("default", BLUE, false)
    }

    override fun toString(): String {
        return "$name $color"
    }

}